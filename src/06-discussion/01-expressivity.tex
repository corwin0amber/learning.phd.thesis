\seclabel{Discussion:Expressivity}

\subsection{Inversion yielding a non-$\AFR$ formula}
\seclabel{Discussion:Expressivity:Inversion}
We show here that a na\"ive attempt to encode the summary of a procedure
(e.g. the one in \figref{Reverse:code}) immediately leads to formula that are not
$\AFR$.

Take $I'_9$~(\equref{NatSummaryReverse}), and substitute occurrences of $\B{\nextf}$
using the inversion formula \equref{Inversion}.
We obtain---
\begin{align*}
\forall\alpha,\beta: &
   \big(\alpha\B{\nextf^+}\beta \land \forall \gamma:
    \alpha\B{\nextf^+}\gamma\limplies\beta\B{\nextf^*}\gamma\big)
    \impliesBothWays \\
  &\big(\alpha\B{\nextf^+_0}\beta \land \forall \gamma:
    \alpha\B{\nextf^+_0}\gamma\limplies\beta\B{\nextf^*_0}\gamma\big)
\end{align*}
which, when converted to Prenex normal form yields the \textit{non-}$\AFR$ formula
\begin{eqnarray*}
\forall\alpha,\beta:
   \exists\gamma_1 \forall\gamma'_1&\exists\gamma_2\forall\gamma'_2:&
     \big(
     \alpha\B{\nextf^+}\beta \land
     (\alpha\B{\nextf^+}\gamma_1\limplies\beta\B{\nextf^*}\gamma_1)
        \limplies\\
    &&\;\alpha\B{\nextf^+_0}\beta \land
      (\alpha\B{\nextf^+_0}\gamma'_1\limplies\beta\B{\nextf^*_0}\gamma'_1)
    \big)\\
    &\land\;&
    \big(
     \alpha\B{\nextf^+_0}\beta \land
     (\alpha\B{\nextf^+_0}\gamma_2\limplies\beta\B{\nextf^*_0}\gamma_2)
        \limplies\\
    &&\;\alpha\B{\nextf^+}\beta \land
      (\alpha\B{\nextf^+}\gamma'_2\limplies\beta\B{\nextf^*}\gamma'_2)
    \big)
\end{eqnarray*}

\subsection{Formulas not expressible in \AF}
\seclabel{Discussion:Expressivity:Formulas}
\paragraph*{Unbounded cutpoints.}
In \secref{Pointer:Deterministic} we saw that with the assumption of ownership, the postcondition of $reverse$ could be expressed as an \AF\ formula. In contrast, if we assume an unbounded number of cutpoints then~\equref{SreachReverse} must be changed to
\begin{equation}
\begin{array}{l}
\forall \alpha, \beta: \alpha \B{\nextf^*} \beta \impliesBothWays \\
\LIFFour
{h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta}{\beta \B{\nextf_0^*} \alpha}
{\neg h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta}{\alpha \B{\nextf_0^*} \beta}
{h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta}{\false}
{\neg h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta}
{\begin{array}{@{}l@{}}
\exists \gamma: \alpha \B{\nextf_0^*} \gamma \land{} \\
  \quad\neg h_0 \B{\nextf_0^*} \gamma
  \land  \beta \B{\nextf_0^*} \nextf_0(\gamma)
\end{array}}
\end{array}
\end{equation}
The first three cases are the same as in \equref{SreachReverse}. The last case considers the situation where $\alpha$ is outside the list while $\beta$ is within the list. For $\alpha$ to reach $\beta$ in the postcondition, it must be the case that there exists a node $\gamma$ such that $\gamma$ is outside the list but its successor is within the list and reachable from $\beta$. 
%now quantifies over $\gamma$, which is the last node which enters the path.
The formula, however, introduces alternation of $\exists$ inside $\forall$ and the use of the function symbol $\nextf$ so it is outside \FOAE\ (and thus outside \AF).

\paragraph*{Correlations Between Data Structures.}
Another example of a non-$\AFR$ formula mentioned 
concerns programs that manipulate two lists of the same length.
\begin{figure}
\begin{minipage}{1in}
\begin{alltt}
\begin{tabbing}
vo\=id correl_lists(int sz) \{  \+ \\
Node c = null; Node d = null;\\
wh\=ile  (sz > 0)  \+ \{ \\
Node t = new Node();\\
t.next = c; c = t;\\
t = new Node(); \\
t.next = d; d = t;\- \\
\}\\
wh\=ile (c != null) \{ \+ \\
c = c.next; 
d = d.next;\-\\
\}\-\\
\}
\end{tabbing}
\end{alltt}
\end{minipage}
\caption{\figlabel{Correl}%
A simple Java program that creates two correlated lists.}
\end{figure}
The program in \figref{Correl} demonstrates 
a case where a weak logic is not enough to prove the absence of null
dereference in a pointer program. The first $\WHILE$ loop creates two
lists of length $sz$. Then, taking advantage of the equal lengths,
it traverses the first list --- the one pointed to by $c$ --- while at
the same time advancing the pointer $d$.

Since each iterator advances one step, the second loop preserves an invariant
that the lists at $c$ and $d$ are of the same length. Hence, as long
as $c$ is not $\nullv$, it guarantees that $d$ is not $\nullv$ either.
Unfortunately, such an invariant requires an inductive definition which is
well outside of \AF:

\begin{eqnarray*}
eqlen(x,y) &\eqdef& (x=\nullv \land y=\nullv) \;\;\lor\\
   && \big(x\neq\nullv \land y\neq\nullv \land eqlen(\nextf(x),\nextf(y))
       \big)
\end{eqnarray*}
