\seclabel{Discussion:Extensions}

\paragraph*{Doubly-linked List and Nested Lists.}
To verify a program that manipulates a doubly-linked list, all that needs to be done is to duplicate the
analysis we did for $\nextf$, for a second pointer field $\prevf$. As long
as the only atomic formulas used in assertions are $\alpha\B{\nextf^*}\beta$
and $\alpha\B{\prevf^*}\beta$ (and not, for example, 
$\alpha\B{(\nextf|\prevf)^*}\beta$), providing the substitutions for atomic
formulas in \tabref{Wprules} would not get us outside of the class $\FOAE$.
In particular, we have verified the doubly-linked list property:
%
\begin{center}
$\forall \alpha, \beta: h\B{\nextf^*}\alpha \land h\B{\nextf^*}\beta \implies
  (\alpha\B{\nextf^*}\beta \impliesBothWays \beta\B{\prevf^*}\alpha)$.
\end{center}
%
In fact we can verify nested lists and, in general, lists with arbitrary number of pointer fields
as long as reachability constraints are expressed using only one function symbol at a time,
like in the case of $next$ and $prev$ above.

\paragraph*{Cycles.}
\seclabel{Discussion:Extensions:Cyclic}
For data structures with a single pointer, the acyclicity restriction may be
lifted by using an alternative formulation that keeps and maintains more
auxiliary information~\cite{Phd:Hesse03,popl:LahiriQ08}.
Instead of keeping track of just $\nextf^*$, we instrument the edge
addition operation with a check: if the added edge is about to close
a cycle, then instead of adding the edge, we keep it in a separate set $M$ of
``cycle-inducing'' edges. Two properties of lists now come into play:
%\begin{enumerate}
%  \item 
(1)~The number of cycles reachable from program variables, and hence the size of $M$, is bounded by the number of program variables;
(2)~Any path (simple or otherwise) in the heap may utilize at most one
    of those edges, because once a path enters a cycle, there is no way out.
%\end{enumerate}
In all assertions, therefore, we replace $\alpha\B{\nextf^*}\beta$ with:
%
%\begin{center}
$\alpha\B{\nextf^*}\beta \lor
   \Lor_{\langle u,v\rangle\in M}
      (\alpha\B{\nextf^*}u \land v\B{\nextf^*}\beta)$.
%\end{center}
Notice that it is possible to construct this formula thanks to the bound on
the size of $M$; otherwise, an existential quantifier would have been required
in place of the disjunction.

\medskip
Cycles can also be combined with nesting, in such a way as to introduce
an unbounded number of cycles. To illustrate this, consider the example
of a linked list beginning at $h$ and formed by a pointer field which we
shall denote $n$, where each element serves as the head of a singly-linked
cycle along a second pointer field $m$. This is basically the same as
in the case of acyclic nested lists, only that the last node in every
sub-chain (a list segment formed by $m$) is connected to the first node
of that same chain.

One way to model this in a simple way is to assume that the programmer
designates the last edge of each cycle; that is, the edge that goes back
from the last list node to the first.
We denote this designation by introducing a ghost field named $c$. This
cycle-inducing edge is thus labeled $c$ instead of $m$.

Properties of the nested data structure can be expressed with $\AF$ formulas
as shown in \tabref{nested-cyclic-list-props}. ``Hierarchy'' means
that the primary list is contiguous, that is, there cannot be $n$-pointers
originating from the middle of sub-lists. ``Cycle edge'' describes the closing
of the cyclic list by a special edge $c$.

We were able to verify the absence of memory errors and the correct 
functioning of the program {\tt flatten}, shown in \figref{flatten}.

\bgroup

\newcommand\via[1]{\langle #1 \rangle}

\newcommand\vianpre{\via{\underline{n}}}
\newcommand\vianrtcpre{\via{\underline{n}^*}}
\newcommand\vianrtc{\via{n^*}}
\newcommand\viamrtc{\via{m^*}}
\newcommand\viarrtc{\via{r^*}}

\renewcommand\a{\alpha}
\renewcommand\b{\beta}
\renewcommand\c{\gamma}

\newcommand\nn[2]{#1\vianrtc #2}
\newcommand\mm[2]{#1\viamrtc #2}
\newcommand\clink[2]{#1\via{c}#2}

\begin{table}
\centering
\begin{tabular}{|l|l|}
\hline
  All lists are acyclic & $sll(n^*) \land sll(m^*)$ \\
\hline
  No sharing between lists &
   $\begin{array}{@{}l@{\;}l}
      \forall\a,\b,\c\!:  &\nn h\a \land \mm\a\b \;\land\\ 
                          &\nn h\c \land \mm\c\b \implies \a=\c
    \end{array}$ \\
\hline
  Hierarchy &
   $\forall \a,\b,\c:  \a\neq\b \land \b\neq\c \land \mm\a\b \implies \lnot\nn\b\c$ \\
\hline
  Cycle edge &
   $\begin{array}{@{}l@{\;}l}
      \forall \a,\b,\c: \a\neq\b \land \a\neq\c \land \nn\a\b \implies \lnot \clink\a\c \\
      \forall \a,\b: \clink\b\a \implies \nn h\a \land \mm\a\b
    \end{array}$ \\
\hline
\end{tabular}
\caption{\tablabel{nested-cyclic-list-props}Properties of a list of cyclic lists expressed in $\AF$}
\end{table}

\begin{figure}
\begin{alltt}\small
\begin{tabbing}
No\=de flatten(Node h) \{\+\\
Node i = h, j = null;\\
wh\=ile (i != null) I1 \{\+\\
Node k = i;\\
wh\=ile (k != null) I2 \{\+\\
j = k; k = k.m;\-\\
\}\\
j.c = null;\\
i = i.n; j.m = null; j.m = i;\-\\
\}\\
j.c = null; j.c := h;\\
return h;\-\\
\}
\end{tabbing}
\end{alltt}
\caption{\figlabel{flatten}A program that flattens a hierarchical structure of lists into a single cyclic list.}
\end{figure}

\egroup

\paragraph*{Bounded Sharing.}
\seclabel{Discussion:Extensions:Bounded}
Arbitrary sharing in data structures is hard, because even in lists,
any node of the list may be shared (that is, have more than one incoming edge).
In this case we have to use quantification since we do not know in advance
which node in the list is going to be a cutpoint for which other nodes.
However, when the \textit{entire} heap consists solely of
lists, the quantifier may be replaced with a
disjunction if we take into account that there is a bounded number of
program variables, which can serve as the heads of lists, and any two lists have at most one cutpoint.
Such heaps when viewed as graphs are
much simpler than general DAGs, since one can define in advance a set
of \textit{constant symbols} to hold the %% cut-edges, that is, the
edges that
induce the sharing; for example, if we have one list through the nodes
$x\to u_1 \to u_2$ and a second list through $y\to v_1 \to v_2$, all distinct 
locations, then adding an edge $u_2\to v_1$ would create sharing, as the
nodes $v_1, v_2$ become accessible from both $x$ and $y$.
This technique is also covered by Hesse~\cite{Phd:Hesse03}.


\paragraph*{Explicit Memory Management}
\seclabel{Discussion:Extensions:Explicit}

\newcommand{\free}{\textit{free}}
\begin{table}
\[
\begin{array}{||l|l|l|l|}
\hline\hline
\textbf{Command} & \textbf{Pre} &   \textbf{Mod} &\textbf{Post}\\
\hline
\texttt{ret = new()} &  \free(s) & \emptyset  & ret = s \land \neg \free(s)\\
\hline
\texttt{access y} &   \neg \free(y) & \emptyset & \\
\hline
\texttt{free(y)} &  y \neq \vnull \land \neg \free(y) & \emptyset & \free(y)\\
\hline\hline
\end{array}
\]
\caption{\tablabel{AtomicSpecResource}%%
The specifications of atomic commands for resource allocations in a C-like language.}
\end{table}

\tabref{AtomicSpecResource} updates the specification of atomic commands (provided in \tabref{AtomicSpec}) to handle explicit memory management operations.
For simplicity, we follow ANSI C semantics but do not handle arrays and pointer arithmetic.
The allocation statement assigns a freed location denoted by $s$ to $ret$ and sets its value to be non-freed.
All accesses to memory locations pointed-to by $y$ in statements \texttt{ret = y.f}, \texttt{y.f = x}, and \texttt{x = y} are required to access non-freed memory.
Finally, \texttt{free(y)} sets the free predicate to true for the node pointed-to by $y$. As a result, all the nodes reachable from $y$ cannot be accessed.

Notice that it is possible to enforce interesting safety properties naturally using \AFR\ formulas such as the absence of memory leaks. For example
a precondition for \texttt{free(x)} can be
\[
\forall \alpha: \frtc{y}{\alpha} \land \alpha \neq y  \land \alpha \neq \vnull \limplies \free(y)
\]
