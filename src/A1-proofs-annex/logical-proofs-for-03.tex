

\begin{definition}[Vocabulary]
%\begin{Name}Vocabulary\end{Name}
A \textbf{vocabulary} $\voc$ is a triple $\B{\constants, \functions, \relations}$ where
$\constants$ is a finite set of constant symbols,
$\functions$ is a finite set of function symbols where each $f \in \functions$ has a fixed arity $\arity(f)$,
and
$\relations$ is a finite set of relation symbols where each $r \in \relations$ has a fixed arity $\arity(r)$.
\end{definition}


  
\section{Reductions between Logics}
\applabel{Proofs:Reductions}
%\begin{Def}
%\begin{Name}Reduction between \FOReach\ formulas and \FO\ formulas\end{Name}
%A \FOReach\ formula $\varphi$  over a vocabulary $\voc=\B{\constants, \functions, \relations}$
%\textbf{is simulated} by a \FO\ formula $\varphi'$ over a vocabulary $\voc'=\B{\constants', \emptyset, \relations'}$
%if $\varphi$ is valid if and only if $\varphi'$ is valid.
%\end{Def}

\bgroup % local proof scope; because I'm going to redefine \a, \b, \c

\newcommand\nf[1]{\nextf({#1})}
\newcommand\nfsymb{\nextf}
\newcommand\nrtcsymb{{\nextf^*}}

\newcommand\setcompreh[2]{\{#1 \;|\; #2\}}
\newcommand\displaycloser[1]{\vspace{-0.5em}#1\par\vspace{-0.5em}}

\renewcommand\a{\alpha}
\renewcommand\b{\beta}
\renewcommand\c{\gamma}


We now state the reduction from the problem of checking validity of an $\AER$ 
formula to effectively-propositional logic for a specific function $\nextf$.
It can be generalized for
any fixed number of function symbols in order to handle, for example, doubly-linked lists or nested lists.
First we show that for \textit{acyclic functions over finite domains},
there is a one-to-one correspondence between $\nfsymb$ and $\nrtcsymb$,
as hinted previously by~\equref{Inversion}.

Let $\Locs$ be the set of memory locations (``objects'').
Let $\alpha\in\Locs$ be some node, then the set of nodes reachable from $\alpha$,
that is, $W=\setcompreh{\beta}{\nrtc{\alpha}{\beta}}$,
is linearly ordered via $\nrtcsymb$.
Thus $\alpha$ has a unique successor --- namely, the minimal
node of $W\setminus\{\alpha\}$. This gives rise to the
following lemma:

\begin{lemma} \lemlabel{RecoverNext}
For $\nfsymb : \Locs\to\Locs\cup\{\nullv\}$, which is acyclic,
\displaycloser{
\[\b=\nf\a \iff
 \ntc\a\b \land \forall\c: \ntc\a\c\implies\nrtc\b\c\]
}
for any $\a,\b\in\Locs$.

\begin{proof} \rm
\splitfst Let $\b=\nf\a$. Trivially $\ntc\a\b$.
Assume some $\c\in\Locs$ such that $\ntc\a\c$, then $\nrtc{\nf\a}\c$;
Hence $\nrtc\b\c$.

\splitsnd Let $\ntc\a\b$ and
$\ntc\a\c\implies\nrtc\b\c$ for any $\c\in\Locs$.
From the first clause, $\nrtc{\nf\a}\b$.
From the second, since $\ntc\a{\nf\a}$, follows $\nrtc\b{\nf\a}$.
Due to acyclicity, $\b=\nf\a$.\qed
\end{proof}

\end{lemma}


Relying on the fact that $L$ is finite,
the right-hand side of the lemma necessarily defines a total function
(which is, in fact, computable).
We can use this fact to simulate reachability constraints in
first-order logic. For this purpose, we use the formula $\varphi'$ from 
\equref{AER-to-EPR} (\secref{Pointer:Deterministic}).

\medskip
\begin{Sproposition}{AER-simulation} {\bf (Simulation of $\AER$)}
Consider an $\AER$ formula $\varphi$ over vocabulary $\voc=\B{\constants, \{\nextf\}, \relations}$.
Let $\varphi' \eqdef \varphi[\rtnext(t_1, t_2)/ t_1 \B{\nextf^*} t_2]$. Then
$\varphi'$ is a \FO\ formula over vocabulary $\voc'=\B{\constants, \emptyset, \relations \cup \{\rtnext\}}$ and
$\varphi$ is simulated by $ \Gamma_{\mbox{\small linOrd}} \limplies \varphi'$ where
$\Gamma_{\mbox{\small linOrd}}$ is the formula in \tabref{Rtnext}.

\begin{proof} \rm We need to show that $\varphi$ is valid $\iff$ $\varphi'$ is valid.

\splitfst Suppose that $\varphi$ is true on all appropriate structures. Let
$A'\in\STRUC{\voc'}$ be an arbitrary {\em finite} structure for $\varphi'$,
with domain $\Locs$ such
that $A'\models \Gamma_{\mbox{\small linOrd}}$.
Define $\nextf^A$ as in \lemref{RecoverNext} ---
this is well-defined since $L$ is finite.
We got a structure $A$ for $\varphi$, so $A\models\varphi$, and $(\nextf^A)^*=\rtnext^{A'}$.
Therefore, $A'\models\varphi'$.

\splitsnd Conversely, suppose that $\Gamma_{\mbox{\small linOrd}}\implies\varphi'$
is true on all appropriate structures, and let $A\in\STRUC{\voc}$ be an
arbitrary structure for $\varphi$;
By setting $\rtnext^{A'}=(\nextf^A)^*$ we get $A'$, which is a model of $\Tlinord$;
hence from the assumption, $A'\models\varphi'$;
therefore $A\models\varphi$. \qed
\end{proof}

\end{Sproposition}


\section{Program Semantics}
\applabel{Proofs:Program}

This section of the appendix refers to the definitions of 
\tabref{wlp-Basic}, \tabref{VCgen} (\secref{Preliminaries:Completeness})
and \tabref{Wprules} (\secref{Pointer:Extending}).
\bigskip

\begin{Sproposition}{AER-VC-closure} {\bf (VCs in $\AER$)}
For every program $S$ whose precondition $P$, postcondition $Q$,
branch conditions, loop conditions, and loop invariants are 
all expressed as $\AFR$ formulas, $\vcgen(\{P\} S \{Q\}) ~\in~ \AER$.

\begin{proof} Follows from closure properties of \FOAE,
 and from the fact that \FOAE\ is closed under $\wlpsym$,
 The proofs of which immediately follow.
In particular, in $\vcaux(\WHILE\ldots)$,
the subformulas
$\semp{B}$ and $I$ are \AF\ and $Q$ is \FOAE.
Thus $I \land \semp{B} \to Q$ is \FOAE.
Similarly for the $\neg\semp{B}$ case.
\end{proof}
\end{Sproposition}


\bigskip
The following proposition summarizes the properties of formulas which are used to guarantee that we can  generate \AF\ formulas for arbitrary procedures
manipulating singly and doubly linked lists with \AF\ specified invariants.
\begin{proposition}[Closure of \FOAE\ Formulas]
Let $\af$ be a closed \AF\ formula,
$\qf$ be a \QF\ formula.
Let $\varphi_1$ and $\varphi_2$ be closed \FOAE\ formulas.
Let $a$ be an atomic subformula in $\varphi_1$ and let $\varphi_1[\qf / a]$ denote the substitution of $a$ in $\varphi_1$ by $\qf$.
Let $c$ be a constant.
Then, the following formulas are all \FOAE\ formulas:
%\begin{description}
\textbf{disjunction}: $\varphi_1 \lor \varphi_2$;
\textbf{conjunction}: $\varphi_1 \land \varphi_2$;
\textbf{\AF-implication}: $\af \implies \varphi_1$;
\textbf{\QF-substitution}: $\varphi_1[\qf / a]$;
\textbf{generalization}: $\forall \alpha: \varphi_1[\alpha / c]$.
%\end{description}
\end{proposition}

\begin{theorem}{\bf (Soundness and Completeness of the $\wlpsym$  rules for heap access---atomic statements)}
\thelabel{wlp-AE:sound-complete}
For every state $\sigma$ be a structure with the vocabulary $\B{\Pvar, \{\nextf\}, \relations}$ over a set of locations
$\Locs$,
for every atomic heap access atomic statement $S$,
$\sigma \models \wlp{S}(Q)$ if and only if $\semp{S}\sigma \models Q\land ac$.

\begin{proof}\rm We will prove the theorem in terms of the operational
semantics of the atomic statements.

Let $\sigma,\sigma'\in\Sigma$ be program states such that
$\sigma\models P\land ac$.

So $\nextf^\sigma$ is a function without cycles.
%Before we continue, a few notations: we introduce the abbreviations
%$n\eqdef \sigma\nextf$ and $n'\eqdef \sigma'\nextf$. Also,
% for variables {\bf not} modified by $S$, we will omit the state
%    prefix since it does not matter: $v \eqdef \sigma v (=\sigma' v)$.

Consider the two atomic commands whose semantics update $\nextf$ ---
this is based almost completely on~\cite{Phd:Hesse03}:
%
\newcommand\npre[1]{{\nextf^\sigma}(#1)}
\newcommand\nrtcpre[2]{#1 \B{{\nextf^\sigma}^*} #2}
\newcommand\nrtcpost[2]{#1 \B{{\nextf^\sigmapost}^*} #2}
\newcommand\xpre{{x^\sigma}}
\newcommand\ypre{{y^\sigma}}
\newcommand\npost[1]{{\nextf^{\semp{S}\sigma}}(#1)}
\newcommand\npostsymb{{\nextf^{\semp{S}\sigma}}}
\newcommand\sigmapost{{\semp{S}\sigma}}

\noindent
\subproof{$S= x.n := y$} Assume that $\npre\xpre=\nullv$.
   Then $\npost\xpre=\ypre$, and for any $\a\neq \xpre$,
   $\npost\a=\npre\a$. In this case,
   $\nrtcpost\a\b \iff \nrtcpre\a\b \lor (\nrtcpre\a\xpre \land \nrtcpre\ypre\b)$,
   so via our assumption,
   \[\sigmapost\models Q \iff
     \sigma\models \substitute{Q}{\alpha \B{\nextf^*} \beta)}
     {\alpha \B{\nextf^*}\beta\lor(\alpha \B{\nextf^*}x\land y\B{\nextf^*}\beta)}\]
   Because $\sigma$ and $\sigmapost$ differ only in $\nextf$ (values of all
   program variables are the same),
   that is $\sigmapost=\sigma[\nextf\mapsto \npostsymb]$.

\noindent
\subproof{$S = x.n := \nullv$}
Similarly, we have
   $\npost\xpre=\nullv$ and for any $\a\neq \xpre$, $\npost\a=\npre\a$.
   Therefore in this case 
   $\nrtcpost\a\b \iff \nrtcpre\a\b \land
       (\neg \nrtcpre\a\xpre \lor \nrtcpre\b\xpre)$
   (here it is important that we know that $n$ is without cycles).
   Hence,
   \[\sigmapost\models Q \iff
     \sigma\models \substitute{Q}{\nrtc\a\b}{\nrtc\a\b \land
       (\neg \nrtc\a x \lor \nrtc\b x)}\]

Consider the atomic command whose semantics read the value of $\nextf$.
These do not change $\nextf$, so safely 
$\sigma\models ac \iff \sigmapost\models ac$. It is left to check
the condition for $Q$:

\noindent
\subproof{$S = x := y.n$}
    Assume $\ell\in\Locs$ such that 
    $\sigma,A\models\substitutetwo{P_{\nextf}}{s}{y}{t}{\a}$
    where $A=[\a\mapsto\ell]$ is a variable assignment
    for a fresh variable $a$.
    According to \lemref{RecoverNext}, this means that
    $\npre\ypre=\ell$.
    We then get $\sigmapost=\sigma[x\mapsto\npre\ypre]=\sigma[x\mapsto\ell]$.
    \[\sigmapost\models Q \iff
      \sigma[x\mapsto\ell]\models Q \iff
      \sigma,A\models \substitute{Q}{x}{\a}\]
    And since this holds for any $A$ as above,
    \[\sigmapost\models Q \iff
      \sigma\models \forall\alpha:\substitutetwo{P_{\nextf}}{s}{y}{t}{\alpha}\implies\substitute{Q}{x}{\alpha}\]

Finally, consider the atomic command that allocates memory.

\noindent
\subproof{$S = x := \new$}
    We model the memory allocation as a function $M:\Sigma\to\Locs$
    that given a structure $\sigma$, returns a non-$\nullv$ location $\ell\in\Locs$
    such that $\ell$ is not ``used'' --- in the sense that it cannot be
    reached from any of the program variables, that is, constant symbols
    of the set $\Pvar$. In this case 
    $\sigmapost = \sigma[x\mapsto M(\sigma)]$. Also,
    \[\sigma,\big[\a\mapsto M(\sigma)\big]\models
        \Land_{p\in\Pvar\cup\{\nullv\}}
          \neg p \B{\nextf^*} \alpha\]
    
    Assume $\sigma\models
              \forall \alpha : \left(\Land_{p\in\Pvar\cup\{\nullv\}} \neg p \B{\nextf^*} \alpha\right)
              \implies \substitute{Q}{x}{\alpha}$. 
    Hence from the use of $\forall$, we know that
    $\sigma,\big[\alpha\mapsto M(\sigma)\big]\models
              \left(\Land_{p\in\Pvar\cup\{\nullv\}} \neg p \B{\nextf^*} \alpha\right)
              \implies \substitute{Q}{x}{\alpha}$.
    Combined with $M$ specifications, 
    $\sigma,\big[\alpha\mapsto M(\sigma)\big]\models \substitute{Q}{x}{\a}$.
    Hence $\sigmapost\models Q$.
    
    Now assume that $\sigmapost\models Q$. It means it should hold for
    {\em any} implementation of $M$. Let $\ell=M(\sigma)$, then we know
    that $\ell$ can be any location such that
    \[\sigma,A\models
        \Land_{p\in\Pvar\cup\{\nullv\}}
          \neg p \B{\nextf^*} \alpha\]
    Where $A=\big[\a\mapsto\ell]$. 
    Also, $\sigma,A\models\substitute{Q}{x}{\a}$ from the same reasons
    as before. Since this holds for any such $A$ satisfying the antecendant,
    we conclude that
    $\sigma\models
              \forall \alpha : \left(\Land_{p\in\Pvar\cup\{\nullv\}} \neg p \B{\nextf^*} \alpha\right)
              \implies \substitute{Q}{x}{\alpha}$.
                    
    \qed    
              
\end{proof}
\end{theorem}



\egroup
