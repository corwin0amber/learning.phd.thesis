
\section{Simulation of an Idempotent Function in EPR}
\seclabel{Proofs:Simulation}

We prove \theref{AEAR-translation}, by providing a concrete translation in three steps.
First, we translate $\varphi$ to a $\forall^*\exists^*$-formula $\varphi'$  as described in \proref{AER-simulation}, 
keeping the function symbols $g,h_1\til h_n$ as is.  Similarly to the proof of \proref{AER-simulation},
one obtains that 
(\textit{a}) if $\varphi$ is valid in all finite structures then
$\varphi'$ is valid in all finite structures; and 
(\textit{b}) if $\varphi'$ is valid then $\varphi$ is valid.

Second, to eliminate the function symbols $h_1 \til h_n$
we repetitively use the following proposition:

\begin{proposition}
Let $N>0$.
Let $h$ be some unary function symbol,
and $\Sigma_h$ be a first-order signature (with equality)
that includes the function symbol $h$.
Let $\Sigma_R$ be the signature obtained from 
$\Sigma_h$ by replacing $h$ with a binary relation symbol $R_h$,
and adding $N$ fresh constant symbols $c_1 \til c_N$.
Consider an $\exists^*\forall^*$ sentence over $\Sigma_g$:
$\Phi={\exists x_1 \til x_n. \forall y_1 \til y_m. \Psi}$ (where $\Psi$ is quantifier free, over the variables  $x_1 \til x_n, y_1 \til y_m$).
Suppose that the only terms occurring in $\Phi$ that involve $h$ have the form
$h(z)$, where $z$ is a variable.
Define $T(\Phi)$ to be the following sentence over  $\Sigma_R$:

\begin{equation}
\begin{array}{l}
\Gamma \wedge \exists x_1, x^h_1 \til x_n,x^h_n. \forall y_1, y^h_1 \til y_m,y^h_m. \\
\bigwedge_{1 \leq i\leq n} R_h(x_i,x^h_i) \wedge (\bigwedge_{1 \leq i\leq m} R_h(y_i,y^h_i) \rightarrow \Psi')
\end{array}
\end{equation}

where:
\begin{itemize}
\item $\Gamma$ is the conjunction of the following formulas:
\begin{itemize}
\item $\forall x,y,z. R_h(x,y) \wedge R_h(x,z) \rightarrow y=z$ 
\item $\forall x. \vee_{1\leq i\leq N} R_h(x,c_i)$
\end{itemize}
\item $x^h_1 \til x^h_n, y^h_1 \til y^h_m$ are fresh variables.
\item $\Psi'$ is obtained from $\Psi$ by replacing each term of the form $h(a_i)$ by $a^h_i$ (for $a\in\{x,y\}$).
\end{itemize}
Then, $\Phi$ is satisfied by a (finite) normal structure interpreting $h$
as a function whose image cardinality is at most $N$ 
iff $T(\Phi)$ is satisfied by some (finite) normal structures.
\end{proposition}
\begin{proof}
Suppose that a (finite) normal structure $M_h$ 
interpreting $h$ as a function whose image cardinality is at most $N$ is
a model of $\Phi$. We can obtain a (finite) normal model of $T(\Phi)$ by:
$(a)$ interpreting $c_1 \til c_N$ as the elements of the image of $h^{M_h}$;
$(b)$ use the (graph of the) interpretation of $h$
as an interpretation of $R_h$.

For the converse, suppose that $M_R$ is a (finite) normal model of $T(\Phi)$.
We can obtain a (finite) normal model $M_h$ of $T(\Phi)$ by
choosing $h^{M_h} = \lambda d\in \dom{M_R}. \iota d'\in\dom{M_R}. \tup{d,d'}\in R_h^{M_R}$. This is well-defined since $M_R$ is a model of $\Gamma$.
It is straightforward to verify that $M_h$ is a model of $\Phi$.
 \end{proof}

Finally, to eliminate the function symbol $g$, we use the following proposition:

\begin{proposition}
Let $\Sigma_g$ be a first-order signature (including equality) with only one unary function symbol $g$, and $\Sigma_R$ be the signature obtained from  $\Sigma_g$ by replacing $g$ with a binary relation symbol $R_g$.
Consider an $\exists^*\forall^*$ sentence over $\Sigma_g$:
$\Phi={\exists x_1 \til x_n. \forall y_1 \til y_m. \Psi}$ (where $\Psi$ is quantifier free, over the variables  $x_1 \til x_n, y_1 \til y_m$).
Suppose that the only terms occurring in $\Phi$ are variables and terms of the form $g(z)$ (where $z$ is a variable).
Define $T(\Phi)$ to be the following sentence over  $\Sigma_R$:

\begin{equation}
\begin{array}{l}
\Gamma \wedge \exists x_1, x^g_1 \til x_n,x^g_n. \forall y_1, y^g_1 \til y_m,y^g_m. \\
\bigwedge_{1 \leq i\leq n} R_g(x_i,x^g_i) \wedge (\bigwedge_{1 \leq i\leq m} R_g(y_i,y^g_i) \rightarrow \Psi')
\end{array}
\end{equation}

where:
\begin{itemize}
\item $\Gamma_{idem}$ is the conjunction of the following formulas:
\begin{itemize}
\item $\exists x,y. R_g(x,y)$
\item $\forall x,y,z. R_g(x,y) \wedge R_g(x,z) \rightarrow y=z$ 
\item $\forall x,y. R_g(x,y) \rightarrow R_g(y,y)$
\end{itemize}
\item $x^g_1 \til x^g_n, y^g_1 \til y^g_m$ are fresh variables.
\item $\Psi'$ is obtained from $\Psi$ by replacing each term of the form $g(a_i)$ by $a^g_i$ (for $a\in\{x,y\}$).
\end{itemize}
Then, $\Phi$ is satisfied by a (finite) normal structure interpreting $g$
as an idempotent function iff $T(\Phi)$ is satisfied by some (finite) normal structure.
\end{proposition}
\begin{proof}
The left-to-right direction is clear. Indeed, if a (finite) normal structure $M_g$ 
interpreting $g$ as an idempotent function is
a model of $\Phi$, then we can use the (graph of the) interpretation of $g$
as an interpretation of $R_g$ and obtain a (finite) normal model of $T(\Phi)$.
We prove the converse. Suppose that $M_R$ is a (finite) normal model of $T(\Phi)$.
Construct a (finite) normal structure $M_g$ (for the signature $\Sigma_g$) as follows:

\begin{enumerate}
\item $\dom{M_g}=\{d \in \dom{M_R} \st \exists d'\in\dom{M_R}. \tup{d,d'}\in R_g^{M_R}\}$
\item $g^{M_g} = \lambda d\in \dom{M_g}. \iota d'\in\dom{M_g}. \tup{d,d'}\in R_g^{M_R}$
\item For every $k$-ary predicate symbol $p$ of $\Sigma_g$, $p^{M_g}=p^{M_R} \cap \dom{M_g}^k$ (i.e., $p^{M_g}$ is the restriction of $p^{M_R}$ to the new domain).
\end{enumerate}
$\dom{M_g}$ is non-empty since 
$M_R$ is a model of $\exists x,y. R_g(x,y)$.
To see that $g^{M_g}$ is well-defined, note that if $\tup{d,d'}\in R_g^{M_R}$ then both $d$ and $d'$ are in $\dom{M_g}$. Indeed, in this case $d\in \dom{M_g}$ by definition, and since $M_R$ is a model of $\forall x,y. R_g(x,y) \limplies R_g(y,y)$, we must have $\tup{d',d'}\in R_g^{M_R}$,
and thus $d'\in \dom{M_g}$ as well.
Hence for every $d\in \dom{M_g}$
there exists an element $d'\in\dom{M_g}$ such that 
$\tup{d,d'}\in R_g^{M_R}$. 
Its uniqueness is guaranteed since $M_R$ is a model of 
$\forall x,y,z. R_g(x,y) \land R_g(x,z) \limplies y=z$.
The fact that $g^{M_g}$ is idempotent directly follows from the fact that 
$M_R \models \forall x,y. R_g(x,y) \limplies R_g(y,y)$.

Now, we show that $M_g$ is a model of $\Phi$.
Let $\sigma$ be an $\dom{M_R}$-assignment (assigning elements of $\dom{M_R}$ to the variables),
such that $M_R, \sigma' \models \bigwedge_{1 \leq i\leq n} R_g(x_i,x^g_i) \land (
\bigwedge_{1 \leq i\leq m} R_g(y_i,y^g_i) \limplies \Psi')$ for every 
$\{y_1,y^g_1 \til y_m,y^g_m\}$-variant $\sigma'$ of $\sigma$.
Then, for every $1 \leq i\leq n$, $\sigma[x_i]$ and $\sigma[x^g_i]$ are elements of $\dom{M_g}$ and $g^{M_g}(\sigma[x_i])=\sigma[x^g_i]$.
Indeed, we have $M_R, \sigma \models \bigwedge_{1 \leq i\leq n} R_g(x_i,x^g_i)$, and thus $\tup{\sigma[x_i],\sigma[x^g_i]}\in R_g^{M_R}$ for every $1 \leq i\leq n$.
Consider an $\dom{M_g}$-assignment $\sigma_g$ such that $\sigma_g[x_i]=\sigma[x_i]$ for every $1 \leq i\leq n$. We show that 
$M_g, \sigma'_g \models \Psi$ for every 
$\{y_1 \til y_m\}$-variant $\sigma'_g$ of $\sigma_g$.
Consequently, $M_g, \sigma_g \models \forall y_1 \til y_m. \Psi$
and hence $M_g \models \Phi$.

Let $\sigma'_g$ be a $\{y_1 \til y_m\}$-variant $\sigma_g$.
We prove that $M_g, \sigma'_g \models \Psi$.
Consider the $\{y_1,y^g_1 \til y_m,y^g_m\}$-variant $\sigma'$ of $\sigma$
defined by $\sigma'[y_i]=\sigma'_g[y_i]$ and $\sigma'[y^g_i]=g^{M_g}(\sigma'_g[y_i])$. 
It follows from our definitions that $M_R, \sigma' \models  \bigwedge_{1 \leq i\leq m} R_g(y_i,y^g_i)$.
Therefore, since $M_R, \sigma' \models \bigwedge_{1 \leq i\leq m} R_g(y_i,y^g_i) \limplies \Psi'$, we have that  $M_R, \sigma' \models \Psi'$.
We show that 
for every atomic $\Sigma_g$-formula $p(t_1\til t_k)$ that occurs in $\Psi$,
$M_g, \sigma'_g \models p(t_1\til t_k)$ iff 
$M_R, \sigma' \models p(t'_1\til t'_k)$, where
for every $1 \leq j\leq k$, $t'_j=t_j$ if $t_j$ is a variable, 
$t'_j=a^g_i$ if $t_j=g(a_i)$ for $a\in\{x,y\}$.
By induction on the structure of $\Psi$, it would easily follow that 
$M_g, \sigma'_g \models \Psi$ (using the definition of $\Psi'$, and
the fact that $M_R, \sigma' \models \Psi'$).

Thus let $p(t_1\til t_k)$ be an atomic $\Sigma_g$-formula that occurs in $\Psi$, and let $t'_1\til t'_k$ be defined as above.
Using the natural extension of assignments to terms (given interpretations of function symbols) we have that: $\sigma'_g[t_j]=\sigma'[t_j']$
for every $1\leq j\leq k$.
This is obvious if $t_j=x_i$ or $t_j=y_i$ (since $\sigma'_g[x_i]=\sigma'[x_i]$ for every $1 \leq i\leq n$, and $\sigma'_g[y_i]=\sigma'[y_i]$ for every $1 \leq i\leq m$). But, also if $t_j=g(x_i)$ or $t_j=g(y_i)$, 
then $t'_j $ is either $x^g_i$ or $y^g_i$, and we have 
$\sigma'[x^g_i]=g^{M_g}(\sigma'_g[x_i])=\sigma'_g[g(x_i)]$ and 
$\sigma'[y^g_i]=g^{M_g}(\sigma'_g[y_i])=\sigma'_g[g(y_i)]$.
Now, $M_g, \sigma'_g \models p(t_1\til t_k)$ iff 
$\tup{\sigma'_g[t_1]\til\sigma'_g[t_k]}\in p^{M_g}$,
or equivalently iff $\tup{\sigma'[t'_1]\til\sigma'[t'_k]}\in p^{M_g}$.
Since $\sigma'[t'_j]\in \dom{M_R}$ for every $1\leq j\leq k$,
and $p^{M_g}=p^{M_R} \cap \dom{M_g}^k$, we can equivalently write
 $\tup{\sigma'[t'_1]\til\sigma'[t'_k]}\in p^{M_R}$. This is exactly the condition for $M_R, \sigma' \models p(t'_1\til t'_k)$.
 \end{proof}


Now, note that the obtained formula $\psi$ is a purely relational $\forall^*\exists^*$-formula, and so it is valid in all finite structures iff it is  generally valid.
It follows that $\varphi$  is valid (in $\AEAR$) iff $\psi$ is valid (in first-order logic).