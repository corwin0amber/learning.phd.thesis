
\section{Relative Completeness of IC3 with Predicate Abstraction}
\applabel{Proofs:Relative}


\begin{Stheorem}{PDR}
Given (i)~the set of abstraction predicates $\AbsPreds = \{p_i \in
\LogicLang\}, 1\leq i\leq n$ where $\LogicLang$ is a decidable logic,
and the full predicate abstraction domain $\AbsDomain$ over
$\AbsPreds$, (ii)~the initial condition $\Init \in \LogicLang$,
(iii)~a transition relation $\rho$ expressed as a two-vocabulary
formula in $\LogicLang$, and (iv)~a formula $\Bad \in \LogicLang$
specifying the set of bad states, $PDR_\AbsDomain(\Init,
\rho, \Bad)$ terminates, and reports either
\begin{enumerate}
\item 
  $\validKw$ if there exists $A \in \AbsDomain$ s.t. 
  (i)~$\Init\limplies A$, (ii)~$A$
  is inductive, and (iii)~$A \implies \lnot \Bad$,
\item 
  a concrete counterexample trace, which reaches a state satisfying $\Bad$, or
\item 
  an abstract trace, if the inductive invariant required to prove the
  property cannot be expressed as an element of $\AbsDomain$.
\end{enumerate}
\end{Stheorem}


\begin{proof}
The first two cases are trivial: if PDR$_\AbsDomain$ terminates returning
some $R[j]$, $j<N$, then $Init\limplies R[j]$ by virtue of $Init\limplies R[0]$
and $R[i]\limplies R[i+1]$ for every $i<N$, and $R[j]\limplies \lnot\Bad$ 
or the check at line \lineref{PDR:checkConsecution} would have failed.
Also, $R[j-1]\equiv R[j]$ so $R[j]$
is inductive.

If PDR$_\AbsDomain$ returns a set of concrete states, then they have
to be a concrete counterexample trace, as they are a model of
$\Init \land \rho^{N-j} \land \afterSteps{\Bad}{(N-j)}$ (\lineref{refine:callModel} of reduce$_\AbsDomain$).

\medskip
For the third case, we show that if the check on the first line of ``reduce''
is ``\satKw'', then there exists a chain of concrete states,
$\sigma_j \quad \sigma_{j+1} ~\cdots~ \sigma_N$,
such that $\sigma_j\models Init$, 
$\sigma_N\models Bad$, and for any $j\leq i < N$ there exist two concrete states $\sigma, \sigma'$ satisfying:
\begin{itemize}
  \item $\sigma\in\gamma(\beta_\AbsDomain(\sigma_i))$
  \item $\sigma'\in\gamma(\beta_\AbsDomain(\sigma_{i+1}))$
  \item $\langle\sigma,\sigma'\rangle \models \rho$
\end{itemize}
The key point is that, because the given abstraction can never distinguish any two
states in $\gamma(\beta_\AbsDomain(\sigma_i))$, the chain 
$\sigma_j \quad \sigma_{j+1} \cdots \sigma_N$ cannot be excluded by the
abstract domain $\AbsDomain$, no matter what Boolean combination of the
predicates of $\AbsPreds$ is used. Moreover, the chain
$\beta_\AbsDomain(\sigma_j) \quad \beta_\AbsDomain(\sigma_{j+1}) ~ \cdots ~ \beta_\AbsDomain(\sigma_N)$ is an
abstract trace that leads from an initial state to an error state.

Notice that the chain above may not be a concrete trace, there can be
``breaks'' between adjacent $\sigma_i$s, within the same abstract element.

Construction of $(\sigma_i)_{i=j\ldots N}$:
Follow the chain of recursive calls to ``reduce'' with index values $N$
down to $j$. The parameter $A$ is always a cube of the form $\beta_\AbsDomain(\sigma)$; 
take one $\sigma\models A$ for each call, forming a series
that we denote by $\sigma_j$, $\sigma_{j+1}$, etc. We show that this
series satisfies the above properties:
At each call except the innermost, ``reduce'' made a
recursive call at line 7, which means that $R[j-1]\land\rho \land (A)'$ was
satisfiable; the returned cube $A_2$ becomes $\beta_\AbsDomain(\sigma_{j-1})$.
Let $\langle\sigma,\sigma'\rangle \models R[j-1]\land\rho \land (A)'$,
then $\sigma\models A_2=\beta_\AbsDomain(\sigma_{j-1})$; 
$\sigma'\models A=\beta_\AbsDomain(\sigma_j)$; and 
$\langle\sigma,\sigma'\rangle\models\rho$ as required.
\end{proof}