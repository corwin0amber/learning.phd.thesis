\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{taucs/taucs}
\LoadClass[a4paper,11pt,twoside]{report}
% Spacing
\usepackage[a4paper,top=30mm,bottom=30mm,left=35mm,right=30mm,headsep=20pt]{geometry}
\usepackage{setspace}\onehalfspace
\renewcommand{\baselinestretch}{1.5}
\renewcommand{\arraystretch}{1.2}
\flushbottom
\pagestyle{headings} 
%
\usepackage{ifpdf}
\usepackage[pdfstartview=FitH,pdfpagemode=UseNone,backref=page,colorlinks=true,citecolor=blue,linkcolor=blue,pdfusetitle]{hyperref}\usepackage{breakurl} % provides url command and crossreferences
\usepackage{latexsym,amssymb,amsmath,amsthm,mathrsfs}
\usepackage{verbatim,alltt} % To get text as is
\usepackage{color}
\usepackage{graphicx}
\usepackage[square,numbers,comma,sort&compress]{natbib} % Use the "Natbib" style for the references in the Bibliography
\usepackage{enumerate} % Enable custom items; example: \begin{enumerate}[(i)]
% Fuzz -------------------------------------------------------------------
\hfuzz2pt % Don't bother to report over-full boxes if over-edge is < 2pt
\sloppy
\setcounter{secnumdepth}{3} % Numbering up to subsubsection
%
\newenvironment{frontmatter}{
    \pagestyle{empty}
    \setcounter{page}{1}
    \renewcommand{\thepage}{\roman{page}}}
   {\cleardoublepage
    \pagestyle{plain}
    \setcounter{page}{1}
    \renewcommand{\thepage}{\arabic{page}}
    \pagenumbering{arabic}   }   		% Arabic page numbers
%
\makeatletter
\newcommand{\advisor}[1]{\def\@advisor{#1}}
\newcommand{\copyrightyear}[1]{\def\@year{#1}}
\newcommand{\coverpages}{
% Title page
\begin{titlepage}
\begin{center}
\vspace*{0mm}
\includegraphics[scale=1.0]{taulogo}\\
The Raymond and Beverly Sackler Faculty of Exact Sciences\\
The Blavatnik School of Computer Science
\vskip 40mm
\textbf{\LARGE\onehalfspacing \@title\\}
\normalsize
\vskip 30mm
Thesis submitted for the degree of Doctor of Philosophy\\by\\
\textbf{\large \@author}
\vskip 20mm
This work was carried out under the supervision of\\
\textbf{\@advisor}
\vskip 20mm
Submitted to the Senate of Tel Aviv University\\
\@date
\end{center}
\end{titlepage}
% Copyright
\newpage
\begin{center}
        \vspace*{ 80mm}
                \large \copyright\  \@year\\
                Copyright by \@author\\
                All Rights Reserved
\end{center}} 
\renewenvironment{abstract}{\newpage\vspace*{20mm}\begin{center}\large\textbf{Abstract}\end{center}\par\noindent\ignorespaces}{\cleardoublepage}
\newenvironment{acknowledge}{\newpage\vspace*{20mm}\begin{center}\large\textbf{Acknowledgements}\end{center}\par\noindent\ignorespaces}{\cleardoublepage} 
\newenvironment{dedication}{\newpage\vspace*{80mm}\begin{center}}%
{\end{center}\cleardoublepage}
\bibliographystyle{plainnat}
