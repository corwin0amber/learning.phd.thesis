\seclabel{Conclusion}

In this thesis we investigated complex properties of programs, involving
high-order concepts as transitive closure of pointer links and its
derivatives such as sharing and entry-point. It is surprising that
such properties can be expressed and handled by a logic based on EPR,
which is among the weakest fragments of first-order logic. A key insight to enable
this was the idea of ``inversion'': instead of a function symbol for
$\nextsinglSym$ and a derived binary relation $\nextrtcSym$ for its
transitive closure, formalize the properties of $\nextrtcSym$ and have
$\nextsinglSym$ derived from it instead. As previously noted, this
is analogous to reasoning in the natural numbers using a successor
function $\textit{succ}$ (which requires constant use of induction)
vs. reasoning with $\leq$.  

% From CAV 2013
The results shed some light on the complexity of reasoning about programs that manipulate linked data structures such as singly- and doubly-linked lists.
The invariants in many of these programs can be expressed without quantifier alternation.
Alternations are introduced by unbounded cutpoints and reasoning about more complicated directed acyclic graphs.
Furthermore, for programs manipulating general graphs higher order reasoning may be
required.

% From CAV 2014

Compared to past work on shape analysis, our approach 
(i)~is based on full predicate abstraction, 
(ii)~makes use of standard theorem proving techniques,
(iii)~is capable of reporting concrete counterexamples, and
(iv)~is based on property-directed reachability.  
The experimental
evaluation in \secref{Loop:Empirical} illustrates these four
advantages of our approach.
The algorithm is able to establish memory-safety
and preservation of data-structure invariants for all of the examples,
using only a handful of simple predicates.
This result may look somewhat surprising because earlier work on shape analysis
that employed the same predicates \cite{kn:Hendren} failed to prove
these properties.
One reason is that \cite{kn:Hendren} only uses positive and negative
combinations of these predicates, whereas our algorithm uses arbitrary
Boolean combinations of predicates.

% From POPL 2014

A crucial method for simplifying the reasoning about linked data structures is partitioning programs into smaller pieces,
where each piece manipulates part of the heap. The part that a sub-program manipulate usually has a bounded number of
entry and exit points. This thesis slightly generalizes by
reasoning about a potentially unbounded number of entry points, as demonstrated by \tfind\ and \tunion.
Only the {\em new} sharing that is introduced by the procedure should be bounded.
Notice that this unboundedness supports modularity: even in the case where in every particular call context
there is a bounded number of paths (e.g. when there is a bounded number of roots in the heap), the bound
is not known in advance, therefore the programmer has to prepare for an unbounded number of cases.

It is important to note that the adaptation rule of \secref{Inter:Adaptation} adds expressive power to verifying programs: it is in general
impossible for the programmer to define, in $\AFR$, a modular specification for all the procedures.
Generation of a verification condition requires coordination between the separate call sites as mentioned above,
in particular taking note of potential sharing. This coordination requires per-call-site instantiation,
which, thanks to having the adaptation rule in the framework, is done automatically.

Finally we remark that there is a trade-off between the complexity of the mod-set and of the post-condition: defining a simpler, but larger $\modset$
may cause the post-condition to become more complicated, sometimes not even $\AFR$-expressible. Also notice
that if $\modset=V$ (the entire heap), modular reasoning becomes trivial since it can be done by relational
composition, but this puts the burden of writing the most complete post-conditions on the programmer, which
we suspect is not always possible in our limited logic.

Therefore, we believe that this thesis is an important step towards the ability to handle real-life programs
in a scalable way.

\bigskip
There are some issues still open. As we have seen, there is an interesting
class of programs that work on linked data structures with pointers and admit
EPR inductive loop invariants. Since EPR is a countable set of formulas, and
from the decision procedure developed in the previous chapters, the problem of
checking whether a program has such a loop invariant becomes r.e. by a 
simple generate-and-test approach (of course,
in general, the problem of checking a single loop invariant is undecidable,
and the problem of finding a loop invariant is not r.e.).
Is there a better way to answer this question?
The method represented in \charef{Loop}
for invariant inference allows limited use of quantifiers, restricted
by the parameter templates. It would be useful to learn new quantified
predicates and refine the abstractions (e.g.~using CEGAR); this would
require less from the programmer and would allow more instances where
fully-automated reasoning on loops can be applied. On a broader scale,
we raise a theoretical question of whether an EPR invariant can be discovered
automatically, and whether the existence of an EPR invariant is decidable.
We conjecture, however, that the answer would be negative, although a
proof is yet to be discovered.

The solution might be a heuristic for EPR inductive loop invariant inference
that would be sound but not complete; while not providing a conclusive
solution, a heuristic has a high chance of working well in practice because
naturally-occurring programs are usually ``well-behaved'' and have a
simple argument that proves their correctness. Moreover, it can be
argued that having a simple loop invariant is a good programming trait
by its own right and that programmers should make an effort to write
such programs. In any case, it is obvious that the programs presented
in undecidability proofs are highly unnatural and there is no practical
need to handle such instances.

To design such heuristics, a larger base of benchmarks should be obtained
and inspected to characterize what common programming idioms exist, then
exploit properties of these idioms to handle variants of the benchmarks
in different contexts. 

On the aspects of modular programming and reasoning, we still do not
know the bounds of modular reasoning and in which situations it can
be used. The restriction of bounded sharing (\secref{Inter:Extending:Bounding})
may be too severe for some cases, and can probably be avoided. Also,
it is interesting to investigate the differences and similarities of
handling procedure calls within loops vs. handling recursion as is
done in this thesis.

Finally, the success we had with reasoning of transitive closure
gives hope that this kind of reasoning can also be applied for the
field of synthesis, where invariants are given and the code is automatically
generated, rather than being checked against the specifications.
This can be a win-win situation, since sometimes generation of code
may even be easier than verification, and the resulting code is
correct by construction.
