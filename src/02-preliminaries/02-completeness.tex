\seclabel{Preliminaries:Completeness}

\begin{headlines}
The weakest (liberal) precondition $\wlpsym$ is defined over a program $c$ and a
post-condition formula $Q$
{\blip}
It is the weakest condition that must hold for an input state such that
after running $c$, the output state would satisfy $Q$
{\blip}
It was shown that $\wlpsym$ can be defined recursively for many programming languages
{\blip}
This means that Hoare logic can prove any valid assertion, given
a complete proof system for the underlying assertion logic
\end{headlines}

\bigskip

As \proref{Hoare-soundness} of the previous section shows, Hoare logic can
only prove true assertions. Is the converse also true---that every true
assertion is provable in Hoare logic? Clearly, this depends heavily on the
nature of the assertion language: the logical language used to describe
$P$ and $Q$. In particular, two important factors come into play:
\begin{itemize}
  \item Our ability to prove \emph{valid implications} within the
    assertion logic; this is required by the consequence rule.
  \item The capacity of the assertion language to express
    \newterm{inductive loop invariants}. This is a subtle point and
    will be addressed later.
\end{itemize}

The completeness property of Hoare logic with respect to some assumed
completeness of the assertion language is known as \newterm{relative completeness}.
To show it, we first introduce a new concept.

\begin{definition}[Weakest liberal precondition]
The \newterm{weakest liberal precondition} of an assertion $Q$ with
respect to a command $c$ is defined as the set of states $\sigma\in\Sigma$ 
where every execution of $c$ starting at state $\sigma$ either diverges
or terminates at a state $\sigma'$ such that $\sigma'\models Q$.
\end{definition}

The term ``liberal'' refers to the possibility of divergence (non-termination)
of $c$, and is used to distinguish this term from a more strict variant
where termination of $c$ must be preserved. In the remainder of the text,
we omit the word ``liberal'' and just write ``weakest precondition'',
but the intention is always ``weakest liberal precondition''.

For the claim of relative completeness we would like to show that
$\wlp{c}(Q)$ is expressible in the logical language of assertions.
If the assertion language is closed under Boolean connectives and
syntactic substitution, and if \emph{the command $c$ is loop-free},
then this is certainly the case, and explicit formulas for constructing
it are given in the first four lines of \tabref{wlp-Basic}.
Winskell~\cite{Winskel:1993:FSP} shows that for such $c$, it holds
that $\models \{P\}c\{Q\}$ if and only if $\models P\limplies \wlp{c}Q$.

\begin{table}
\begin{center}
\[
\begin{array}{|rcl|}
 \hline
 \wlp{\skipc}(Q) &\eqdef& Q\\
 \wlp{x := a}(Q) &\eqdef& \substitute{Q}{x}{a}\\
\wlp{c_1 ~; ~ c_2}(Q) &\eqdef& \wlp{c_1}(\wlp{c_2}(Q))\\
\wlp{\IF ~ B ~ \THEN~ c_1  ~ \ELSE~ c_2}(Q) &\eqdef& 
\semp{B}\land \wlp{c_1}(Q)\;\lor \\ &&\lnot\semp{B}\land\wlp{c_2}(Q) \\
\wlp{\WHILE ~ B ~ \{I\} ~ \DO~ c}(Q) &\eqdef& I\\
\hline
\end{array}
\]
\end{center}
\caption{\tablabel{wlp-Basic}
Standard rules for computing weakest liberal preconditions for While-language
procedures annotated with loop invariants and postconditions.
$I$ denotes the loop invariant, $\semp{B}$ is the semantics
of Boolean program conditions,
and $Q$ is the postcondition --- all are expressed as first-order formulas.
}
\end{table}

\subsubsection*{Inductive Loop Invariants}

In the presence of loops, where the length of the program's execution
is not bounded, the situation becomes more difficult, since these programs
require reasoning on the sets of \newterm{reachable states} from an
arbitrary starting state---which may be infinite. This usually requires the use of high-order logic.
A common alternative is to require that loops are annotated with an
appropriate \newterm{inductive loop invariant}.

A \newterm{loop invariant} is a condition that holds at the beginning
and at the end of every loop iteration. An \newterm{inductive loop invariant}
is a loop invariant with an additional restriction: satisfaction of the invariant at iteration
$i$ of the loop (for $i>1$) has to follow solely from its satisfaction at iteration
$i-1$; it cannot be based on any previous iterations.

To illustrate this subtle, but crucial, difference, consider the example
program:
\[X~:=~2 ~;~ \WHILE~Y>0~\DO~(~ X ~:=~ 2X-1 ~;~ Y~:=~Y-1 ~)\]

Regardless of the value of $Y$, it is easy to see that $X>0$ is a
valid loop invariant. $X$ is about to receive the sequence of values
2, 3, 5, 9, \ldots until at some point the loop terminates.
However, it is not an \emph{inductive} invariant: for example, if at
iteration $i-1$, $X$ would have the value $\frac{1}{2}$, then at iteration
$i$ the value will become $0$, violating the condition. The corresponding
\emph{inductive} invariant needed to show that $X$ is positive in this case
would be $X>1$ (since $X>1 \implies 2X-1>1$).

Assume that all loops are annotated with appropriate inductive
loop invariants. Then, for each loop, apply the Hoare rule for $\WHILE$:
\[\frac{\{P\land\semp{B}\}c\{P\}}
     {\{P\}\WHILE~B~\DO~c\{P\land\lnot\semp{B}\}}\]

with $P$ as the inductive loop invariant. This effectively splits 
the given program into loop-free segments, for which we know Hoare
logic to be complete, so we can finish the proof as before.

For example, take the previous program with the following annotations:
\[\{\true\}~
  X~:=~2 ~;~ \{X>1\}~\WHILE~Y>0~\DO~(~ X ~:=~ 2X-1 ~;~ Y~:=~Y-1 ~)
  ~\{X>0\}\]

Here, the pre-condition is $\true$, the post-condition is $X>0$,
and the loop invariant is $X>1$. Thus the proof obligations are
\[\begin{array}{ll}
  (i) & \{\true\} ~ X~:=~2 ~ \{X>1\} \\
  (ii) & \{X>1 \land Y>0\} ~ X ~:=~ 2X-1 ~;~ Y~:=~Y-1 ~ \{X>1\} \\
  (iii) & \{X>1 \land \lnot (Y>0)\} ~ \skipc ~ \{X>0\}
  \end{array}\]
%
which may be discharged by proving the following implications in
the theory of real numbers:
\[\begin{array}{ll}
  (i) & \true \limplies \wlp{X~:=~2}(X>1) \\
  (ii) & (X>1 \land Y>0) \limplies \wlp{X ~:=~ 2X-1 ~;~ Y~:=~Y-1}(X>1) \\
  (iii) & (X>1 \land \lnot (Y>0)) \limplies \wlp{\skipc}(X>0)
  \end{array}\]

\pagebreak[4]
We can now state a completeness theorem for Hoare logic.

\begin{theorem}[Hoare logic---completeness]
Let $c$ be a While-language command where every loop is annotated with
a loop invariant. If $\models \{P\}c\{Q\}$ and, furthermore, all loop
invariants are {\bf valid and inductive}, then there exists a Hoare proof
for $\{P\}c\{Q\}$.
\end{theorem}
