
\begin{headlines}
Computer programs represent transitions from an input state to an output state
{\blip}
Hoare logic is a formal framework for reasoning about properties
{\blip}
Hoare triples define preconditions and postconditions
{\blip}
Using proof rules, claims written as Hoare triples can be proven
\end{headlines}

\bigskip

This section presents a formal proof system for proving properties
of imperative programs, based on proof rules for individual language
constructs. Historically R.W.Floyd invented rules for reasoning about
flow charts~\cite{Floyd67}, and later C.A.R.Hoare modified and extended
these to treat programs written as code in an imperative
programming language~\cite{Hoare69}. To demonstrate this approach, consider a
simplistic programming language called ``While-language'' with the
following abstract syntax:

\[
\begin{array}{rcl}
  a & ::= & n ~|~ X ~|~ a+a ~|~ a-a ~|~ a\times a \\
  b & ::= & \true ~|~ \false ~|~ a = a ~|~ \lnot b ~|~ b\land b ~|~ b\lor b \\
  c & ::= & \skipc ~|~ X := a ~|~ c;c ~|~
    \IF~b~\THEN~c~\ELSE~c ~|~ \WHILE~b~\DO~c
\end{array}
\]

Here, $n$ ranges over integer literals, and $X$ ranges over program variables (occasionally called \newterm{locations}).
The syntactic group $a$ represents arithmetic expressions; $b$ represents Boolean
expressions; and $c$ represents commands.
We rely on the reader's intuitive model for understanding the behavior
of programs written in While.

\begin{definition}[Hoare triple]
A \newterm{partial correctness assertion} (also referred to as a \newterm{Hoare triple})
has the form
\[\{P\}~c~\{Q\}\]

\noindent where $c$ is a command and $P$, $Q$ are \newterm{assertions}.
We do not formally define the language of assertions here, but assume
they are written in some form of logic.
\end{definition}

A partial correctness assertion is said to be valid (written $\models \{P\}~c~\{Q\}$)
when every successful computation of $c$ from a state satisfying $P$
results in a state satisfying $Q$.

For example,
\[\models \{X>3\}~X := X+1~\{X>4\}\]


\begin{table}
\[
\begin{array}{cc}
\{A\}\skipc\{A\} & \{Q[a/X]\}X:=a\{Q\} \\[1em]
\displaystyle
\frac{\{P\}c_1\{C\} \quad \{C\}c_2\{Q\}}
     {\{P\}c_1;c_2\{Q\}} &
\displaystyle
\frac{\{P\land \semp{B}\}c_1\{Q\} \quad \{P\land \lnot\semp{B}\}c_2\{Q\}}
     {\{P\}\IF~B~\THEN~c_1~\ELSE~c_2\{Q\}} \\[2em]
\displaystyle
\frac{\{P\land\semp{B}\}c\{P\}}
     {\{P\}\WHILE~B~\DO~c\{P\land\lnot\semp{B}\}} &
\displaystyle
\frac{\models P\limplies P' \quad \{P'\}c\{Q'\} \quad \models Q'\limplies Q}
     {\{P\}c\{Q\}}
\end{array}
\]
\caption{\tablabel{IMP:Hoare-rules}Hoare rules for the basic 
  While-language (\cite{Winskel:1993:FSP}).}
\end{table}

\tabref{IMP:Hoare-rules} contains a set of inference rules for proving
validity of partial correctness assertions. The notation $\semp{B}$ is used
for the semantics of a Boolean condition, expressed as a logical formula.
The proof rules are often
called \newterm{Hoare rules} and the collection of rules constitutes a proof
system called \newterm{Hoare logic}.

The last rule (bottom right), called the \newterm{consequence rule}, is special
because the premises include validity of logical implications. Such
implications may be hard to prove for their own worth, and may require,
for example, arithmetical reasoning. This is the most complicated rule
in the system, and is definitely required; indeed, without it we would
not even be able to prove some trivial assertion such as---
\[\{X>1\}~\skipc~\{X>0\}\]

Of course, this makes reasoning as hard as any logical reasoning in
the logical language of the assertions.

\begin{proposition}[Soundness of Hoare logic]
\prolabel{Hoare-soundness}
If an assertion $\{P\}c\{Q\}$ is provable using the proof system
of \tabref{IMP:Hoare-rules}, then $\models \{P\}c\{Q\}$.
\end{proposition}

The proof of this proposition is carried out by systematic induction
on the proof tree. It is covered in detail in~\cite{Winskel:1993:FSP}.
