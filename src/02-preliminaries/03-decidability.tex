
\begin{headlines}
Transforming a Hoare triple into a formula also provides the
other direction
{\blip}
If a logic has a decision procedure for valid formulas, one
can also decide the validity of a Hoare triple
{\blip}
Thus effectively checking whether a program meets its specification
{\blip}
In many cases, you can get a counterexample when the specification is violated
{\blip}
Examples of decidable logical languages include Persburger arithmetic, real arithmetic, uninterpreted functions, and EPR
\end{headlines}



\paragraph*{Generating Verification Conditions}

\tabref{VCgen} provides the standard rules for computing first-order verification conditions corresponding to Hoare triples
using weakest liberal preconditions.
An auxiliary function $\vcaux$ is used for defining the set of side conditions for the loops occurring in the program.
These rules are standard and their soundness and relative completeness have been discussed elsewhere (e.g. see \cite{frade2011verification}).

The rule for $\WHILE$ loop is split into two parts: in the $\wlpsym$ we take just the loop invariant, where $\vcaux$ asserts that
loop invariants are inductive and implies the postcondition for each loop.

Verification conditions allow to completely reduce checking the validity
of (annotated) partial correctness assertions to checking validity
of logical formulas.
To check whether $\models \{P\}c\{Q\}$ as a partial correctness assertion,
under the assumption that the inductive loop invariants have been chosen
correctly, it is enough to verify that
$\models \vcgen(\{P\}c\{Q\})$ as a logical statement. 

\medskip\noindent
{\bf Remark.}
The rules may generate formulas of exponential size due to the
extensive use of syntactic substitution.
Another solution can be implemented either by using a set of symbols for every program point, 
or through the method of Flanagan and Saxe~\cite{POPL:FlanaganS01}.
Such size optimization techniques are not discussed in this thesis.

\medskip

%
 \begin{table}
 \[
 \begin{array}{|c|}
 \hline
 \vcaux(S, Q) \eqdef \varnothing \hspace{0.7cm}\mbox{\small (for any atomic command $S$)} \\
 \vcaux(S_1;S_2, Q) \eqdef \vcaux(S_1, \wlp{S_2}(Q)) \cup \vcaux(S_2, Q)\\
 \vcaux(\IF ~ B ~ \THEN~ S_1  ~ \ELSE~ S_2, Q) \eqdef \vcaux(S_1, Q) \cup \vcaux(S_2, Q)\\
 \vcaux(\WHILE ~ B ~ \{I\} ~ \DO~ S, Q) \eqdef \vcaux(S, I) \;\cup \hspace{1.7cm}{}\\ \hspace{3.4cm}\{I \land \semp{B} \limplies \wlp{S}(I), I \land \neg \semp{B} \limplies Q\}\\
\hline
\vcgen(\{P\} S \{Q\}) \eqdef \big(P \limplies \wlp{S}(Q)\big) \land \Land \vcaux(S, Q)\\
\hline
\end{array}
\]
\caption{\tablabel{VCgen}Standard rules for computing VCs using weakest liberal preconditions for procedures annotated with loop invariants and pre/postconditions.
  The rules for computing $\wlp$ appear in \tabref{wlp-Basic}.
  The auxiliary function $\vcaux$ accumulates a conjunction of VCs for the correctness of loops.
}
\end{table}

\medskip

\pagebreak
Of course, the problem of checking whether a Hoare triple is valid is undecidable
(for example, $\models\{\true\}c\{\false\}$ iff $c$ never terminates), so we can
not hope to solve it by reducing it to checking validity of logical formulas.
Indeed, checking whether a formula is valid is also undecidable in general; for
first-order logic, the problem is recursively enumerable (since one can enumerate
all possible proofs and check them), but not recursive. For first-order logic
over \emph{finite} structures, the problem is not even r.e., and there is no
complete proof system.

However, some subsets of logic are nicely behaved, in the sense that the problem
of checking whether a given formula is valid is decidable. 
Consequently, if one is able to define a language with axiomatic semantics and
corresponding $\wlp{}$ such that the verification conditions are defined in such
a logic, then the verification problem for the set of programs expressible in that
language becomes decidable by reduction to checking of logical validity.

We give a few examples of decidable logics:
\begin{itemize}
  \item \newterm{Presburger arithmetic} is the logic of natural numbers with only
    addition. The language contains the constants 0 and 1, and the binary operator
    $+$, from which all the natural numbers can be constructed. The only relation
    symbol is $=$ for equality. This is enough to express linear equations over
    natural numbers, as multiplication by a natural constant can be expressed by
    successive applications of $+$.
  \item \newterm{Real arithmetic} contains the real constants 0 and 1, equality,
    an order relation $\leq$, and the operations $+$, $-$, $\cdot$. It was shown
    by Tarski that the theory of real closed fields can be axiomatized in
    first-order logic. Furthermore, this theory admits \emph{quantifier elimination},
    so the problem is still decidable if the formulas have quantifiers, even
    though the complexity becomes non-elementary.
  \item \newterm{Quantifier-free with uninterpreted functions} is the logic where languages
    are allowed to contain any function and relation symbols, and where these symbols
    may have any interpretation (in contrast with $+$, for example, which must be 
    interpreted as addition in all structures); the only designated symbol is $=$
    for equality. Formulas must be quantifier-free and contain no variable symbols.
  \item \newterm{Effectively-propositional logic} is a subset of relational logic.
    It is introduced in the next sub-section.
\end{itemize}
