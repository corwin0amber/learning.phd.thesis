\seclabel{Preliminaries:Effectively}

\begin{headlines}
Careful use of quantifiers leads to a decidable fragment of first-order logic
{\blip}
Avoid function symbols and preserve a quantifier prefix of $\exists^*\forall^*$
{\blip}
For this family, it is known that satisfiablity is reducable to Boolean SAT
{\blip}
Hence, it is also decidable
{\blip}
Despite the exponential complexity,
modern solvers solve it efficiently in practice
\end{headlines}

\medskip

In propositional logic, atomic formulas are just predicate symbols. When viewed
as a subset of first-order logic, it means that all predicates are nullary---
take no arguments---and therefore there are no terms: no function symbols,
no variables, and no quantifiers. The satisfiability problem for propositional
formulas is widely known as SAT and is NP-complete.

A slight extension would be to allow arbitrary arity of the predicates, and have
nullary function symbols (constants). Formulas are closed by nature and take the
form of a Boolean combination of shallow atoms such as:
%
\[\varphi ~=~ P(a) \limplies P(b) \lor Q(c,a)\]

It is easy to convince oneself that if this formula has a model, then it also
has one where $a$, $b$, and $c$ are all different elements of the domain. This
suggest that we substitute every ground atom with a fresh nullary predicate
symbol and get:
%
\[\varphi' ~=~ P_a \limplies P_b \lor Q_{ca}\]

Which is equi-satisfiable to the previous one; that is, $\varphi'$ is satisfiable
iff $\varphi$ is satisfiable. So, we have effectively reduced the satisfiability
of ground formulas in this slight extension to regular Boolean SAT.

A more interesting extension involves variables and quantification. Consider a
language like the one used to express $\varphi$, but with variables and with a
quantifier \emph{prefix} (that is, all the quantifiers must occur at the beginning
of the formula) of only universal quantifiers.
%
\[\psi ~=~ \forall \alpha: P(a) \limplies P(\alpha) \lor Q(c,\alpha)\]

For this type of formula we employ a basic theorem of first-order logic,
known as \newterm{Herbrand's theorem}.

\begin{definition}Let $\Gamma(\overline{x})$ be a quantifier-free formula
  over a vocabulary $\Sigma$ with free variables $\overline{x}=x_1\til x_k$. 
  A \newterm{closed instance} of $\Gamma$ is any closed formula $\Gamma(\overline{t})$
  where $\overline{t}=t_1\til t_k$ are ground terms over $\Sigma$.
\end{definition}

\begin{theorem}[Herbrand's Theorem]
  Let $\psi = \forall \overline{x}: \Gamma(\overline{x})$.
  Then $\psi$ is satisfiable iff every finite set of closed instances of $\Gamma$
  is satisfiable as a propositional formula---i.e., with every atomic formula
  substituted by a distinct predicate symbol (and multiple occurrences of the same
  atom substituted by the same symbol).
\end{theorem}

\medskip

In the above example of $\psi$, we have $\Gamma(\alpha) = P(a) \limplies P(\alpha) \lor Q(c,\alpha)$.
Since the vocabulary contains only two constants and no other functions,
the closed instances of $\Gamma$ are only $\Gamma(a)$ and $\Gamma(c)$:
%
\[\begin{array}{c}
    P(a) \limplies P(a) \lor Q(c,a)\\
    P(a) \limplies P(c) \lor Q(c,c)
  \end{array}\]
  
Now these are ground formulas similar to $\varphi$ before, so again these formulas
can be reduced to an equi-satisfiable propositional theory:
%
\[\begin{array}{c}
    P_a \limplies P_a \lor Q_{ca}\\
    P_a \limplies P_c \lor Q_{cc}
  \end{array}\]

Clearly this theory is satisfiable, e.g. by the assignment
$\{P_1,P_2,Q_1,Q_2\mapsto\false\}$.
This means that $\psi$ is also satisfiable. To construct its model, we take
the \newterm{Herbrand universe}, which is the set of terms over the vocabulary $\Sigma$,
in this case $\{a,c\}$, and make it the domain; then we set the truth values of the
predicates according to the satisfying Boolean assignment:
%
\[\begin{array}{l|c}P& \\ \hline a & \false \\ c & \false\\\end{array}
\qquad
  \begin{array}{l|cc}Q& a & b\\ \hline a & X & X \\ c & \false & \false\\\end{array}
\]

The $X$'s are ``don't-care'' values, which may be either $\true$ or $\false$.

\medskip

Using Herbrand's theorem, the satisfiability of every universal formula over a
\newterm{relational} vocabulary (with only predicate symbols, constants, and variables)
reduces to SAT. In fact, for any formula of the form
$\exists\overline{x}\forall\overline{y}:\Gamma(\overline{x},\overline{y})$
we can apply first-order Skolemization, introducing only new constants, because
the existential variables $\overline{x}$ occur only on the outer level and are not
nested inside universals. This means that the vocabulary remains relational,
and we can carry on with the reduction.

\begin{definition}[EPR]
  Formulas of the form $\exists\overline{x}\forall\overline{y}:\Gamma(\overline{x},\overline{y})$
  are called \newterm{Effectively Propositional} formulas.
\end{definition}
  
The name comes from the fact that they can be simulated, as shown above, by
a propositional theory.

We should note that the size of the reduction is clearly exponential.
The exponent comes from the number of nested
universal quantifiers in the formula, since every universally quantified
variable has to be instantiated with all possible constants. Hence,
if a large formula can be factored into many small formulas with a small
degree of nesting, this exponent can be reduced.

\subsubsection*{Logic with Equality}

The reduction to SAT discusses only uninterpreted relation symbols. It is possible
to add a designated equality relation $=$ by the usual technique of encoding the
\newterm{equality axioms}:
%
\[\begin{array}{ll}
    \forall \alpha: \alpha=\alpha & \mbox{(reflexivity)} \\
    \forall \alpha,\beta: \alpha=\beta\limplies\beta=\alpha & \mbox{(symmetry)}\\
    \forall \alpha,\beta,\gamma:
    \alpha=\beta \land \beta=\gamma \limplies \alpha=\gamma &
    \mbox{(transitivity)} \\
    \forall \overline{x}_1,\overline{x}_2:
      \overline{x}_1=\overline{x}_2 \limplies P(\overline{x}_1)\liff P(\overline{x}_2)
    & \\
    \quad\mbox{for every predicate symbol }P\in\Sigma
  \end{array}\]
  
\noindent
where $\overline{x}_1,\overline{x}_2$ are lists of distinct variables with length
corresponding to the arity of $P$. The formula $\overline{x}_1 = \overline{x}_2$
is a shorthand for a conjunction of pointwise equalities.

Since the theory of equality is relational and universal, it can be added to any
EPR formula and the reduction explained above would work just the same. The only
difference is that to construct a model where the interpretation of $=$ is indeed
equality, some elements need to be merged; for example, if we get the following
truth table for $=$:
%
\[\begin{array}{l|ccc}=& a & b & c\\ \hline a & \true & \false & \false \\ 
b & \false & \true & \true \\ c & \false & \true & \true \\\end{array}
\]

\noindent
then the elements $b$ and $c$ should be merged, because $b=c$ holds in the model.
The axioms of equality make sure that $=$ is always interpreted as a congruence,
and that the merging is always possible because substituting $b$ for $c$ in any
predicate results in the same truth value.

\subsubsection*{Implementation in SAT/SMT Solvers}

Seemingly, using the aforementioned reduction to solve the satisfiability
problem for EPR requires a preprocessing phase where all ground instances
are generated. Since there are exponentially many such instances---where
the exponent is a quantifier ``depth'', that is, how many universal quantifiers
are nested---such preprocessing would inevitably require exponential time
even in the best case. Modern first-order solvers, however, manage to avoid
this thanks to the fact that in many cases, only a small number of these
is needed to find a refutation (and so prove that the formula is unsatisfiable).
The SMT solver Z3~\cite{de2008z3} uses \newterm{model-based quantifier instantiation}
(MBQI, explained e.g. in~\cite{CAV:GeM:2009}) where variables in quantifiers
are instantiated according to partial candidate models for the input formula.
This heuristic results in much less than the exponential number of
actual ground instances of the input formula, and leads to very good
running times in practice.

In the presence of an underlying theory, such as arithmetic or arrays,
the power of SMT (satisfiability modulo theory) solvers can be combined
to solve even more complicated formulas. Z3 also supports this form of
reasoning.
