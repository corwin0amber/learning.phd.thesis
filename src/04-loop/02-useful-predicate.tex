\seclabel{Loop:Useful-Predicate}

\begin{headlines}
We show one possible abstraction space that performs well for
linked data structures
{\blip}
The abstraction predicates are expressed as $\AFR$ formulas
\end{headlines}

\bigskip

In this section, we describe how PDR$_\AbsDomain(\Init,
\rho, \Bad)$ described in \algref{PDR} can be instantiated for
verifying linked-list programs.
The key insight is the use of the
reachability logics for expressing properties of linked
lists, as defined in~\secref{Pointer:Deterministic}.

We begin by presenting a set of properties that occur naturally when observing
programs manipulating linked lists. \tabref{Abstraction-Predicates} contains a list
of properties with an English description.

\begin{table}[t]
\begin{center}
$
\begin{array}{||l|l|c||}
\hhline{|t:===:t|}
\textbf{Name} & \textbf{Description} & \textbf{Mnemonic}\\
\hhline{||---||}
x = y   & \text{equality} & \\
\field{f}{x}{y} & x\texttt{->}f = y & \\
\reachField{f}{x}{y} & \text{an}~f~\text{path from}~x~\text{to}~y & \\
%\preachField{f}{x}{y} & \text{An}~f~\text{non-empty path from}~x~\text{to}~y & \\
\ls{f}{x}{y}  & \text{unshared}~f~\text{linked-list segment between}~x~\text{and}~y & \\
\hhline{||---||}
\alloc(x)   & x~\text{points to an allocated element} & A \\
\stable{f}{h}  & \text{any $f$-path from $h$ leads to an allocated element} & A \\
\hhline{||---||}
\reverse{f}{b}{x}{y}  & \text{reversed}~f/b~\text{linked-list segment between}~x~\text{and}~y & R \\
\hhline{||---||}
\sorted{f}{x}{y}  & \text{sorted}~f~\text{list segment between}~x~\text{and}~y  & S \\
\hhline{|b:===:b|}
\end{array}$
\end{center}
\caption{\tablabel{Abstraction-Predicates}%
  Predicates for expressing various properties of linked lists whose
  elements hold data values.
  $x$ and $y$ denote program variables that point to list elements or $\vnull$.
  $f$ and $b$ are parameters that denote pointer fields.
  (The mnemonics are referred to later in \tabref{Loop:Experiments}.)
}
\end{table}

We use two related logics for expressing properties of linked data structures.
One of them is $\AFR$, which already appeared as part of~\defref{Reach-Logics}.
The second is $\EAR$, in fact the dual of $\AER$ also defined there.
We will need $\rho$, the transition relation, to be $\EAR$ (as opposed to $\AER$)
because it is used in a positive context in SAT queries in \algref{PDR} and \algref{Reduce}.
An alternative set of rules for $\wlp{}$ that generate $\EAR$ formulas, and are closed under $\EAR$,
is given in~\tabref{EA-Wprules} below.

\begin{definition}\deflabel{EAR}\textbf{($\EAR$)}
(Extension for \defref{Reach-Logics})

\begin{itemize}
 \item \textbf{Exists-Forall Formulas with Reachability} ($\EAR$) are Boolean combinations of the atomic propositions
with additional quantifiers of the form $\exists^*\forall^*\!\!:\!\! \varphi$ where $\varphi$ is a $\QFR$ formula.
\end{itemize}
In particular, $\QFR \subset \AF \subset \EAR$. Also, if $\varphi\in\AER$, then $\lnot\varphi$ has an equivalent in $\EAR$, and vice versa.
\qed
\end{definition}


Technically, $\EAR$ forbids \emph{any}
use of an individual function symbol $f$; however, when $f$
defines an acyclic linkage chain---as in acyclic singly linked and
doubly linked lists---$f$ can be defined in terms of $f^*$
by using universal quantification to express that an element is
the closest in the chain to another element, as demonstrated by \equref{Inversion}.
However, because of the quantifier occurring on the
the right-hand side, this macro substitution can only be used
in a context that does not introduce a quantifier alternation
(e.g., if $f$ occurs in negative context inside a universal quantifier,
the resulting formula will not be $\EAR$).


\begin{table}[t]
\resizebox{\textwidth}{!}{
$\begin{array}{||l|l||}
\hhline{|t:==:t|}
\textbf{Name} & \textbf{Formula}\\
\hline
\field{f}{x}{y} & E_f(x, y)\\
\ls{f}{x}{y}  & \forall \alpha, \beta:
    \frtcBetween{x}{\alpha}{y} \land \frtc{\beta}{\alpha}
    \implies (\frtc{\beta}{x} \lor \frtc{x}{\beta}) \\
\hline
\stable{f}{h} & \forall \alpha: \frtc{h}{\alpha} \implies \alloc(\alpha) \\
\hline
\reverse{f}{b}{x}{y}  & \forall \alpha,\beta:
    \left(\begin{array}{c@{\hspace{.75ex}}l}
            & \alpha\neq\tnull \land \beta\neq\tnull \\
      \land & \frtcBetween{x}{\alpha}{y} \land \frtcBetween{x}{\beta}{y}
    \end{array}\right)
    \limplies \big(\frtc\alpha\beta \liff \brtc\beta\alpha\big)\\
\sorted{f}{x}{y}  & \forall \alpha,\beta:
    \left(\begin{array}{c@{\hspace{.75ex}}l}
            & \alpha\neq\tnull \land \beta\neq\tnull \\
      \land & \reachField{f}{x}{\alpha} \land  \reachField{f}{\alpha}{\beta} \land \reachField{f}{\beta}{y}
    \end{array}\right)
    \limplies \dle(\alpha,\beta) \\
\hhline{|b:==:b|}
\end{array}$
}
\vspace{.75ex}
{\caption{\tablabel{PredFormulas}%
\AFR\ formulas for the derived predicates shown in \tabref{Abstraction-Predicates}.
$f$ and $b$ denote pointer fields.
$\dle$ is an uninterpreted predicate that denotes a total order on the data values.
The intention is that $\dle(\alpha, \beta)$ holds whenever $\alpha\texttt{->}d \leq \beta\texttt{->}d$, where $d$ is the data field.
We assume that the semantics of $\dle$ are enforced by an appropriate total-order background theory.
}}
\end{table}

\subsubsection*{A Predicate Abstraction Domain that uses $\AFR$}
The abstraction predicates used for verifying properties of linked list programs
were introduced informally in \tabref{Abstraction-Predicates}.
\tabref{PredFormulas} gives the corresponding formal definition of
the predicates as $\AF$ formulas.
Note that all four predicates defined in \tabref{PredFormulas} are quantified.
(The quantified formula for $E_f$ is given in \equref{Inversion}.)
In essence, we use a template-based approach for obtaining quantified
invariants: the discovered invariants have a quantifier-free
structure, but the atomic formulas can be quantified $\AFR$ formulas.
Restricting the formulas in such a manner makes sure that the resulting
satisfiability checks in \algref{Reduce} would be of $\EAR$ formulas.

%\subsection{$\EAR$ Satisfies the Requirements of PDR algorithm}
\bigskip
We now show that the $\EAR$ logic satisfies requirements \itemref{Decidable} and
\itemref{ConcreteSemantics} for the PDR algorithm stated in \secref{Loop:Property}.


\subsubsection*{Decidability of $\EAR$}
To satisfy requirement \itemref{Decidable} stated in
\secref{Loop:Property}, we have to show that $\EAR$ is decidable for satisfiability.

$\EAR$ is decidable for satisfiability because any formula in this logic can
be translated into the ``effectively propositional'' decidable logic
of $\exists^*\forall^*$ formulas described by Piskac et al.\ \cite{JAR:PdMB10}.
$\EAR$ includes relations of the form $f^*$ (the reflexive transitive
closure of a function symbol $f$),
but only allows limited use of $f$ itself.

Every $\EAR$
formula can be translated into an $\exists^{*}\forall^{*}$ formula
using the the same steps as in~\secref{Pointer:Deterministic}: (i)~add a new
uninterpreted relation $R_f$, which is intended to represent reflexive
transitive reachability via $f$; (ii)~add the consistency rule
$\Tlinord$ shown in \tabref{Rtnext}, which asserts that $R_f$ is a
partial order, i.e., reflexive, transitive, acyclic, and
linear;\footnote{Note that the order is a partial order and not a
  total order, because not every pair of elements must be ordered.  }
and (iii)~replace all occurrences of $t_1 \B{f^*} t_2$ by $R_f(t_1,
t_2)$.  (By means of this translation step, acyclicity is built into
the logic.)

\begin{proposition}[Simulation of \EAR]
Consider \EAR\ formula $\varphi$ over vocabulary $\voc=\B{\constants, \functions, \relations}$.
Let $\varphi' \eqdef \varphi[R_f(t_1, t_2)/ t_1 \B{f^*} t_2]$.
Then $\varphi'$ is a first-order formula over vocabulary
$\voc'=\B{\constants, \emptyset, \relations \cup \{R_f \colon f \in \functions}$,
and $\Tlinord \land \varphi'$ is satisfiable if and
only if the original formula $\varphi$ is satisfiable.
\end{proposition}
This proposition is the dual of \proref{AER-simulation} for validity of $\forall^{*} \exists^{*}$ formulas.




\begin{table}[t]
\resizebox{\textwidth}{!}{
$\begin{array}{||l|l||}
\hhline{|t:==:t|}
\textbf{Command~C} & \wlp{C}(Q)\\
\hline
\texttt{assume}~\varphi  & \varphi \limplies Q\\
%\texttt{assert}~\varphi  & \varphi \land Q\\
\texttt{x = y} & \substitute{Q}{x}{y}\\
\texttt{x = y->f} & y \neq \tnull \land \exists \alpha:
                            (\fsingl y \alpha \land \substitute{Q}{x}{\alpha})\\
\texttt{x->f = null} &   x \neq \tnull \land \substitute{Q}{\alpha \B{f^*} \beta}{\alpha \B{f^*}\beta \land (\lnot \alpha \B{f^*}x \lor \beta \B{f^*}x)}\\
\texttt{x->f = y}  &  x \neq \tnull \land \substitute{Q}{\alpha \B{f^*} \beta}{\alpha \B{f^*}\beta \lor (\alpha \B{f^*}x \land y \B{f^*}\beta)}\\
\hline
\texttt{x = malloc()} & \exists \alpha: \neg \alloc(\alpha)  \land \substitute{Q}{\alloc(\beta))}{(\alloc(\beta) \lor (\beta = \alpha \land \beta = x))}\\
\texttt{free(x)} &  \alloc(x) \land \substitute{Q}{\alloc(\beta))}{(\alloc(\beta) \land \beta \neq x)}\\
\hhline{|b:==:b|}
\end{array}$
}
%
{\caption{\tablabel{EA-Wprules}%
A revised set of basic $\wlp{}$ rules for invariant inference.
$\fsingl y \alpha$ is the universal formula defined in \equref{Inversion}.
$\alloc$ stands for a memory location that has been allocated
and not subsequently freed.
}}
\end{table}

\subsubsection*{Axiomatic specification of concrete semantics in $\EAR$}
To satisfy requirement \itemref{ConcreteSemantics} stated in
\secref{Loop:Property}, we have to show that the transition relation for each
statement $\Cmd$ of the programming language can be expressed as a
two-vocabulary formula $\rho \in \EAR$, where each program state variable is represented
by two constants: $c$, representing the value of the variable in the pre-state,
and $c'$ representing the value of the variable in the post-state. Similarly, each pointer
field is represented by two binary relations $f*$ and $f'*$ denoting reachability along
$f$-paths in pre- and post-states, respectively.

Let $\wlp{\Cmd}(Q)$ be the
weakest liberal precondition of command $\Cmd$ with respect to
$Q \in \EAR$. 
Then, the transition formula for command $\Cmd$ is $\wlp{\Cmd}(\Identity)$, where $\Identity$ is a two-vocabulary formula that
specifies that the input and the output states are identical, i.e.,
\begin{equation*}
  \Identity \eqdef \Land_{c \in \constants} c = c' \land \Land_{f \in \functions} \forall \alpha, \beta: \alpha \B{f^*} \beta \impliesBothWays  \alpha \B{f'^*} \beta .
\end{equation*}

To show that the concrete semantics of linked list programs
can be expressed in $\EAR$, we have to prove that $\EAR$ is closed under
$\wlp$; that is, for all commands $\Cmd$ and $Q \in \EAR$, $\wlp{\Cmd}(Q) \in \EAR$.

\tabref{EA-Wprules} shows rules for computing $\wlpsym$ of atomic commands.
This is slightly revised from \tabref{Wprules} which generates $\exists^*\forall^*$
formulas instead of $\forall^*\exists^*$.
Note that pointer-related rules in \tabref{EA-Wprules} each include a
memory-safety condition to detect \tnull-dereferences.
For instance, the rule for ``\lstinline{x->f = y}'' includes
the conjunct ``$x \neq \tnull$'';
if, in addition, we wish to detect accesses to unallocated memory,
the rule would be extended with the conjunct ``$\alloc(x)$''.

The following lemma establishes the soundness and completeness of the
$\wlpsym$ rules.
\begin{lemma}
Consider a command $C$ of the form defined in \tabref{EA-Wprules}
and postcondition $Q$.
Then, $\sigma \models \wlp{C}(Q)$ if and only if the execution of $C$
on $\sigma$ can yield a state $\sigma'$ such that $\sigma' \models Q$.
\end{lemma}
This lemma is the dual of \theref{wlp-AE:sound-complete} 
(\appref{Proofs:Program}) 
for validity of $\forall^{*} \exists^{*}$ formulas.

\medskip

Consider a program with a single loop ``\texttt{while}~\Cond~\texttt{do}~\Cmd''.
\algref{PDR} can be used to prove whether or not a precondition $\Pre
\in \AF$ before the loop implies that a postcondition $\Post \in \AF$
holds after the loop, if the loop terminates: we supply \algref{PDR} with $\Init
\eqdef \Pre$, $\rho \eqdef \Cond \land \wlp{\Cmd}(\Identity)$ and
$\Bad \eqdef \neg \Cond \land \neg \Post$.
Furthermore, memory safety can be enforced on the loop body by setting
$\Bad \eqdef (\lnot \Cond \land \neg \Post) \lor (\Cond \land \neg
\wlp{\Cmd}(\true)$.  

