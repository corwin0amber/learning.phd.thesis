The literature on program analysis is vast, and the subject
of shape analysis alone has an extensive literature.
Thus, in this section we are only able to touch on a few pieces of
prior work that relate to the ideas used in this chapter.

\paragraph*{Predicate abstraction.}
Houdini \cite{FME:FL01} is the first algorithm of which we are aware
that aims to identify a loop invariant, given a set of predicates as
candidate ingredients.
However, Houdini only infers \emph{conjunctive} invariants from a
given set of predicates.
Santini \cite{ENTCS:TLLR13,UW-TR-1790:TLLR13} is a recent algorithm
for discovering invariants expressed in terms of a set of candidate
predicates.
Like our algorithm, Santini is based on full predicate abstraction
(i.e., it uses arbitrary Boolean combinations of a set of predicates),
and thus is strictly more powerful than Houdini.
Santini could make use of the predicates and abstract domain described
in this chapter;
however, unlike our algorithm, Santini would not be able to report
counterexamples when verification fails.
Other work infers quantified
invariants~\cite{PLDI:SrivastavaG09,DBLP:conf/tacas/HoderKV11} but
does not support the reporting of counterexamples.
Templates are used in many tools to define the abstract domains used
to represent sets of states, by fixing the form of the constraints
permitted.
Template Constraint Matrices \cite{DBLP:conf/vmcai/SankaranarayananSM05}
are based on inequalities in linear real arithmetic (i.e., polyhedra),
but leave the linear coefficients as symbolic inputs to the analysis.
The values of the coefficients are derived in the course of running
the analysis.
In comparison, a coefficient in our use of $\EAR$ corresponds to one
of the finitely many constants that appear in the program, and we
instantiated our templates prior to using PDR.

As mentioned in the beginning of this chapter, PDR meshes well with full predicate
abstraction: in effect, the analysis obtains the benefit of the
precision of the abstract transformers for full predicate abstraction,
without ever constructing the abstract transformers explicitly.
PDR also allows a predicate-abstraction-based tool to create
concrete counterexamples when verification fails.

\paragraph*{Abstractions based on linked-list segments.}
Our abstract domain is based on formulas expressed
in $\AF$, which has very limited capabilities to express properties
of stretches of data structures that are not pointed to by a program
variable.
This feature is similar to the self-imposed limitations on
expressibility used in a number of past approaches, including
(a) canonical abstraction \cite{TOPLAS:SRW02};
(b) a prior method for applying predicate abstraction to linked lists
\cite{VMCAI:MYRS05};
(c) an abstraction method based on ``must-paths'' between nodes that
are either pointed to by variables or are list-merge points
\cite{CAV:LIS06}; and
(d) domains based on separation logic's list-segment primitive
\cite{TACAS:DOY06,CAV:BCCDOWY07} (i.e., ``$\textrm{ls}[x,y]$'' asserts
the existence of a possibly empty list segment running from the node
pointed to by $x$ to the node pointed to by $y$).
Decision procedures have been used in previous work to compute the
best transformer for individual statements that manipulate linked
lists~\cite{TACAS:YRS04,POPL:PodelskiW10}.


\paragraph*{STRAND and elastic quantified data automata.}
Recently, Garg et al.\ developed methods for obtaining
quantified invariants for programs that manipulate linked lists
via an abstract domain of \emph{quantified data automata}
\cite{CAV:GLMN13,SAS:GMP13}.
To create an abstract domain with the right properties, they use a
weakened form of automaton---so-called \emph{elastic} quantified data
automata---that is unable to observe the details of stretches of data
structures that are not pointed to by a program variable.
(Thus, an elastic automaton has some of the characteristics
of the work based on linked-list segments described above.)
An elastic automaton can be converted to a formula in the
decidable fragment of the STRAND logic over lists \cite{SAS:MQ11}.

\paragraph*{Other work on IC3/PDR.}
Our work represents the first application of PDR to programs that
manipulate dynamically allocated storage.
We chose to use PDR because it has been shown to work extremely well
in other domains, such as hardware verification
\cite{VMCAI:Bradley11,FMCAD:EMB11}.
Subsequently, it was generalized to software model checking
for program models that use linear real arithmetic \cite{SAT:HB12}
and linear rational arithmetic \cite{CAV:CG12}.
Cimatti and Griggio \cite{CAV:CG12} employ a quantifier-elimination
procedure for linear rational arithmetic, based on an approximate
pre-image operation.
Our use of a predicate-abstraction domain allows us to obtain an
approximate pre-image as the unsat core of a single
call to an SMT solver (\lineref{refine:checkBackward} of \algref{Reduce}).
