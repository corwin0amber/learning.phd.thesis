\seclabel{Loop}
\label{SE:LOOP}

\published{cav:IBRST14}

\begin{headlines}
Direct Hoare verification has one critical flaw
{\blip}
A human programmer has to provide loop invariants for
the loops occurring in the program
{\blip}
The invariant not only has to hold at every iteration,
but also needs to be {\bf inductive}
{\blip}
It means that just by knowing the invariant holds at
iteration $i$, one can prove it would hold at $i+1$
{\blip}
It is often very difficult to come up with such a suitable loop invariant
{\blip}
Using recent techniques, though, some invariants can be inferred automatically
\end{headlines}

\bigskip

% 1. Describe the problem
As is well known by anyone who ever taught or practiced Hoare logic,
an especially intricate part of every Hoare proof, when the program in question
involves a loop, is to come up with the \newterm{loop invariant}.

The goal of our work is to automatically generate quantified
invariants for programs that manipulate
singly-linked and doubly-linked list
data structures.  For a correct program, the invariant generated
ensures that the program has no memory-safety violations,
such as null-pointer dereferences, and that
data structure invariants are preserved.  For a program in which it is
possible to have a memory-safety violation or for a data-structure
invariant to be violated, the algorithm produces a concrete
counterexample.

%%% Begin Motivating Example
\lstset{
  language=C,
  columns=fullflexible,
  basicstyle=\footnotesize\ttfamily,
  numbers=none,
  numberstyle=\scriptsize,
  stepnumber=1,
  breaklines=true,
}

To illustrate the problem and proposed solution, 
consult the example procedure \lstinline{insert}, 
shown in \figref{Insert}, which
inserts a new element pointed to by \lstinline{e} 
into the non-empty, singly-linked list pointed to by \lstinline{h}.
\lstinline{insert} 
is annotated with a pre-condition and a post-condition.

\begin{figure}[t]
\begin{lstlisting}[mathescape]
void insert(Node e, Node h, Node x) {
  $\textbf{Requires:~} h \neq \tnull \land \ntc{h}{x} \land \nrtc{x}{\tnull}  \land e \neq \tnull \land \nsingl{e}{\tnull} \land \neg \nrtc{h}{e}$
  $\textbf{Ensures:~} h \neq \tnull \land \nrtc{h}{e} \land \nsingl{e}{x} \land \nrtc{x}{\tnull}$
  Node p = h, q = null;
  while (p != x && p != null) {
    q = p; p = p.n;
  }
  q.n = e; e.n = p;
}
\end{lstlisting}
{\caption{\figlabel{Insert}%
  A procedure to insert the element pointed to by \lstinline{e} into
  the non-empty, (unsorted) singly-linked list pointed by \lstinline{h},
  just before the element \lstinline{x} (which must not be first).
  The while-loop uses the trailing-pointer idiom: \lstinline{q} is always one
  step behind \lstinline{p}.
}}
\end{figure}


The task of filling in an appropriate loop invariant for \lstinline{insert}
is not trivial because
(i) a loop invariant may be more complex than a program's
pre-condition or post-condition, and
(ii) it is infeasible to enumerate all the potential invariants
expressible as CNF formulas over a set of properties.
For instance, there are $6$ variables in \lstinline{insert}
(including $\vnull$),
so even if we are only interested in properties of the form $\nrtc x y$, 
there are $2^{6 \times 6}$ possible clauses that can be created as disjunctions of these atomic formulas.
Therefore, the number of candidate invariants that can be formulated
with these predicates is $2^{2^{6 \times 6}}$.
It would be infeasible to investigate them all explicitly.

Intuitively, an invariant for the while-loop in \lstinline{insert}
must assert that $q$ is one step behind $p$.
This is not true, however, for the first iteration where $q=\vnull$.
Consequently, we expect the loop invariant to look something like the
following:
%
\noindent
\begin{equation}
  \equlabel{Insert:ExpectedInvariant}
  \begin{array}{l}
    h \neq \tnull \land \ntc{h}{x} \land \nrtc{x}{\tnull}  \land e \neq \tnull \land \nsingl{e}{\tnull} \land \neg \nrtc{h}{e} \land{}\\
    (j = \tnull \limplies i = h) \land{}\\
    (j \neq \tnull \limplies \nrtc{h}{k} \land \nsingl{j}{i}) \land{} \\
    \nrtc{i}{x}
  \end{array}
\end{equation}

%%% End Motivating Example

%%\subsubsubsection{Analysis overview.}
The analysis shown here is based on 
\emph{property-directed reachability} \cite{VMCAI:Bradley11}.
It starts with the trivial invariant $\true$, which is repeatedly
refined until it becomes inductive.\footnote{An invariant $I$ is
  inductive at the entry to a loop if whenever the code of the loop
  body is executed on an arbitrary state that satisfies both $I$ and the
  loop condition, the result is a state that satisfies $I$.
}
On each iteration, a concrete counterexample to inductiveness is used
to refine the invariant by excluding predicates that are implied by
that counterexample.



Although in this chapter we mainly discuss
memory-safety properties and data-structure invariants, the technique
can be easily extended to other correctness properties (see
\secref{Loop:Empirical}).

To the best of our knowledge, our method represents the first
shape-analysis algorithm that is capable of (i)~reporting concrete
counterexamples, or alternatively (ii)~establishing that the abstraction
in use is not capable of proving the property in question.
This result is achieved by combining several existing ideas in a new
way:
\begin{itemize}
  \item
    The algorithm uses a predicate-abstraction domain \cite{CAV:GS97}
    in which quantified (closed) formulas
    express properties of singly and doubly linked
    lists, and serve as the abstraction predicates.
    In contrast to most recent work, which uses restricted forms of
    predicate abstraction --- such as Cartesian abstraction
    \cite{TACAS:BPR01} --- the following analysis uses full predicate abstraction
    (i.e., the abstraction uses arbitrary Boolean combinations of the predicates).
  \item
    The abstraction predicates and language semantics are expressed in
    the reachability logics defined earlier (\secref{Pointer:Deterministic}), which
    are decidable using a reduction to SAT.
  \item
    The algorithm is property-directed---i.e., its choices
    are driven by the memory-safety properties to be proven.
    In particular, the algorithm is based on IC3 \cite{VMCAI:Bradley11},
    which we here refer to as \emph{property-directed reachability} (PDR).
\end{itemize}

PDR integrates well with full predicate abstraction:
in effect, the analysis obtains the same precision as
the best abstract transformer for full predicate abstraction,
without ever constructing the transformers explicitly.
In particular, we cast PDR as a \emph{framework} that is parameterized on
\begin{itemize}
\item the logic $\LogicLang$ in which the semantics of
    program statements are expressed, and
  \item the finite set of predicates that define the abstract domain
    $\AbsDomain$ in which invariants can be expressed. An element of
    $\AbsDomain$ is an arbitrary Boolean combination of the
    predicates.
\end{itemize}
Furthermore, this PDR framework is \emph{relatively complete with respect
to the given abstraction}. That is, the analysis
is guaranteed to terminate and
either
(i)~verifies the given property, (ii)~generates a concrete
counterexample to the given property, or (iii)~reports
that the abstract domain is not expressive enough to establish
the proof.

Outcome (ii) is possible because the ``frame'' structure
maintained during PDR can be used to build a trace formula;
if the formula is satisfiable, the model can be presented to the
user as a concrete counterexample. Moreover, if the analysis
fails to prove the property or find a concrete counterexample (outcome (iii)),
then there is no way to express an inductive invariant that
establishes the property in question using a Boolean combination of
the abstraction predicates.  
Note that outcome (iii) is a much stronger guarantee than what other
approaches provide when they terminate with an ``unknown'' result.
For example, abstract interpretation techniques such as~\cite{kn:TVLASAS}
may fail to find an invariant even when an appropriate one exists in
the given abstract domain.

Key to instantiating the PDR framework for shape analysis was
the development of the $\AFR$ and $\AER$ logics for expressing properties of
linked lists in \secref{Pointer}. %In the PDR framework,
$\AFR$ is used to define abstraction predicates,
and $\AER$ is used to express the language semantics.
$\AER$ is a decidable, alternation-free fragment of first-order logic
with transitive closure ($\FOTC$).  When applied to list-manipulation
programs, atomic formulas of $\AFR$/$\AER$ can denote reachability relations
between memory locations pointed to by pointer variables, where
reachability corresponds to repeated dereferences of pointer fields such
as {\tt next} or {\tt prev}.
One advantage of $\AF$ is that it does not require
any special-purpose reasoning machinery:
an $\AF$ formula can be
converted to a formula in ``effectively propositional'' logic,
which can then be reduced to Boolean SAT solving.
That is, in
contrast to much previous work on shape analysis, our method makes use
of \emph{a general purpose SMT solver}, Z3~\cite{TACAS:deMB08}
 (rather than specialized
tools developed for reasoning about linked data structures, e.g.,
\cite{TOPLAS:SRW02,TACAS:DOY06,CAV:BCCDOWY07,SAS:GMP13}).

As we saw, the main restriction in $\AFR$ is that it allows the use of a relation
symbol $f^*$ that denotes the transitive closure of a function symbol
$f$, but only limited use of $f$ itself. Despite this restriction,
as a language for expressing invariants, $\AFR$
provides a fairly natural abstraction, which means that analysis
results should be understandable by non-experts (similar to
\figref{Counterexample-example}).\footnote{ By a
  ``non-expert'', we mean someone who has no knowledge of either the
  analysis algorithm,
or the abstraction techniques used inside the
  algorithm.  }

% 2. State your contributions
This formulation produces a new algorithm for shape analysis that either
(i)~succeeds, (ii)~returns a concrete counterexample, or
(iii)~returns an abstract trace showing that the abstraction in use is
 not capable of proving the property in question.
 
In this chapter of the thesis, the reader will find:
\begin{itemize}
  \item
    A description of a framework, based on the IC3 algorithm, for finding an inductive
    invariant in a certain logic fragment (abstract domain) that
    allows one to prove that a given pre-/post-condition holds or find
    a concrete counter-example to the property, or, in the case of a
    negative result, the information that there is no inductive
    invariant expressible in the abstract domain
    (\secref{Loop:Property}).
  \item
    An instantiation of the framework for finding invariants of
    programs that manipulate singly-linked or doubly-linked lists.
    This instantiation uses $\AFR$ to define a simple
    predicate-abstraction domain, and is the first application of PDR
    to establish quantified invariants of programs that manipulate
    linked lists (\secref{Loop:Useful-Predicate}).
  \item
    An empirical evaluation showing the efficacy of the PDR framework
    for a set of linked-list programs (\secref{Loop:Empirical}).
\end{itemize}

