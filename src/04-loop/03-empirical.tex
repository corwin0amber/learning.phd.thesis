\seclabel{Loop:Empirical}

\begin{headlines}
The PDR framework was implemented, parametric of the abstraction predicates
{\blip}
Instantiated with different subsets of predicates
{\blip}
Z3 was again used for generating models and unsat-cores
{\blip}
Tested on a set of programs with linked lists
{\blip}
Invariants were successfully generated for correct programs
{\blip}
We were also able to detect concrete bugs when introduced
\end{headlines}

\bigskip

\begin{table}[t]
\centering
  \resizebox{\textwidth}{!}{
  \begin{tabular}{||l @{\;\;}||@{\;} c|r|r|c|c || c|r|r|c|c ||}
    \hhline{|t:===========:t|}
    	\multirow{3}{*}{Benchmark}
             &  \multicolumn{5}{c||}{Memory-safety}  &  
                \multicolumn{5}{c||}{} \\
             &  \multicolumn{5}{c||}{+ data-structure integrity}  &  
                \multicolumn{5}{c||}{Additional properties} \\
    \hhline{||~||----------||}
               & $\AbsDomain$ & \multicolumn{1}{c|}{Time} & N & \# calls to Z3 & \# clauses
               & $\AbsDomain$ & \multicolumn{1}{c|}{Time} & N & \# calls to Z3 & \# clauses \\
    \hhline{||=#=|=|=|=|=#=|=|=|=|=||}
create                &     &    1.37 &     3 &    28 &     3 &    &    8.19 &     4 &    96 &     7 \\
delete                &     &   14.55 &     4 &    61 &     6 &    &    9.32 &     3 &    67 &     7 \\
deleteAll             &  A  &    6.77 &     3 &    72 &     6 & A  &   37.35 &     7 &   308 &    12 \\
filter                &     &    2.37 &     3 &    27 &     4 &    &   55.53 &     5 &    94 &     5 \\
insert                &     &   26.38 &     5 &   220 &    16 &    &   25.25 &     4 &   155 &    13 \\
prev                  &     &    0.21 &     2 &     3 &     0 &    &   11.64 &     4 &   118 &     6 \\
last                  &     &    0.33 &     2 &     3 &     0 &    &    7.49 &     3 &    41 &     4 \\
reverse               &     &    5.35 &     5 &   128 &     4 &    &  146.42 &     6 &   723 &    11 \\
sorted insert         &  S  &   41.07 &     3 &    48 &     7 & S  &   51.46 &     4 &   134 &    10 \\
sorted merge          &     &   26.69 &     4 &    87 &    10 & S  &  256.41 &     5 &   140 &    14 \\
make doubly-linked    &     &   18.91 &     3 &    44 &     5 & R  & 1086.61 &     5 &   112 &     8 \\
    \hhline{|b:===========:b|}
  \end{tabular}
  }
  \vspace{1.0ex}
  \caption{\tablabel{Loop:Experiments}Experimental results.
    Column $\AbsDomain$ signifies the set of predicates used
    (blank = only the top part of \tabref{Abstraction-Predicates}; 
    S = with the addition of the {\it sorted} predicate family;
    R = with the addition of the {\it rev} family;
    A = with the addition of the {\it stable} family, where $\alloc$
      conjuncts are added in $\wlpsym$~rules).
  	Running time is measured in seconds. N denotes the highest
  	index for a generated element $R[i]$.
  	The number of clauses refers to the inferred loop invariant.}
\end{table}

To evaluate the usefulness of the analysis algorithm, we applied it to
a collection of sequential procedures that manipulate singly and doubly-linked
lists (see \tabref{Loop:Experiments}).
For each program, we report the predicates used, the time (in seconds), the number of PDR
frames, the number of calls to Z3, and the size of the resulting inductive
invariant, in terms of the number of clauses.
All experiments were run on a 1.7GHz Intel Core i5 machine with 4GB of RAM, 
running OS X 10.7.5.
We used version 4.3.2 of Z3~\cite{TACAS:deMB08}, compiled for a 64-bit Intel architecture
(using gcc 4.2 and LLVM).


\begin{table}[t]
\centering
\resizebox{.96\textwidth}{!}{
\begin{tabular}{||@{\;}l@{\;\;}||@{\;\;}p{11cm}@{\;}||}
	\hhline{|t:==:t|}
       Benchmark & Property checked \\
	\hhline{||==||}
	create    &  Some memory location pointed to by {\tt x} (a global variable)
		         that was allocated prior to the call, is not reachable from 
		         the list head, {\tt h}. \\
	delete    &  The argument {\tt x} is no longer reachable from {\tt h}. \\
	deleteAll &  An arbitrary non-null element {\tt x} of the list becomes
	             non-allocated. \\
	filter    &  Two arbitrary elements {\tt x} and {\tt y} that satisfy the 
	             filtering criterion and have an {\tt n}-path between them,
	             maintain that path. \\
	insert    &  The new element {\tt e} is reachable from {\tt h} and is
	             the direct predecessor of the argument {\tt x}. \\
	last      &  The function returns the last element of the list. \\
	prev      &  The function returns the element just before {\tt x},
	             if one exists. \\
	reverse   &  If {\tt x} comes before {\tt y} in the input, then {\tt x}
	             should come after {\tt y} in the output. \\
	sorted insert  &  The list rooted at {\tt h} remains sorted. \\  
	make doubly-linked &
	          The resulting {\tt p} is the inverse of {\tt n} within the list rooted
	          at {\tt h}. \\
	\hhline{|b:==:b|}
\end{tabular}
}
\caption{\tablabel{AdditionalProps}Some correctness properties that 
  can be verified by the analysis procedure.
  For each of the programs, we have defined suitable $\Pre$ and $\Post$
  formulas in $\AFR$.}
\end{table}

For each of the benchmarks, we verified that the program avoids
\tnull-dereferences, as well as that it preserves the data-structure
invariant that the inputs and outputs are acyclic linked-lists.
In addition, for some of the benchmarks we were also able to verify
some additional correctness properties.
While full functional correctness, or even partial correctness, is
hard to achieve using predicate abstraction, we were able to use
simple formulas to verify several interesting properties that go
beyond memory-safety properties and data-structure invariants.
\tabref{AdditionalProps} describes the properties we checked for the
various examples.
As seen from columns 3, 4, 8, and 9 of the entries for
\lstinline{delete} and \lstinline{insert} in \tabref{Loop:Experiments},
trying to prove {\em stronger} properties can sometimes result in {\em
fewer} iterations being needed, resulting in a {\em shorter} running
time.
In the remainder of the examples, handling additional properties
beyond memory-safety properties and data-structure invariants
required more processing effort, which can be attributed mainly to the larger
set of symbols (and hence predicates) in the computation.

\begin{table}[t]
\centering
\resizebox{1.0\textwidth}{!}{
\begin{tabular}{||l || p{6.5cm} || r|r|c|c ||}
  \hhline{|t:======:t|}
   & & \multicolumn{4}{c||}{Automatic bug finding} \\
  \hhline{||~~----||}
  Benchmark & Bug description & 
    \multicolumn{1}{l|}{Time} & N & \# calls to Z3 & c.e. size \\
  \hhline{||======||}
  insert & Precondition is too weak (omitted $e\neq\tnull$)
                                      & 4.46 & 1 & 17 & 8 \\
  filter & Potential \tnull\ dereference
                                      & 6.30 & 1 & 21 & 3 \\
         & Typo: list head used instead of list iterator
                                      & 103.10 & 3 & 79 & 4 \\
  reverse & Corrupted data structure: a cycle is created
                                      & 0.96 & 1 & 9 & 2 \\
  \hhline{|b:======:b|}
\end{tabular}
}
\caption{\tablabel{BugFinding}Results of experiments
  with buggy programs.
  Running time is measured in seconds. N denotes the highest
  index for a generated element $R[i]$.
  ``C.e.\ size'' denotes the largest number of individuals
  in a model in the counterexample trace.}
\end{table}

\subsubsection*{Bug Finding}
We also ran our analysis on programs containing deliberate bugs, to
demonstrate the utility of this approach to bug finding.
In all of the cases, the method was able to detect the bug and generate
a concrete trace in which the safety or correctness properties are violated.
The output in that case is a series of concrete states $\sigma_0 .. \sigma_N$
where each $\sigma_i$ contains the set of heap locations, pointer references,
and program variables at step $i$.
%
The experiments and their results are shown in \tabref{BugFinding}.
We found both the length of the trace and the size of the heap structures to
be very small.
Their small size makes the traces useful to present to a human programmer,
which can help in locating and fixing the bug.

\subsubsection*{Observations}
It is worth noting that for programs where the proof of safety is trivial---because
every access is guarded by an appropriate conditional check, such as in
\lstinline{prev} and \lstinline{last}---the algorithm terminates almost 
immediately with the correct invariant $\true$.
This behavior is due to the property-directedness of the approach,
in contrast with abstract interpretation, which always tries to find
the least fixed point, regardless of the desired property.

We experimented with different refinements of 
inductive-generalization (\secref{Loop:Property:Optimization}).
Our algorithm could in many cases succeed even without minimizing the unsat-core,
but we observed runs with up to $N=40$ iterations.
On the other hand, the more advanced versions of inductive generalization
did not help us: trying to remove literals resulted in a large number of
expensive (and useless) solver calls; and blocking spurious counter-examples
using inductive generalization also turned out to be quite expensive in our
setting. 

We also noticed that the analysis procedure is sensitive to the number
of abstraction predicates used.
In particular, using predicates whose definitions involve quantifiers
can affect the running time considerably.
When the predicate families $\sorted{f}{x}{y}$ and $\reverse{f}{b}{x}{y}$
are added to $\AbsDomain$, running times can increase substantially
(about 20-60 times), as the space of formulas grows much larger.
This effect occurred even in the case of \lstinline{sorted merge}, where we did
not attempt to prove an additional correctness property beyond safety and
integrity---and indeed there were no occurrences of the added predicates
in the loop invariant obtained.
As can be seen from \tabref{Loop:Experiments}, the PDR algorithm \emph{per se}
is well-behaved, in the sense that the number of calls to Z3
increased only modestly with the additional predicates.
However, each call to Z3 took a lot more time.

In \lstinline{make doubly-linked}, proving the {\tt prev}/{\tt next}-inverse 
property requires the predicate family $\reverse{f}{b}{x}{y}$
(see \tabref{PredFormulas}), resulting in a slowdown of more than twenty times
over just proving safety and integrity (without the use of the
 $\reverse{f}{b}{x}{y}$ predicates).
For \lstinline{sorted merge}, we did not prove an additional correctness
property beyond safety and integrity;
the numbers reported in columns 6--9 are for proving safety and integrity
when the predicate family $\sorted{f}{x}{y}$ is also included.
These predicates are not needed to prove safety and integrity, nor did
they show up in the resulting invariant;
however, the analysis time was more than sixty times slower than was
needed to prove safety and integrity without the additional
predicates, and two orders of magnitude slower than \lstinline{delete}
and \lstinline{insert}.
As can be seen from column 4, about the same number of calls to Z3
were performed for all four examples;
however, in the case of (the extended) \lstinline{sorted merge} and
\lstinline{make doubly-linked}{}, each call to Z3 took a lot more time.
