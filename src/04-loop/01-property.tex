\seclabel{Loop:Property}

\begin{headlines}
The IC3 algorithm originates from the field of Model Checking
{\blip}
Originally designed to find invariants in a finite state space,
it was shown to explore the state space efficiently
{\blip}
One of its prominent traits is being {\emph property-directed}
{\blip}
It focuses on the property of the reachable state set that is of
interest to verify
{\blip}
In cases where the state space is infinite, an abstraction is
employed
{\blip}
The result is a full (non-Cartesian) predicate analysis
\end{headlines}

\bigskip

%\newcommand\afterSteps[2]{(#1)^{\overset{#2}{\overbrace{''\cdots'}}}}
\newcommand\afterSteps[2]{{(#1)'}^{\scriptscriptstyle\times #2}}


\begin{figure}[t]
{\small
\begin{tabular}{c@{\hspace{2.0ex}}c}
  \begin{tabular}[t]{c}
  \begin{minipage}{2.5in}
    \begin{algorithm}[H]
      \caption{PDR$_\AbsDomain (\Init, \rho, \Bad)$}
      \alglabel{PDR}
      $R[-1] \assign \false$\linelabel{PDR:initR-1}\;
      $R[0] \assign \true$\linelabel{PDR:initR0}\;
      $N \assign 0$\linelabel{PDR:initN}\;
      \While{$\trueKw$\linelabel{PDR:beginWhile}}{
        \If{$\ThereExistsKw\ 0 \leq i<N$ $~~~~~~~~~~\SuchThatKw\ R[i] = R[i+1]$\linelabel{PDR:checkInduction}}
        { \Return{$\validKw$}\linelabel{PDR:returnValid}\;  }
        $(r, A) \assign \CheckKw(\Bad, R[N]) $\linelabel{PDR:checkConsecution}\;
        \If {$r = \unsatKw$}  {
          $N \assign N + 1$\linelabel{PDR:incN}\;
          $R[N] \assign \true$\linelabel{PDR:initRN}\;
        }
        \Else {
          reduce$_\AbsDomain(N, A)$\linelabel{PDR:callRefine}\;
        }
      }% end while
    \end{algorithm}
  \end{minipage}
  \end{tabular}
  &
  \begin{tabular}[t]{c}
  \begin{minipage}{2.7in}
    \begin{algorithm}[H]
      $(r, A_1) \assign \CheckKw(\Init, A)$\linelabel{refine:checkInit}\;
      \If {$r = \satKw$\linelabel{refine:initSat}}  {
        $\sigma \assign \ModelKw(\Init \land \rho^{N-j} \land \afterSteps{\Bad}{(N-j)})$\linelabel{refine:callModel}\;
        \lIf {$\sigma\ \isKw\ \NoneKw\ $}  {
          $\errorKw$ ``abstraction failure''\linelabel{refine:abstractionFailure}
        }
        \lElse {
          $\errorKw$ ``concrete counterexample$(\sigma)$''\linelabel{refine:concreteCounterexample}
        }

      }
      \While {$\trueKw$\linelabel{refine:beginWhile}}  {
        $(r, A_2) \assign$ \\ $\quad\CheckKw( (Init)' \lor (R[j-1] \land  \rho), (A)')$\linelabel{refine:checkBackward}\;
        \lIf {$r = \unsatKw$} {\breakKw\linelabel{refine:backwardUnsat}}
        \lElse {  reduce$_\AbsDomain(j-1, A_2)$\linelabel{refine:callRefine} }
      }
      \For{$i=0 \ldots j$\linelabel{refine:beginFor}}{
        $R[i] \assign R[i] \land (\lnot A_1 \lor \lnot A_2)$\linelabel{refine:conjoinRefinement}
      }
      \caption{reduce$_\AbsDomain(j, A)$}
      \alglabel{Reduce}
    \end{algorithm}
  \end{minipage}
  \end{tabular}
\end{tabular}
}%end small
\end{figure}


In this section, we present an adaptation of the IC3 algorithm 
(``Incremental Construction of Inductive Clauses for Indubitable Correctness''~\cite{VMCAI:Bradley11})
that uses predicate abstraction. In this chapter, by
\emph{predicate abstraction} we mean the technique that performs
verification using a given \emph{fixed} set of abstraction
predicates~\cite{POPL:FQ02}, and not techniques that incorporate
automatic refinement of the abstraction predicates; e.g. CEGAR.
The PDR algorithm shown in \algref{PDR} is parameterized
by a given finite set of predicates $\AbsPreds$ expressed
in a logic $\LogicLang$. The requirements on the logic $\LogicLang$ are:
% Custom list for listing requirements ``R1'', ``R2'', etc
\newlist{requirements}{enumerate}{2}
\setlist[requirements]{label=R\arabic*,leftmargin=*, widest=00}
%
\begin{requirements}
  \item \itemlabel{Decidable}
    $\LogicLang$ is decidable (for satisfiability).
  \item \itemlabel{ConcreteSemantics}
    The transition relation for each statement of the programming language
    can be expressed as a two-vocabulary $\LogicLang$ formula.
\end{requirements}
Then for a particular program, we are given:
\begin{itemize}
\item A finite set of predicates $\AbsPreds = \{p_i \in
    \LogicLang\}, 1\leq i\leq n$.
\item The transition relation of the system as a two-vocabulary
    formula $\rho \in \LogicLang$.
\item The initial condition of the system, $\Init \in \LogicLang$.
\item The formula specifying the set of bad states, $\Bad \in
    \LogicLang$.
\end{itemize}

 Let $\AbsDomain$ be the full predicate abstraction domain over the
 predicates $\AbsPreds$.  That is, each element $A \in \AbsDomain$ is
 an \emph{arbitrary} Boolean combination of the predicates
 $\AbsPreds$.  $A\in \AbsDomain$ is inductive with respect to $\Init$
 and $\rho$ if and only if $\Init \implies A$ and $A \land \rho
 \implies (A)'$.  $(\varphi)'$ renames the vocabulary of constant
 symbols and relation symbols occurring in $\varphi$ from $\{c,
 \ldots, r, \ldots \}$ to $\{c', \ldots, r', \ldots \}$. $\varphi$ is
 $(\varphi)'$ stripped of primes.

If the logic $\LogicLang$ is
propositional logic, then \algref{PDR} is an instance of IC3 from~\cite{VMCAI:Bradley11}.
Our presentation is a simplification of more advanced variants~%
\cite{VMCAI:Bradley11,FMCAD:EMB11,SAT:HB12}.
For instance, the presentation omits inductive generalization, although
our implementation does
implement inductive generalization (see \secref{Loop:Empirical}).
Furthermore, this simplified presentation brings out the fact that the
PDR algorithm is really an analysis \emph{framework} that is
parameterized on the set of abstraction predicates $\AbsPreds$.


The algorithm employs an unbounded array $R$, where each \emph{frame}
$R[i] \in \AbsDomain$ over-approximates the set of concrete states
after executing the loop at most $i$ times.  The algorithm maintains
an integer $N$, called the \emph{frame counter}, such that the
following invariants hold for all $0 \leq i < N$:
\begin{enumerate}
\item $\Init$  is a subset of all $R[i]$, i.e., $\Init \implies R[i]$.
\item The safety requirements are satisfied, i.e., $R[i] \implies \neg \Bad$.
\item Each of the $R[i+1]$ includes the states in $R[i]$, i.e., $R[i]
  \implies R[i+1]$.
\item The successors of $R[i]$ are included in $R[i+1]$, i.e., for all
  $\sigma, \sigma'$ if $\sigma \models R[i]$ and $\B{\sigma, \sigma'}
  \models \rho$, then $\sigma' \models R[i+1]$.
\end{enumerate}

We illustrate the workings of the algorithm using a simple example,
after which we explain the algorithm in detail.
\begin{example}
Consider the program
{\tt while (x != y) x = x.n;}
where {\tt n} is a pointer field (replaces {\tt next} for brevity).
Suppose the precondition is
$\Init := y\neq\vnull \land \ntc x y$.
We wish to prove the absence of null-dereference;
that is, $\Bad := x\neq y \land x=\vnull$.

\tabref{simple-run} shows a trace of PDR running
with this input; each line represents a SAT query
carried out by PDR$_\AbsDomain$ (\lineref{PDR:checkConsecution})
or by reduce$_\AbsDomain$ (\lineref{refine:checkBackward}).
The predicate abstraction domain $\AbsDomain$ comprises of equalities
and $\nrtc{}{}$-paths.
At each stage, if the result ($r$) is ``\unsatKw'', then either we
unfold one more loop iteration ($N := N + 1$) or we learn a new
clause to add to $R[j]$ of the previous step, as marked by the ``$\nearrow$'' symbol.
If the result is ``\satKw'', the resulting model is used to further
refine an earlier clause by recursively calling reduce$_\AbsDomain$.

On the first row, we start with $R[0]=\true$, so definitely 
$R[0]\land \Bad$ is satisfiable, for example with a model
where $x=y=\vnull$. The algorithm checks if this model represents
a reachable state at iteration 0 (see the second row), 
and indeed it is not---the result is ``\unsatKw'' and the
unsat-core is $x=\vnull$ ($\Init\land x=\vnull$ is not satisfiable).
Therefore, we infer the negation, $x\neq\vnull$, and add that to $R[0]$.
The algorithm progresses in the same manner---e.g., four lines later, we have
$R[0]=(x\neq\vnull \land x\neq y)$, and so on.
Eventually, the loop terminates when $R[i] = R[i+1]$ for some $i$;
in this example, the algorithm terminates because $R[1] = R[2]$. 
The resulting invariant is $R[2] \equiv (y\neq\vnull \land \nrtc x y)$,
a slight generalization of $Pre$ in this case.
\qed
\end{example}


\begin{table}[t]
\newcommand\unsat{\textbf{unsat}}
\resizebox{\textwidth}{!}{%
$
\begin{array}{|r|l|l|l|l|}
\hline
N & \textbf{Formula} & \textbf{Model} & A := \beta_\AbsDomain({Model}) & \textbf{Inferred} \\[0.2em]
\hline
0   & R[0] \land \Bad    & (\vnull, 1)~1\mapsto \vnull   & A := x = \vnull \land x \neq  y \land \lnot\nrtc x y \land \nrtc y x  &  \\
0   & ((\Init)' \lor (R[-1] \land \rho)) \land (A)'   & \unsat    & & R[0] \models x \neq \vnull \\
0   & R[0] \land \Bad    & \unsat    & & \\
1   & R[1] \land \Bad    & (\vnull, 1)~1\mapsto \vnull   & A := x=\vnull \land x\neq y \land \lnot\nrtc x y \land \nrtc y x  &  \\
1   & ((\Init)' \lor (R[0] \land \rho)) \land (A)'    & (1, 1)~1\mapsto \vnull    & A := x = y \neq  \vnull \land \nrtc x y \land \nrtc y x   &  \\
1   & ((\Init)' \lor (R[-1] \land \rho)) \land (A)'   & \unsat    & & R[0]\models x \neq y  \\
1   & R[1] \land \Bad    & (\vnull, 1)~1\mapsto \vnull   & A := x=\vnull \land x\neq y \land \lnot\nrtc x y \land \nrtc y x  &  \\
1   & ((\Init)' \lor (R[0] \land \rho)) \land (A)'    & (1, 2)~1,2\mapsto \vnull  & A := x\neq y \land x,y\neq \vnull \land \lnot\nrtc x y \land \lnot\nrtc y x   &  \\
1   & ((\Init)' \lor (R[-1] \land \rho)) \land (A)'   & \unsat    & & R[0]\models \nrtc x y \\
1   & R[1] \land \Bad    & (\vnull, 1)~1\mapsto \vnull   & A := x=\vnull \land x\neq y \land \lnot \nrtc x y \land \nrtc y x &  \\
1   & ((\Init)' \lor (R[0] \land \rho)) \land (A)'    & \unsat    & & R[1]\models\nrtc x y \\
1   & R[1] \land \Bad    & \unsat    & & \\
2   & R[2] \land \Bad    & (\vnull, 1)~1\mapsto \vnull   & A := x=\vnull \land x\neq y \land \lnot\nrtc x y \land \nrtc y x  &  \\
2   & ((\Init)' \lor (R[1] \land \rho)) \land (A)'    & \unsat    & & R[2]\models\nrtc x y \\
2   & R[1] = R[2]  & \textbf{valid}     &  &    \\
\hline
\end{array}
$}
\vspace{1mm}
\caption{\tablabel{simple-run}
 Example run with $\Init := y\neq\vnull \land \ntc x y$,~
 $\Bad := x\neq y\land x=\vnull$,
 and $\rho := (x'=n(x))$.
 Intermediate counterexample models are written as $(x,y)\,E$ where
 $(x,y)$ is the interpretation of the constant symbols $x$,$y$ and $E$ are the $n$-links.
 The output invariant is $R[1] = R[2] = \nrtc x y$.}
\end{table}

%The output of PDR$\langle \AbsPreds \rangle (\Init, \rho, \Bad)$ is either:
%\begin{enumerate}
%\vspace{-1ex}
%\item \itemlabel{valid}
%  $\validKw$, if there exists $A \in \AbsDomain$ such that (i)~$A$ is
%  inductive, and (ii)~$A \implies \lnot \Bad$,

%\item \itemlabel{concrete}
%  a concrete counterexample trace, which shows that the property is violated, or

%\item \itemlabel{completeness}
%  an abstract trace, if the inductive invariant required to prove the
%  property cannot be expressed as an element of $\AbsDomain$.
%\end{enumerate}

%% In this section, we present a generalization of the PDR algorithm for
%% infinite state systems.  The approach we take is to use a finitary
%% abstraction based on finite set of predicates $\AbsPreds$ expressed in
%% a logic $\LogicLang$; that is, $\AbsPreds = \{p_i \in \LogicLang\},
%% 1\leq i\leq n$. The assumptions on the logic $\LogicLang$ are:
%% \begin{Assumptions}
%% \item \itemlabel{DeciableLogic}
%%   The logic $\LogicLang$ is decidable.

%% \item \itemlabel{ConcreteSemantics} The concrete semantics of the
%%   system to be verified are expressible axiomatically in
%%   $\LogicLang$. In particular, the transition relation $\rho$ is
%%   expressible as a two-vocabulary formula in $\LoicLang$.

%% \item \itemlabel{Conditions} Furthermore, the initial condition of the
%%   system, $\Init$, and the formula specifying the set of bad states,
%%   $\Bad$, are expressible in $\LogicLang$.
%% \end{Assumptions}

\noindent
Some terminology used in the PDR algorithm:
%
\vspace{-0.5em}
\begin{itemize}
\item $\ModelKw(\varphi)$ returns a model $\sigma$ satisfying $\varphi$ if
it exists, and $\NoneKw$ if it doesn't.

\item
The abstraction of a model $\sigma$, denoted by $\beta_\AbsDomain(\sigma)$,
is the cube of predicates from $\AbsPreds$ that hold in $\sigma$:
$\beta_\AbsDomain(\sigma) = \bigwedge \{ p \mid \sigma \models p, p \in \AbsPreds \} \wedge \bigwedge \{ \lnot q \mid \sigma \models \lnot q, q \in \AbsPreds \}$.
\item
Let $\varphi \in \LogicLang$ is a formula in the unprimed vocabulary, $A\in \AbsDomain$ is
a value in the unprimed or primed vocabulary.
$\CheckKw(\varphi, A)$ returns a
pair $(r, A_1)$ such that
\begin{itemize}
\item if $\varphi \land A$ is satisfiable, then $r = \satKw$ and
  $A_1$ is the abstraction of a concrete state in the unprimed vocabulary.
  That is, if the given $A$ is in the unprimed vocabulary, 
  then  $\beta_\AbsDomain(\sigma)$ for some $\sigma \models \varphi \land A$;
  else if $A$ is in the primed vocabulary, 
  then $A_1 = \beta_\AbsDomain(\sigma)$ for some $(\sigma, \sigma') \models \varphi \land A$.
\item if $\varphi \land A$ is unsatisfiable, then $r = \unsatKw$, and
  $A_1$ is a predicate such that $A \implies A_1$ and $\varphi \land
  A_1$ is unsatisfiable.  The vocabulary of $A_1$ is the same as that
  of $A$.
  If $A$ is in the primed vocabulary (as in \lineref{refine:checkBackward} of
  \algref{Reduce}), $\CheckKw$ drops the
  primes from $A_1$ before returning the value.
\end{itemize}
A valid choice for $A_1$ in the unsatisfiable case would be
$A_1 = A$ (and indeed the algorithm would still be correct), but
ideally $A_1$ should be the weakest such predicate.
For instance, $\CheckKw(\false, A)$ should return $(\unsatKw, \true)$.
In practice, when $\varphi \land A$ is unsatisfiable, the $A_1$
returned is an unsat core of $\varphi \land A$ constructed exclusively
from conjuncts of $A$.
Such an unsat core is a Boolean combination of predicates in $\AbsPreds$,
and thus is an element of $\AbsDomain$.
\end{itemize}

We now give a more detailed explanation of \algref{PDR}.
Each $R[i], i\geq 0$ is
initialized to $\true$ (\linerefs{PDR:initR0}{PDR:initRN}), and
$R[-1]$ is $\false$. $N$ is initialized to $0$ (\lineref{PDR:initN}).
At \lineref{PDR:checkInduction}, the algorithm checks whether $R[i] =
R[i+1]$ for some $0\leq i < N$.  If true, then an inductive invariant
proving unreachability of $\Bad$ has been found, and the algorithm
returns $\validKw$ (\lineref{PDR:returnValid}).

At \lineref{PDR:checkConsecution}, the algorithm checks whether $R[N]
\land \Bad$ is satisfiable. If it is unsatisfiable, it means that $R[N]$ excludes the states
described by $\Bad$, and the frame counter $N$ is incremented
(\lineref{PDR:incN}). Otherwise, $A \in \AbsDomain$ represents an
abstract state that satisfies $R[N] \land \Bad$. PDR then attempts to
reduce $R[N]$ to try and exclude this abstract
counterexample by calling reduce$_\AbsDomain(N,A)$ (\lineref{PDR:callRefine}).

The reduce algorithm (\algref{Reduce}) takes as input an integer $j,
0\leq j \leq N$, and an abstract state $A \in \AbsDomain$ such that
there is a path starting from $A$ of length $N-j$ that reaches
$\Bad$. \algref{Reduce} tries to strengthen $R[j]$ so as to exclude $A$.
At \lineref{refine:checkInit}, reduce first checks whether
$\Init \land A$ is satisfiable. If it is satisfiable, then
there is an abstract trace of length $N-j$ from $\Init$ to $\Bad$,
using the transition relation $\rho$.
The call to $\ModelKw$ at
\lineref{refine:callModel} checks whether there exists a concrete
model corresponding to the abstract counterexample.
$\rho^k$ denotes $k$ unfoldings of the transition relation $\rho$;
$\rho^0$ is $\true$.
$\afterSteps{\Bad}{k}$ denotes $k$ applications of
the renaming operation $(\cdot)'$ to $\Bad$.
If no such concrete model is found, then the
abstraction was not precise enough to prove the required property
(\lineref{refine:abstractionFailure});
otherwise, a concrete counterexample to the property is returned
(\lineref{refine:concreteCounterexample}).

Now consider the case when
$\Init \land A$ is unsatisfiable on \lineref{refine:checkInit}. $A_1
\in \AbsDomain$ returned by the call to $\CheckKw$ is such that $\Init
\land A_1$ is unsatisfiable; that is, $\Init \implies \lnot A_1$.

The while-loop on \lineseqref{refine:beginWhile}{refine:callRefine}
checks whether the $(N-j)$-length path to $\Bad$ can be
extended backward to an $(N-j+1)$-length path.
In particular, it checks whether $R[j-1] \land \rho \land (A)'$ is satisfiable.
If it is satisfiable, then the algorithm calls reduce recursively on $j-1$ and
$A_2$ (\lineref{refine:callRefine}).
If no such backward extension is possible, the algorithm exits the while loop
(\lineref{refine:backwardUnsat}).
Note that if $j=0$, $\CheckKw(R[j-1] \land \rho, A)$ returns
$(\unsatKw, \true)$, because $R[-1]$ is set to $\false$.

The conjunction of $(\lnot A_1 \lor \lnot A_2)$ to $R[i], 0\leq i \leq j$,
in the loop on \lineseqref{refine:beginFor}{refine:conjoinRefinement}
eliminates abstract counterexample $A$ while preserving the required invariants
on $R$.
In particular, the invariant $\Init \implies R[i]$ is maintained
because $\Init \implies \lnot A_1$, and hence
$\Init \implies (R[i] \land (\lnot A_1 \lor \lnot A_2))$.
Furthermore, $A_2$ is the abstract state from which there is a
(spurious) path of length $N-j$ to $\Bad$.
By the properties of $\CheckKw$, $\lnot A_1$ and $\lnot A_2$ are
each disjoint from $A$, and hence $(\lnot A_1 \lor \lnot A_2)$ is
also disjoint from $A$.
Thus, conjoining $(\lnot A_1 \lor \lnot A_2)$ to $R[i], 0\leq i \leq j$
eliminates the spurious abstract counterexample $A$.
Lastly, the invariant $R[i] \implies R[i+1]$ is
preserved because $(\lnot A_1 \lor \lnot A_2)$ is conjoined to all
$R[i], 0\leq i\leq j$, and not just $R[j]$.



Formally, the output of PDR$_\AbsDomain (\Init, \rho, \Bad)$ is
captured by the following theorem:
\begin{theorem}
\thelabel{PDR}
Given (i)~the set of abstraction predicates $\AbsPreds = \{p_i \in
\LogicLang\}, 1\leq i\leq n$ where $\LogicLang$ is a decidable logic,
and the full predicate abstraction domain $\AbsDomain$ over
$\AbsPreds$, (ii)~the initial condition $\Init \in \LogicLang$,
(iii)~a transition relation $\rho$ expressed as a two-vocabulary
formula in $\LogicLang$, and (iv)~a formula $\Bad \in \LogicLang$
specifying the set of bad states, $PDR_\AbsDomain(\Init,
\rho, \Bad)$ terminates, and reports either
\begin{enumerate}
\item \itemlabel{valid}
  $\validKw$ if there exists $A \in \AbsDomain$ s.t. 
  (i)~$\Init\limplies A$, (ii)~$A$
  is inductive, and (iii)~$A \implies \lnot \Bad$,
\item \itemlabel{concrete}
  a concrete counterexample trace, which reaches a state satisfying $\Bad$, or
\item \itemlabel{completeness}
  an abstract trace, if the inductive invariant required to prove the
  property cannot be expressed as an element of $\AbsDomain$.
\qed
\end{enumerate}
\end{theorem}

The proof of \theref{PDR} in \appref{Proofs:Relative}
is based on the observation that,
when ``abstraction failure'' is reported by reduce$_\AbsDomain(j,A)$, 
the set of models $\sigma_i\models R[i]$ ($j\leq i<N$)
represents an abstract error trace.


\subsubsection*{Optimization}
\seclabel{Loop:Property:Optimization}

There are some optimizations that contribute to reducing the number of iterations required
by \algref{PDR}. We describe one of them in brief and refer to additional material.

\begin{paragraph}{Inductive Generalization.}
Each $R[i]$ is a conjunction
of clauses $\varphi_1\land \cdots \land \varphi_m$.
If we detect that some $\psi_j$ comprising a subset of literals of $\varphi_j$,
it holds that $R[i]\land \rho \land \psi_j \models (\psi_j)'$, then
$\psi_j$ is {\em inductive relative to} $R[i]$.
In this case, it is safe to conjoin $\psi_j$ to $R[k]$ for $k \leq i + 1$.
Spurious counter-examples can also be purged if they are inductively blocked.
The advantages of this method are explained thoroughly by Bradley~\cite{VMCAI:Bradley11}.
\end{paragraph}

\subsubsection*{Comparison to Other Predicate-Abstraction Approaches}
\seclabel{Loop:Property:OtherApproaches}
Our approach differs in two ways from earlier approaches that perform
verification via predicate abstraction; that is, verification using
a given \emph{fixed} set of abstraction predicates~\cite{POPL:FQ02}.
  First, our approach is
\emph{property directed}.  Our PDR algorithm does not aim to find the
strongest invariant expressible in $\AbsDomain$, instead the algorithm
tries to find an invariant that is \emph{strong enough} to verify the
given property. Hence, the PDR algorithm could be more efficient in
practice, depending on the property to be verified. Second, the PDR
algorithm fails to verify the property or provide a concrete
counter-example only when it is not possible to express the required
inductive invariant as a Boolean combination of the given abstraction
predicates $\AbsPreds$. Most approaches to predicate abstraction use
an approximation of the best abstract transformers.  As a consequence,
earlier approaches might not necessarily find the inductive invariant
even if it is expressible in $\AbsDomain$.  On the other hand,
approaches that use the best abstract transformers are able to compute
the strongest inductive invariant expressible in $\AbsDomain$
\cite{VMCAI:RSY04,ISSTA:YBS06,SAS:TER12,ENTCS:TLLR13}, but are not
property directed.
Furthermore, the frame structure
maintained by the PDR algorithm allows it to find concrete
counterexamples.
In contrast, other approaches to verification via
predicate abstraction only return an abstract counterexample trace,
from which it is not always possible to construct a concrete
counterexample.

