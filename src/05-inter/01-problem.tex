\seclabel{Inter:Problem}

\subsection{A Running Example}
\seclabel{Inter:Problem:Running}

To make the discussion more clear, we start with an example program.
We use the union-find data structure, due to Tarjan~\cite{JACM:Tar75},
which efficiently maintains a partition of elements, supporting the union of two bins and
giving each bin a unique representative.
It does so by maintaining a forest using a parent pointer at each node (see \figref{UnionFind:code}).
\footnote{We have simplified \tunion\ by
  not keeping track of the sizes of 
  sets (usually employed in order to attach the smaller set to the larger).},
Two elements are in the same bin iff they share a common root.


The method \tfind\ requires that the argument $x$ is  not null.
The formula $\tail{f}(x, r_x)$ means that the last node on an $f$-path from $x$ is $r_x$, which asserts that 
the auxiliary variable $r_x$ is equal to the root of $x$.
The procedure changes the pointers of some nodes in the list segment between $x$ and $r_x$,
denoted using the closed interval $[x, r_x]_f$. All the nodes in this segment are set to 
point directly to $r_x$.

The return value of \tfind\ (denoted by $\retval$) is $r_x$.
The postcondition uses the symbol $\upsinglSym$ denoting the value of $f$ before the method was invoked.
Since \tfind\ compresses paths from ancestors of $x$ to single edges to $r_x$, 
this root may be shared via new parent pointers.
\figref{Find:scenario} depicts a typical run of \tfind.

The method \tunion\ requires that both its arguments are not null.
It potentially modifies the ancestors of $x$ and $y$, i.e., $[x, r_x]_f \cup [y, r_y]_f$.
\figref{Union:scenario} depicts a typical run of \tunion.
Notice that the number of cutpoints~\cite{POPL:RinetzkyBRSW05} into
the branches of $x$ and $y$ is unbounded.



\begin{figure}
\begin{tabular}{@{}l@{~~}l@{}}
@ requires & $x \neq \vnull \land \tail{f}(x, r_x)$\\
@ $\modset$ & $[x, r_x]_f$\\
@ ensures &  $\begin{array}{l}\retval = r_x \land{} \\\
    \forallmod{\alpha, \beta}: \prtc{\alpha}{\beta} \liff \alpha = \beta \lor \beta = r_x\end{array}$
\end{tabular}
{\alltt
\begin{tabbing}
No\=de find(Node x) \{\+\\
Node i = x.f;\\
if\= (i != null) \{\+\\
i = find(i);\\
x.f = i;\-\\
\}\\
else \{\+\\
i = x;\-\\
\}\\
return i;\-\\
\}\\
\end{tabbing}}
\begin{tabular}{@{}l@{~~}l@{}}
@ requires & $x \neq \vnull \land  y \neq \vnull \land \tail{f}(x, r_x) \land \tail{f}(y, r_y)$\\
@ $\modset$ & $[x, r_x]_f \cup  [y, r_y]_f$\\
@ ensures \\
\end{tabular}\\
$~~\qquad\forallmod{\alpha, \beta}:
    \begin{array}{l}
    \big(\uprtc{x}{\alpha}\limplies (\prtc\alpha\beta\liff 
            \beta=\alpha \lor \beta=r_x\lor\beta=r_y)\big)\\
    {}\land \big(\uprtc{y}\alpha \limplies (\prtc\alpha\beta\liff \beta=\alpha\lor\beta=r_y)\big)
    \end{array}$
{\alltt
\begin{tabbing}
vo\=id union(Node x, Node y) \{\+\\
Node t = find(x); \\
Node s = find(y);\\
if (t != s) t.f = s;\-\\
\}
\end{tabbing}}
\caption{\figlabel{UnionFind:code}%
An annotated implementation of Union-Find in Java.
$f$ is the backbone field denoting the parent of a tree node.}
\end{figure}

\newcommand\retvalmark{\tikz\node[listnode,retval] {};}

\begin{figure}
\centering
\begin{tikzpicture}
  \def\cw{3mm}
  \def\szA{4cm}
  \matrix {
  \node[listnode](x) at (0,0) [label=left:$x$] {};
  \node[listnode](px) at (1,0) {};
  \node[listnode](r) at (2,0) [label=right:$r$] {};
  \node[listnode](y) at (0,-0.7) [label=left:$y$] {};
  \draw[->] (x) -- (px); \draw[->] (px) -- (r);
  \draw[->] (y) -- (px); &
  \node[symbol,label=above:{\small\tfind\ $x$}] {$\leadsto$}; &
  \node[listnode](x) at (1,0.7) [label=left:$x$] {};
  \node[listnode](px) at (1,0) {};
  \node[listnode,retval](r) at (2,0) [label=right:$r$] {};
  \node[listnode](y) at (0,-0.7) [label=left:$y$] {};
  \draw[->] (x) -- (r); \draw[->] (px) -- (r);
  \draw[->] (y) -- (px);
  \\
  };
\end{tikzpicture}
\def\cw{2mm}
\caption[An example scenario of running \tfind]{\figlabel{Find:scenario}An example scenario of running \tfind\
    (\,{\protect\retvalmark} {\small = return value}).}
\end{figure}


\begin{figure}
\centering
\begin{tikzpicture}
  \def\cw{3mm}
  \def\szA{4cm}
  \matrix[column sep=2mm] {
  \node[listnode](x) at (0,0) [label=left:$x$] {};
  \node[listnode](px) at (1,0) {};
  \node[listnode](r) at (2,0) [label=right:$t$] {};
  \node[listnode](w) at (0,-0.7) {};
  \node[listnode](y) at (0, 0.7) [label=left:$y$] {};
  \node[listnode](py) at (0.9,0.7) {};
  \node[listnode](s) at (1.8,0.7) [label=right:$s$] {};
  \path[->] (y) edge (py) (py) edge (s);
  \draw[->] (x) -- (px); \draw[->] (px) -- (r);
  \draw[->] (w) -- (px); &
  \node[symbol,label=above:{\small\tunion\ $x,y$}] {$\leadsto$}; &
  \node[listnode](x) at (1.3,-0.7) [label=left:$x$] {};
  \node[listnode](px) at (1,0) {};
  \node[listnode](r) at (2,0) [label=right:$t$] {};
  \node[listnode](w) at (0,-0.7) {};
  \node[listnode](y) at (0.3, 0.7) [label=left:$y$] {};
  \node[listnode](py) at (1.2,0.7) {};
  \node[listnode](s) at (2.2,0.7) [label=right:$s$] {};
  \draw[->] (x) -- (r); \draw[->] (px) -- (r);
  \draw[->] (w) -- (px);
  \path[->] (r) edge (s) (py) edge (s) (y) edge[out=30,in=150] (s);
  \\
  };
\end{tikzpicture}
\caption{\figlabel{Union:scenario}An example scenario of running \tunion.}
\end{figure}

\subsection{Working Assumptions}
\seclabel{Inter:Problem:Working}

\begin{description}
\item [Type correct] The procedure manipulates references to dynamically created objects in a
  type-safe way.  For example, we do not support pointer arithmetic.  
\item [Deterministic Reachability] The specification may use arbitrary uninterpreted relations.
It may also use the reachability formula $\frtc{\alpha}{\beta}$ meaning that
$\beta$ is reachable from $\alpha$ via zero or more steps along the functional backbone field $f$.
It may not use $f$ in any other way.
Until \secref{Discussion:Extensions}, we require $f$ to be acyclic and we restrict our attention to only one
backbone field.
\item [Precondition] There is a  \newterm{requires clause} defining the precondition which
   is written in alternation-free relational first-order logic ($\AFR$) and may use the relation
   $f^\star$ (see \defref{Reach-Logics}).
\item [Mod-set] There is a \newterm{modifies clause} defining the mod-set ($\modf$), which is the set of
  potentially changed memory locations.
(We include both source and target of every edge that is added or deleted). 
 The  modified set may have an unbounded number of vertices, but we require it to be the union of
a bounded number of $f$-intervals, that is chains of vertices through $f$-pointers.
\item [Postcondition] There is an \newterm{ensures clause} which exactly defines the new
  reachability relation $f^\star$ restricted to $\modf$.
The ensures clause, written in $\AFR$, may use two vocabularies (employing 
both $\ufsinglSym$ and $f$ to
refer to the reachability relations before and after.
\item [Bounded new sharing] All the new shared nodes --- nodes pointed to by more than one node --
must be pointed to by local variables at the end of the procedure's execution.  This requires
that only a bounded number of new shared nodes can be introduced by each procedure call.
\item [Loop-free] We assume that all code is loop free, with loops replaced by recursive calls.
\end{description}

Our goal is to reason modularly about a procedure that modifies a subset of the heap.
We wish to automatically update the reachability relation in the entire heap based on the changes to
the modified subset.
We remark that we are concerned with reachability between any two nodes
in the heap, as opposed to only those pointed to by program variables.
When we discuss sharing we mean sharing via pointer fields in the heap
as opposed to aliasing from stack variables, which does not
concern us in this context.

\subsection{Non-Local Effects}

Reachability is inherently non-local: a single edge
mutation can affect the reachability of an unbounded number of points that are an unbounded
distance from the point of change.
\figref{Find:scenario-nonlocal} contains a typical run of \tfind.
Two kinds of ``frames'' are depicted: (i)~$\modp=[x, r_x)_f$, specified by the programmer, denotes the nodes
whose edges can be directly changed by \tfind --- this is the standard notion of a frame condition;
(ii)~$\modprtc$ denotes nodes for which $f^*$, the reachability relation, has changed. We do not and in
general we cannot specify this set in a modular way because it usually depends on 
variables outside the scope of the current function such as $y$ in \figref{Find:scenario-nonlocal}.
In the example shown, there is a path from $y$ to $c$ before the call which does not exist after the call.
Furthermore, $\modprtc$ can be an arbitrarily large set: in particular, it
may not be expressible as the union of a bounded set of intervals: for example, when adding a subtree as a child of some node in another tree,
$\modp$ spans only one edge, whereas $\modprtc$ is the entire subtree added ---
which may contain an unbounded number of branches.

\def\cw{3mm}

\begin{figure}
\centering
\begin{tikzpicture}
      { [start chain=going above, node distance=4mm, every node/.style={on chain,listnode,join}, every join/.style=->]
        \node(x) {};
        \node(b) {};
        \node(c) {};
        \node(r) {};
      }
      \path(x) [late options={label={[name=xlbl]right:$x$}}]
           (c) [late options={label=right:$c$}];
      \node[listnode,left=6mm of x,label={[name=ylbl]left:$y$}](y) {};
      \node(z)[listnode,left=9mm of y,label=left:$z$] {};
      \node(zn)[listnode,above=of z] {}; \draw[->] (z) -> (zn);
      \node(modp)[modset,fit=(x) (r)] {};
      \node[modsetlbl,anchor=north east] at (modp.north west) {\small$\modp$};
      \draw[->] (y) -> (b);

      \node(modprtc)[frameset,fit=(modp) (y) (ylbl) (xlbl)] {};
      \node[framesetlbl,anchor=north east,inner sep=0] at (modprtc.north west) {\small$\modprtc$};

      \node[symbol,label=above:{\small\tfind\ $x$}] at(2.25,1) {$\leadsto$};

      % Now the same thing again to the right, with some edges relocated
      { [start chain=going above, node distance=4mm, every node/.style={on chain,listnode}, every join/.style=->]
        \node(x) at(6,0) {};
        \node(b) {};
        \node(c) {};
        \node(r) {};
      }
      \path(x) [late options={label={[name=xlbl]right:$x$}}]
           (c) [late options={label=right:$c$}];
      \node[listnode,left=6mm of x,label={[name=ylbl]left:$y$}](y) {};
      \node(z)[listnode,left=9mm of y,label=left:$z$] {};
      \node(zn)[listnode,above=of z] {}; \draw[->] (z) -> (zn);
      \node(modp)[modset,fit=(x) (r)] {};
      \node[modsetlbl,anchor=north east] at (modp.north west) {\small$\modp$};
      \draw[->] (y) -> (b);
      \draw[->] (c) -> (r);
      \draw[->] (x) edge[out=45,in=-45] (r);
      \draw[->] (b) edge[out=120,in=-135] (r);

      \node(modprtc)[frameset,fit=(modp) (y) (ylbl) (xlbl)] {};
      \node[framesetlbl,anchor=north east,inner sep=0] at (modprtc.north west) {\small$\modprtc$};
\end{tikzpicture}
\caption[A case where changes made by \tfind\ have a non-local effect.]
{\figlabel{Find:scenario-nonlocal}%
A case where changes made by \tfind\ have a non-local effect:
$\uprtc{y}{c}$, but $\lnot\prtc{y}{c}$.}
\end{figure}

The postcondition of \tfind\
is sound (every execution of \tfind\ satisfies it),
but incomplete: it does not provide a way to determine information
concerning paths outside $\modset$, such as from $y$ to $c$ in \figref{Find:scenario-nonlocal}.
Therefore, this rule is often not enough to verify the correctness of programs
that invoke \tfind\ in larger contexts.

Notice the difficulty of updating the global heap, especially the part $\modprtc\setminus\modp$.
In particular, using only the local specification of \tfind, one would
not be able to prove that $\lnot\frtc{y}{c}$.
Indeed, the problem is updating the reachability of elements that are {\bf outside} $\modset$;
in more complex situations, these elements may be far from the changed interval,
and their number may be unbounded.

One possibility to avoid the problem of incompleteness is to specify a postcondition which is specific to
the context in which the invocation occurs. However, such a solution requires reasoning per call site and
is thus {\em not modular}.
We wish to develop a rule that will fit in all contexts.
Reasoning about all contexts is naturally done by quantification.
