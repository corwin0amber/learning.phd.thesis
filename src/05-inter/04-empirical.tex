\seclabel{Inter:Empirical}


\subsection{Implementation Details}
A VC generator described in \secref{Inter:Extending:VC} is implemented in Python,
and PLY (Python Lex-Yacc) is employed at the front-end
to parse modular recursive procedure specifications as defined in \secref{Inter:Extending:Modular}.
The tool checks that the pre and the post-conditions are specified in $\AFR$ and
that the modset is defined.
SMT-LIB v2~\cite{smtlib} standard notation is used to format the VC and
to invoke Z3. As before, the validity of the VC can be checked by providing
its negation to Z3.
If Z3 exhibits a satisfying assignment then that serves as counterexample
for the correctness of the assertions. This means that either the procedure's
post-condition is not met, or that some precondition in one of the sub-calls
is violated.
If no satisfying assignment exists, then the generated
VC is valid, and therefore the program satisfies the assertions.

The output model/counterexample (S-Expression), if one is generated,
is then also parsed and $f^*$ is evaluated on all pairs of nodes.
This structure represents the state of the program either at the input or at the
beginning of a loop iteration: running the program from this
point will violate one or more invariants.
To provide feedback to the user, $f$ is recovered by computing \equref{Inversion}, and then the {\tt pygraphviz} tool is used to visualize
and present to the user a directed graph, whose vertices are
nodes in the heap, and whose edges are the $f$ pointer fields.



\subsection{Verification Examples}
We have written modular specifications for the example procedures shown in \tabref{Modular-Experiments}.
We are encouraged by the fact that it was not difficult to express assertions in \AFR\ for these procedures.
The annotated examples and the VC generation tool are publicly available.
We picked examples with interesting cutpoints to show the benefits of the modular approach
relative to the techniques presented in \charef{Pointer}.

To give some account of the programs' sizes, we observe the program summary
specification given as pre- and postcondition,
count the number of atomic formulas in each of them, and note the depth of
quantifier nesting; all our samples had only universal quantifiers 
in the specification.
We did the same for the generated VC;
naturally, the the VC is much larger than the specification even for
small programs.
Still, the time required by Z3 to prove that the VC is valid is short.


Thanks to the fact that FOL-based tools, and in particular SAT
solvers, permit multiple relation symbols we were able to express ordering properties in sorted lists, and thus in
the sorting routines implementing Quicksort and insertion-sort.


\begin{table}
\begin{tabular}{l@{\;---\;}p{5cm}}
    UF: find, UF: union &
    Implementation of a Union-Find dynamic data structure. \\
    SLL: filter &
    Takes a linked list and deletes all elements not satisfying
    some predicate $C$. \\
    SLL: quicksort &
    Sorts a linked-list in-place using the Quicksort algorithm. \\
    SLL: insert-sort &
    Creates a new, sorted linked-list from a given list by repeatedly
    running \texttt{insert} on the elements of the input list.
\end{tabular}
\caption{\label{ta:Exp-Descriptions}%
Description of some pointer manipulating programs verified by our tool.}
\end{table}


\begin{table}
\centering
\begin{tabular}{|l|rr|r|rr|r|}
 \hline
   \multirow{3}{*}
   {Benchmark} & \multicolumn{5}{c}{Formula size}
                & Solving \\
   & \multicolumn{2}{c}{P,Q}
   & \multicolumn{1}{@{ }c@{ }}{\modset}
   & \multicolumn{2}{c|}{VC} & \multicolumn{1}{c|}{time} \\
   & {\small \#} & {\small $\forall$}
   & {\small \#}
   & {\small \#} & {\small $\forall$}
             & \multicolumn{1}{c|}{\small (Z3)} \\
   \hline
   SLL: filter      &   7 &  2 & 1 & 217 &  6 &   0.48s \\
   SLL: quicksort   &  25 &  2 & 1 & 745 &  9 &   1.06s \\
   SLL: insert-sort &  21 &  2 & 1 & 284 & 11 &   0.37s \\
   UF: find         &  13 &  2 & 1 & 203 &  6 &   0.40s \\
   UF: union        &  20 &  2 & 2 & 188 &  6 &   1.39s \\
 \hline
\end{tabular}
\caption{\tablabel{Modular-Experiments}Implementation Benchmarks;
P,Q --- program's specification given as pre- and post-condition,
\modset --- mod-set, VC --- verification condition,
\# --- number of atomic formulas/intervals, $\forall$ --- quantifier nesting
The tests were conducted on a 1.7GHz Intel Core i5 machine with 4GB
of RAM, running OS X 10.7.5. The version of Z3 used was 4.2, complied
for 64-bit Intel architecture (using {\tt gcc} 4.2, LLVM). The solving
time reported is wall clock time of the execution of Z3.}
\end{table}



\subsection{Buggy Examples}
\label{Se:Buggy}

\begin{table}
\begin{tabular}{|p{2.9cm}|r@{ }r|r@{ }r|r|r|}
 \hline
               & \multicolumn{4}{c}{Formula size}
                & \;Solving\; & \multicolumn{1}{c|}{C.e.} \\
    {Benchmark}
    & \multicolumn{2}{c}{P,Q}
    & \multicolumn{2}{c}{VC} & \multicolumn{1}{c|}{time}
    & \multicolumn{1}{c|}{size} \\
    {\small (+ Nature of defect)}
    & \;{\small \#}\; & \;{\small $\forall$}\;
    & \;{\small \#}\; & \;{\small $\forall$}\;
             & \multicolumn{1}{c|}{\small (Z3)}
             & \multicolumn{1}{c|}{\small ($|\Locs|$)} \\
 \hline
   UF: find &
   27 &  3 & 201 & 6 & 1.60s & 2 \\
   {\small  Incorrect handling of corner case} & & & & & & \\
   UF: union &
   19 &  2 & 186 & 6 & 0.70s & 8 \\
   {\small  Incorrect specification} & & & & & & \\
   SLL: filter &
   36 &  4 & 317 & 6 & 0.49s & 14 \\
   {\small  Uncontrolled sharing} & & & & & & \\
   SLL: insert-sort &
   21 &  2 & 283 & 9 & 0.88s & 8 \\
   {\small  Unmet call precondition} & & & & & & \\
 \hline

\end{tabular}
\caption{\tablabel{Modular-Experiments-Bugs}%
Information about benchmarks that demonstrate
  detection of several kinds of bugs in pointer programs.
  In addition to the previous measurements, the last column lists the size
  of the generated counterexample in terms of the number of vertices ---
  linked-list or tree nodes.}
\end{table}


We also applied the tool to erroneous programs and programs with incorrect assertions.
The results, including run-time statistics and formula sizes,
are reported in \tabref{Modular-Experiments-Bugs}. In addition, we measured the size of the
model generated, by observing the size of the generated domain---which reflects the
number of nodes in the heap.
As expected, Z3 was able to produce concrete counterexamples of reasonable size,
providing output that is readable for the programmer and useful for debugging.
Since these are slight variations of the correct programs, size and running
time statistics are similar.
