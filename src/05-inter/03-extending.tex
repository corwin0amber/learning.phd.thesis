

\subsection{Modular Specifications of Procedure Behaviours}
\seclabel{Inter:Extending:Modular}

Here we explain how procedure specifications are written in order to support modular verification.

\begin{definition} {\rm A vocabulary 
$\voc=\B{\constants, \set{f}, \relations}$ is a triple of constant symbols, a function
symbol, and relation symbols.  The special constant symbol $\vnull$ is always included.
A \textbf{state}, $M$, is a logical structure with universe $\dom{M}$, including $\vnull$, and
$\vnull^M = \vnull = f^M(\vnull)$.\footnote{Remember that $\vnull^M$ is the interpretation of $\vnull$ in the structure $M$, and $f^M$ is the interpretation of $f$.}
A state is \textbf{appropriate} for an annotated procedure $\proc$ if its vocabulary includes every
symbol occurring in its annotations, and constants corresponding to 
all of the program variables occurring in $\proc$.}
\end{definition}

The diagrams in this chapter denote program states. 
For example \figref{Reverse-modular:snapshot} shows a transition
between two states. 

\begin{definition}[Backbone Differences]
\deflabel{backnoneDiff}
For states $\pre{M}$ and $M$ with the same domain
($\dom{\underbar{M}}=\dom{M}$ denotes the domain of $M$),
we denote by $\pre{M}/M$ the structure over the two-vocabulary
$\voc=\B{\{\vnull\}\cup\constants \cup \pre{\constants}, \{f,\pre{f}\}, \relations \cup \pre{\relations}}$ obtained by combining $\pre{M}$ and $M$ in the obvious way.
The set $\pre{M}\oplus M$ consists of the ``differences between $\pre{M}$ and $M$ excluding $\vnull$'',
i.e. $\pre{M}\oplus M=\{u \st f^M(u)\neq f^{\pre{M}}(u)\} \cup
\{f^M(u) \st f^M(u)\neq f^{\pre{M}}(u)\} \setminus \{\vnull\}$.
\end{definition}

\subsubsection*{Modification Set}

We must specify the \textbf{mod-set}, $\modset$, containing all the endpoints of edges that are modified.
Therefore when adding or deleting an edge $\abra{s,t}$, both ends --- the source, $s$, and the target, $t$ ---
are included in $\modset$.
Often in programming language semantics, 
only the source is considered modified. However, thinking of 
the heap as a graph, it is useful to consider both ends of a modified edge as modified.

Our mod-sets are built from two kinds of intervals:

\begin{definition}[Intervals]
\deflabel{interval}{\rm
The \newterm{closed interval} $[a,b]_f$ is
\[[a,b]_f \eqdefS \{\alpha \stS \frtcBetween{a}{\alpha}{b}\}\]
and  the \newterm{half-open interval} $[a,\vnull)_f$ is:
\[
[a,\vnull)_f \eqdefS \{
     \alpha \stS \frtc{a}{\alpha} \land \ftc{\alpha}{\vnull}\}
\]}
\end{definition}

(notice that $\ftc{\alpha}{\vnull}$ is trivially true in acyclic heaps).

\begin{definition}[mod-set]
\deflabel{mod}
  The mod-set, $\modset$, of a procedure is a union $I_1\cup I_2\cup \ldots \cup I_k$,
  where each $I_i$ may be  $[s_i,t_i]_f$ or 
  $[s_i,\vnull)_f$, $s_i, t_i$ are
  parameters of the procedure or constant symbols occurring in the pre-condition.
\end{definition}

In our examples, the mod-sets of \tfind\ and \tunion\ are written above
each procedure, preceded by the symbol ``@ mod'' (\figref{UnionFind:code}).
Note that it follows from \defref{mod} that ``$\alpha\in\modset$''
is expressible as a quantifier-free formula.


\begin{definition}
\deflabel{mod-interpretation}
Given an appropriate state $M$ for $\proc$ with modset $\modset$,
$\modset^M$ is the set of all elements in $\dom{M}$ that are in $\modset$
(where $\modset$ is defined by a union of intervals, see \defref{interval}).
\end{definition}




\subsubsection*{Pre- and Post-Conditions}

The programmer specifies \AFR\ pre- and post-conditions.
Two-vocabulary formulas may be used in the post-conditions where $\ufsinglSym$ denotes the value
of $f$ before the call. 

\subsubsection*{Specifying Atomic Commands}

\begin{table*}
\[
\begin{array}{||l|l|l|l|}
\hline\hline
\textbf{Command} & \textbf{Pre} &   \textbf{Mod} &\textbf{Post}\\
\hline
\texttt{ret = y.f} &  y \neq \vnull \land E_f(y, s) & \emptyset  & ret = s\\
\hline
\texttt{y.f = null} &  y \neq \vnull \land E_f(y, s) & [y, s]_f  & \neg \frtc{y}{s} \land \neg \frtc{s}{y}\\
\hline
\texttt{assume y.f==null}; & & &\\
\texttt{y.f = x} &  y \neq \vnull \land E_f(y, \vnull) \land \neg \frtc{x}{y} & [y, y]_f \cup [x, x]_f & \frtc{y}{x} \land \neg \frtc{x}{y}\\
\hline\hline
\end{array}
\]
\caption{\tablabel{AtomicSpec}%%
The specifications of atomic commands. $s$ is a local constant denoting the $f$-field of $y$.
$E_f$ is the inversion formula defined in \equref{Inversion}.}
\end{table*}

\tabref{AtomicSpec} provides specification of atomic commands.
They describe the memory changed by atomic statements and the changes on the local heap.

\paragraph{Accessing a pointer field}
The statement \texttt{ret = y.f} reads the content of the $f$-field of \texttt{y} into \texttt{ret}.
It requires that \texttt{y} is not \texttt{null} and that an auxiliary variable \texttt{s} point to the $f$-field of \texttt{y} (which may be \texttt{null}).
It does not modify the heap at all.
It sets \texttt{ret} to \texttt{s}.
It is interesting to note that the postcondition is much simpler than the one provided in \secref{Pointer:Extending}
because there is no need to specify the effect on the whole heap.
In particular, it does not employ quantifiers at all.
The quantification is ``filled in'' by the adaptation rule.

\paragraph{Edge Removals}
The statement \texttt{y.f = null} sets the content of the $f$-field of \texttt{y}, into \texttt{null}.
It requires that \texttt{y} is not \texttt{null} and that an auxiliary variable \texttt{s} points to the $f$-field of \texttt{y} (which may be \texttt{null}).
It modifies the node pointed-to by \texttt{y} and potentially the node pointed-to by \texttt{s}.
Notice that the modset includes two elements pointed by $y$ and $t$, the two end-points of the edge.
It removes paths between \texttt{y} and \texttt{s}.
The postcondition assert that there are no paths from $y$ to $s$.
Also, since $s$ is potentially modified, it has to assert that no path is created from $s$ to $y$.

\paragraph{Edge Additions}
The statement \texttt{y.f = x} is specified assuming without loss of generality,  that
the statement \texttt{y.f = null} was applied before it.
Thus, it only handles edge additions.
It therefore requires that \texttt{y} is not \texttt{null} and its $f$-field is \texttt{null}.
It modifies the node pointed-to by \texttt{y} and potentially the node pointed-to by \texttt{x}.
It creates a new path from \texttt{y} to \texttt{x} and asserts the absence of new paths from \texttt{x} back to \texttt{y}.
Again the absence of back paths (denoted by $\neg \frtc{x}{y}$) is needed for completeness.
The reason is that both the node pointed-to by \texttt{x} and \texttt{y} are potentially modified.
Since \texttt{x} is potentially modified, without this assertion, a post-state in which \texttt{x.f == y} will be allowed by the postcondition.

\subsubsection{Soundness and Completeness}


We now formalize the notion of soundness and completeness of modular specifications and assert that the specifications of atomic commands are sound and complete.


\begin{definition}[Soundness and Completeness of Procedure Specification]
Consider a procedure $\proc$ with
 precondition $P$, modset $\modset$, post-condition $Q$.
We say that $\B{P, \modset, Q}$ \textbf{is sound with respect to} $\proc$ if for every appropriate pre-state $\pre{M}$ such that $\pre{M} \models P$,
and appropriate post-state $M$ which is a potential outcome of the body of $\proc$ when executed on $\pre{M}$:
(i)~$\pre{M} \oplus M\subseteq\modset^{\pre{M}}$,
(ii) $\pre{M}/M \models Q$.
Such a triple $\B{P, \modset, Q}$ \textbf{is complete with respect to} $\proc$ if for every appropriate states $\pre{M}, M$ such that
(i)~$\pre{M} \models P$,
(ii)~$\pre{M}/M \models Q$,
and
(iii)~$\pre{M} \oplus M\subseteq\modset^{\pre{M}}$,
then there exists an execution of the body of $\proc$ on $\pre{M}$ whose outcome is $M$.
\end{definition}

The following proposition establishes the correctness of atomic statements.
\begin{proposition}[Soundness and Completeness of Atomic Commands]
The specifications of atomic commands given in \tabref{AtomicSpec} are sound and complete.
\end{proposition}



The following lemma establishes the correctness of \tfind\ and \tunion, which is interesting since they update an unbounded amount of memory.
\begin{lemma}[Soundness and Completeness of Union-Find]
The specification of \tfind\ and \tunion\ in \figref{UnionFind:code} is sound and complete.
\end{lemma}

We can now state the following proposition:

\begin{proposition}[Soundness and Completeness of $\adapt{}$]
Let $\modset$ be a mod-set of some procedure $proc$.
Let $M$ and $\underbar{M}$ be two appropriate states for $\proc$.
Then, ${\pre{M}\oplus M \subseteq \modset^{\pre{M}}}$
iff $\pre{M}/M$ augmented with some interpretation for the function symbol
$\en{\modset}$ is a model of $\adapt{\modset}$.
\end{proposition}


\subsection{Generating Verification Condition for Procedure With Sub-calls in $\AEAR$}
\seclabel{Inter:Extending:VC}

We will now extend the basic rules for weakest (liberal) precondition with
appropriate formulas for procedure call statements.


\subsubsection{Modular Verification Conditions}


The modular verification condition will also contain a conjunct for checking
that $\modset$ affected by the invoked procedure is a subset of the
``outer'' $\modset$. This way the specified restriction can be checked
in $\AEAR$ and the SMT solver can therefore be used to enforce it automatically.

\subsubsection{Weakest-precondition of Call Statements}

As discussed in \secref{Inter:Problem:Working}, the specification as it appears in the ``ensures''
clause of a procedure's contract is a local one, and in order to make reasoning
complete we need to adapt it in order to handle arbitrary contexts.
This is done by conjoining $Q_{\proc}$ occurring in the ``ensures'' clause
from the specification
of $\proc$ with the universal adaptation rule $\textrm{adapt}[\modset]$, where $\modset$ is replaced with the mod-set
as specified in the ``modifies'' clause of $\proc$.

\tabref{wp:call-statement} presents a formula for the weakest-precondition
of a statement containing the single procedure call, where the invoked
procedure has the specifications as in \figref{proc:spec},
where ``$\proc$'' has the formal parameters $\vecx=x_1,\ldots,x_k$,
and it is used with $\veca=a_1,\ldots,a_k$
(used in the formula)
as the actual arguments for a specific procedure call;
we assume w.l.g. that each $a_i$ is a local variable of the calling procedure.

\begin{figure}
\begin{tabular}{@{}l@{ }l@{}l}
@ requires & $P_{\proc}$ \\
@ $\modset$ & $\modset_{\proc}$ \\
@ ensures & $Q_{\proc}$ \\
\end{tabular}\\
\textit{return-type} $\proc(\vecx)$ \texttt{\{ \ldots\ \}}
\caption{\figlabel{proc:spec}%
Specification of $\proc$ with placeholders.}
\end{figure}

\begin{table}
\renewcommand{\arraystretch}{1.2}
\[
    \begin{array}{l@{\ }l}
        \wlp{r := &proc(\overline{a})}(Q) \eqdefS \\
        &\subst{P_{\proc}}{\vecx}{\veca}
            \;\land\\
        & \forall \alpha: \alpha\in \subst{\modset_{\proc}}{\vecx}{\veca} \limplies \alpha\in \modset_{\prog} \;\land \\
        &
        \forall \zeta: \substFOUR{Q_{\proc}}{\vecx}{\veca}{f}{\freshfSym}{\ufsinglSym}{f}{\retval}{\zeta}
        \land \\
        &\quad \substTHREE{\textrm{adapt}\left[{\subst{\modset_{\proc}}{\ufsinglSym}{f}}\right]}
        	{\vecx}{\veca}{f}{\freshfSym}{\ufsinglSym}{f}
        \limplies \\
        &\quad \substTWO{Q}{f}{\freshfSym}{r}{\zeta}
    \end{array}
\]
\caption{\tablabel{wp:call-statement}%
Computing the weakest (liberal) precondition for a statement containing
a procedure call. $r$ is a local variable that is assigned the return value;
$\veca$ are the actual arguments passed. {\protect\freshfSym} is a fresh function
symbol.}
\end{table}

In general it is not obvious how to enforce that the set of locations
modified by inner calls is a subset of the set of locations declared
by the outer procedure. Moreover, this can be tricky to check since
it depends on aliasing and paths between nodes pointed to by different variables.
Fortunately,
the sub-formula $\forall \alpha: \alpha\in \subst{\modset_{\proc}}{\vecx}{\veca} \limplies \alpha\in \modset_{\prog}$
captures this property, ensuring that the outer procedure does not exceed its own $\modset$
specification, taking advantage of the interval-union structure of the $\modset$. 
Since all the modifications (even atomic ones)
are done by means of procedure calls, this ensures that no edges incident to
nodes outside $\modset$ are changed.

\newcommand\uM{\underline{M}}

\begin{proposition}
The rule for $\wlp{}$ of call statements is sound
and complete, that is, when $\proc$ is a procedure with specification
as in \figref{proc:spec}, called in the context of $\prog$ whose mod-set is $\modset$:
\begin{equation}
	\begin{array}{c}
		\uM \models \wlp{r := \proc(\veca)}(Q) \\
		\Updownarrow \\
		\uM\models \subst{P_{\proc}}{\vecx}{\veca} \land 
			\modset^{\uM}_{\proc} \subseteq \modset^{\uM} \land \\
		\forall M: \big(
			\uM/M\models \subst{Q_{\proc}}{\vecx}{\veca}
			\land \uM\oplus M\subseteq \modset^{\uM}_{\proc}\big)\\
			\Rightarrow
			M[r\mapsto \retval^M] \models Q
	\end{array}
\end{equation}
\end{proposition}

\subsubsection{Reducing Function Symbols}
\seclabel{Inter:Extending:Reducing}

Notice that when we apply the adaptation rule for $\AEAR$, as discussed
above, it introduces a new function symbol $\en{\modset}$
depending on the concrete mod-set of the called procedure.
This introduces a complication:
the mod-sets of separate procedure calls may differ from the one of the top procedure, hence multiple applications
of \tabref{wp:call-statement}
naturally require a separate function symbol $\en{\modset}$ 
for every such invocation.
Consider for example the situation of \tunion\ making two invocations
to \tfind. In \figref{modular:find-in-union} one can see that
the $\modset$ of \tunion\ is $[x,r_x]_f\cup [y,r_y]_f$, while the
$\modset$ of the first call \texttt{t := find(x)} is $[x,r_x]_f$, which
may be a proper subset of the former. The $\modset$ of the second
invocation is $[y,r_y]_f$, which may overlap with $[x,r_x]_f$.


\begin{figure}
\centering
\def\cw{4mm}
\begin{tikzpicture}[decoration={snake,amplitude=1mm,segment length=14mm,pre length=1mm,post length=1mm},
    en/.style={-stealth,dashed,shorten <=1mm,shorten >=1mm,draw=black!50,looseness=1.3}]
      { [start chain=going above, node distance=4mm, every node/.style={on chain,listnode}, every join/.style=->, font=\tiny]
        \node(x) {5};
        \node(b)[join] {4};
        \node(c)[join] {3};
        \node(d)[join] {2};
        \node(r)[join] {1};
      }
      { [start chain=going above, node distance=4mm, every node/.style={on chain,listnode}, every join/.style=->, font=\tiny]
        \node(y)[left=0.5cm of x] {5};
        \node(e)[join] {4} edge[->] (c);
      }
      \path[label distance=1.5mm, inner xsep=0]
            (x) [late options={label={[name=xlbl]right:$x$}}]
            (y) [late options={label={[name=ylbl]left:$y$}}];
      \begin{pgfonlayer}{background layer}
      \node(inner0)[inner modset,fit=(x) (xlbl) (r),inner ysep=2mm,inner xsep=2.5mm] {};
      \end{pgfonlayer}
      \node(inner lbl)[inner modsetlbl,anchor=north east] at (inner0.north west) {\small{$[x,r_x]_f$}};

      \node(outer)[modset,fit=(inner0) (inner lbl) (x) (xlbl) (y) (ylbl),inner xsep=1.5mm] {};
      \node[modsetlbl,anchor=south west,align=right]
         at (outer.north west) {\small{$[x,r_x]_f \cup [y,r_y]_f$}};

      \path[en] (y) edge[out=150,in=170]
        node[pos=0.8,anchor=south east,inner sep=.5mm] {\tiny{$\en{[x,r_x]}$}} (c);

      \begin{scope}[xshift=5cm]
      { [start chain=going above, node distance=4mm, every node/.style={on chain,listnode}, every join/.style=->, font=\tiny]
        \node(x) {5};
        \node(b)[] {4};
        \node(c)[] {3};
        \node(d)[] {2};
        \node(r)[join] {1};
      }
      { [start chain=going above, node distance=4mm, every node/.style={on chain,listnode}, every join/.style=->, font=\tiny]
        \node(y)[left=0.5cm of x] {5};
        \node(e)[join] {4} edge[->] (c);
      }
      \path[->] (c) edge[out=120,in=-120] (r)
                (b) edge[out=65,in=-65] (r)
                (x) edge[out=60,in=-46,looseness=.7] (r);
      \path[label distance=1.5mm, inner xsep=0]
            (x) [late options={label={[name=xlbl]right:$x$}}]
            (y) [late options={label={[name=ylbl]left:$y$}}]
            (r) [late options={label={[name=tlbl]right:$t$}}];
      \begin{pgfonlayer}{background layer}
      \node(inner)[inner modset,fit=(x) (xlbl) (r),inner ysep=2mm,inner xsep=2.5mm] {};
      \end{pgfonlayer}
      \node(inner lbl)[inner modsetlbl,anchor=north east] at (inner.north west) {\small{$[x,r_x]_{\upsinglSym}$}};

      \node(outer)[modset,fit=(inner) (inner lbl) (x) (xlbl)] {};
      \node[modsetlbl,anchor=south west,align=right]
         at (outer.north west) {\small{$[x,r_x]_{\upsinglSym} \cup [y,r_y]_{\upsinglSym}$}};

      \path[en] (y) edge[out=150,in=170]
        node[pos=0.8,anchor=south east,inner sep=.5mm] {\tiny{$\en{[x,r_x]}$}} (c);

      \end{scope}

      \path[->] (inner0.50) edge[shorten <=1mm,shorten >=1mm,decorate]
        node[pos=0.33,above=3mm] {\small{$t$ := \tfind\ $x$}}
       (inner.130) ;
\end{tikzpicture}
\caption{\figlabel{modular:find-in-union}%
An example invocation of \tfind\ inside \tunion.}
\end{figure}

To meet the requirement of $\AEAR$ concerning the function symbols, we observe that:
$(a)$ the amount of sharing that
any particular function call creates, as well as the entire call,
is bounded, and we can pre-determine a bound for it;
$(b)$ the modification set of any sub-call must be a subset
of the top call, as otherwise it violates the obligation not to change any edge outside $\modset$.
These two properties allow us to express the functions $\en{\modset}$ of the
sub-calls using $\en{\modset}$ of the top procedure and extra intermediate functions with bounded image.
Thus, we replace all of the function symbols
$\en{S}$ introduced by $\adapt{S}$ for different $S$'s, with a single
(global, idempotent) function symbol together with a set of bounded function
symbols.

Consider a statement $r := \proc(\veca)$ in
a procedure $\prog$. Let $A$ denote the mod-set of
$\prog$, and $B$ the mod-set of $\proc$.
We show how $\en{B}$ can be expressed using $\en{A}$ and one more function,
where the latter has a bounded range.
We define $\en{B|A} : A\setminus B \to B$ a new function that is the
restriction of $\en{B}$ to the domain $A\setminus B$.
$\en{B|A}$ is defined as follows:
%
\begin{equation}
\en{B}(\sigma)\eqdefS
\begin{cases}
\en{A}(\sigma) & \en{A}(\sigma)\in B \\
\en{B|A}(\en{A}(\sigma)) & \text{otherwise} \\
\end{cases}
\end{equation}

Using equality the nesting of function symbols can be reduced (without affecting
the quantifier alternation).

Consult \figref{en-modp:inner}; notice that
$\en{B|A}(\sigma)$ is always either:
\begin{itemize}
  \item The beginning $s_i$ of one of the intervals $[s_i,t_i]_f$ of $B$
    (such as \tikz[baseline=(a.base)] \node(a)[listnode] {\tiny 1}; in the figure);
  \item A node that is {\bf shared} by backbone pointers from two nodes
     in $A$ (such as \tikz[baseline=(a.base)] \node(a)[listnode] {\tiny 3};);
  \item The value $\vnull$.
\end{itemize}

\begin{figure}
\centering
\begin{tikzpicture}[k/.style={listnode,on chain},entry function]
    \def\cw{4mm}
    \def\dw{6mm}
    \begin{scope}[sll chain]
        \node(d1)[k] {};
        \node(d2)[k,join] {};
        \node(d3)[k,join] {};
    \end{scope}
    \begin{scope}[sll chain,yshift=-10mm,xshift=-5mm]
        \node(e0)[k] {};
        \node(e1)[k] {\tiny 1};
        \node(e2)[k,join] {\tiny 2};
        \node(e3)[k,join] {\tiny 3};
        \node(e4)[k,join] {\tiny 4};
    \end{scope}
    \draw[->] (d3) -> (e3); \draw[->] (e0) -> (e1);
    \node(inner modp)[inner modset,fit=(e1) (e2) (e3) (e4), inner sep=0.7*\cw] {};
    \node(lbl)[inner modsetlbl,anchor=north east,inner sep=0.5mm] at (inner modp.south east) {\small(inner)$\modset=B$};
    \node(modp)[modset,fit=(d1) (d2) (d3) (e0) (inner modp), inner sep=\dw] {};
    \node[modsetlbl,anchor=south east] at (modp.north east) {\small$\modset=A$};

    % enmodp and en-> edges
    \node(delta)[listnode,label=left:$\delta$,above=of d1] {}
         edge[out=0,in=90,en] node[above right]{$\en{A}$} (d2);
    \path (d2) edge[en,out=45,in=55,looseness=2] node[right=2mm]{$\en{B|A}$} (e3);
\end{tikzpicture}
\caption{\figlabel{en-modp:inner}%
The inner $\en{\modset}$ is constructed from the outer one by composing with
an auxiliary function $\en{B|A}$.}
\end{figure}

A bound on the number of $s_i$'s is given in the modular specification
of $\proc$. A bound on the number of shared nodes is given in the next subsection.
This bound is effective for all the procedure calls in $\prog$;
hence $\en{B|A}$ can be used in the restricted logic $\AEAR$.

\subsubsection{Bounding the Amount of Sharing}
\seclabel{Inter:Extending:Bounding}

We show that under the restrictions of the specification
given in \secref{Inter:Extending:Modular}, the number of shared
nodes inside $\modset$ --- that is, nodes in $\modset$ that are pointed two by more than one
$f$-pointer of other nodes in $\modset$ --- has a fixed bound throughout
the procedure's execution.

Consider a typical loop-free program $\prog$ containing calls 
of the form $v_i$ := $\proc_i$($\veca_i$).
Assume that the mod-set of $\prog$ is a union of $k$ intervals.
We show how to compute a bound on the number of shared nodes inside
the mod-set. Since there are $k$ start points,
at most $\binom{k}{2}$ elements can be shared when
$\prog$ starts executing. Each sub-procedure invoked from $\prog$ may
introduce, by our restriction, at most as many shared nodes as there 
are local variables in the sub-procedure. Therefore, by computing the
sum over all invocation statements in $\prog$, plus 
$\binom{k}{2}$,
we get a fixed bound on the number of shared nodes inside the mod-set.
%
\[
N_{\mathrm{shared}} = k + \binom{k}{2} + \sum_{\proc_i} |\Pvar_{\proc_i}|
\]

$\Pvar^{\proc_i}$ signifies the set of local variables in the procedure $\proc_i$.
Notice that if the same procedure is invoked twice, it has to be included twice
in the sum as well.

\subsection{Verification Condition for the Entire Procedure}
\label{sec:vc:proc}

Since every procedure on its own is loop-free, the verification condition
is straightforward:
%
\[
\vc{\prog} = P_{\prog} \limplies \wlp{\prog}\big(Q_{\prog} \land ``shared\subseteq\Pvar"\big)
\]
%
where $``shared\subseteq\Pvar"$ is a shorthand for the ($\AER$) formula:
%
\bgroup
\renewcommand\a\alpha
\renewcommand\b\beta
\renewcommand\c\gamma
\begin{equation}
\begin{array}{l@{\ }l}
\displaystyle
\forall \a,\b,\c\in&\modset: E_f(\a,\c) \land E_f(\b,\c) \limplies\\
	& \a=\b \lor \bigvee_{v\in\Pvar}\c=v
\end{array}
\end{equation}
\egroup

See \equref{Inversion} for the definition of $E_f$. 
This It expresses the obligation mentioned in \secref{Inter:Problem:Working} that
all the shared nodes in $\modset$ should be pointed to by local variables,
effectively limiting newly introduced sharing to a bounded number of memory
locations.

\medskip
Now $\vc{\prog}$ is expressed in $\AEAR$, and it is valid if-and-only-if
the program meets its specification. Its validity can be checked using
effectively-propositional logic according to \secref{Inter:Extending:Modular}.
