\seclabel{Inter}
\label{SE:INTER} % patch for RELATED WORK

\published{popl:IBILNS14}

%% Executive Summary
This chapter shows how to harness existing SAT solvers for proving that a program
containing (potentially recursive) procedures satisfies its specification and for
automatically producing counterexamples when it does not.
We concentrate on proving safety properties of imperative programs manipulating linked data structures which is challenging since we need to reason about unbounded
memory and destructive pointer updates.
The tricky part is to identify a logic which is expressive enough to enable the modular verification of interesting procedures and properties
and weak enough to enable sound and complete verification using SAT solvers.

\begin{figure*}
\centering
\begin{tikzpicture}
    \def\cw{4mm}
    \def\dw{5mm}
    \matrix[column sep=\dw, row sep=\dw, label distance=3mm, font=\tiny] {
        \node(a)[listnode,label={[name=h]left:\small$h$}] {1}; &
        \node(b)[listnode] {2}; &
        \node(c)[listnode] {3}; &
        \node(d)[listnode] {4}; &
        \node(e)[listnode] {5}; \\
        &
        \node(z1)[listnode,label={[name=z1lbl]below:\small$z_1$}] {}; &
        \node(z2)[listnode,label={[name=z2lbl]below:\small$z_2$}] {}; &
        \node(z3)[listnode,label={[name=z3lbl]below:\small$z_3$}] {}; \\
    };
    \path[->] (h) edge (a) (a) edge (b) (b) edge (c) (c) edge (d) (d) edge (e)
        (z1lbl) edge (z1) (z1) edge[extpath] (b)
        (z2lbl) edge (z2) (z2) edge[extpath] (c)
        (z3lbl) edge (z3) (z3) edge[extpath] (d);
    \begin{pgfonlayer}{background layer}
      \node[modset,fit=(a)(b)(c)(d)(e)] {};
    \end{pgfonlayer}

    \node[symbol,label=above:{\small\texttt{reverse} $x$}] at(3.5cm,0.75cm) {$\leadsto$};

    \begin{scope}[xshift=7cm]
    \matrix[column sep=\dw, row sep=\dw, label distance=3mm, font=\tiny] {
        \node(a)[listnode] {1}; &
        \node(b)[listnode] {2}; &
        \node(c)[listnode] {3}; &
        \node(d)[listnode] {4}; &
        \node(e)[listnode,label={[name=h]right:\small$h$}] {5}; \\
        &
        \node(z1)[listnode,label={[name=z1lbl]below:\small$z_1$}] {}; &
        \node(z2)[listnode,label={[name=z2lbl]below:\small$z_2$}] {}; &
        \node(z3)[listnode,label={[name=z3lbl]below:\small$z_3$}] {}; \\
    };
    \path[->] (h) edge (e) (e) edge (d) (d) edge (c) (c) edge (b) (b) edge (a)
        (z1lbl) edge (z1) (z1) edge[extpath] (b)
        (z2lbl) edge (z2) (z2) edge[extpath] (c)
        (z3lbl) edge (z3) (z3) edge[extpath] (d);
    \begin{pgfonlayer}{background layer}
      \node[modset,fit=(a)(b)(c)(d)(e)] {};
    \end{pgfonlayer}
    \end{scope}
\end{tikzpicture}
\caption{\figlabel{Reverse-modular:snapshot}%
Reversing a list pointed to by a head $h$ with many shared nodes accessible from outside the local
heap (surrounded by a rounded rectangle).}
\end{figure*}


%% Effectively Propositional
In the previous chapters we have shown how to employ effectively propositional logic for verifying programs manipulating
linked lists.
%% Why doesn't it suffice
However, we see that effectively propositional logic does not suffice to naturally express the effect on the global heap when the local heap of a procedure
is accessible via shared nodes from outside.
For example, \figref{Reverse-modular:snapshot} shows a pre- and post-heap before a list pointed-to by $h$ is reversed.
The problem is how to express the change in reachability between nodes such as $z_i$ and list nodes $1, 2, \ldots, 5$: note that, e.g., nodes $3, 4, 5$ are unreachable from $z_1$ in the post-heap.


This chapter shows that in many cases, including the above example, reachability can still be checked precisely using SAT solvers.
Our solution is based on the following principles:
\begin{itemize}
\item We follow the standard techniques (e.g., see \cite{Larch,JML,SpecS,PLDI:ZeeKR08}) by requiring that the programmer define the set of
potentially modified elements.
\item The programmer only specifies postconditions on local heaps and ignores the effect of paths from the global heap.
\item We provide a general and exact \newterm{adaptation rule} for adapting postconditions to the global heap.
This adaptation rule is expressible in a generalized version of $\AER$ (\defref{Reach-Logics}) called $\AEAR$.
$\AEAR$ allows an extra function symbol, called the \newterm{entry} function, that maps each node $u$ in the global heap into the first node accessible
from $u$ in the local heap.
In \figref{Reverse-modular:snapshot},
 $z_1, z_2$ and $z_3$  are mapped to $2, 3$ and $4$, respectively.
The key facts are that \AEAR\ suffices to precisely define the global reachability relationship
after each procedure call and yet
any \AEAR\ formula can be simulated by an $\AER$ formula, and from \proref{AER-simulation},
by an effectively propositional one.
Thus the automatic methods used in \charef{Pointer}
still apply to this significantly more general setting.
\item We restrict the verified procedures in order to guarantee that the generated verification condition of every procedure remains in \AEAR.
The main restrictions are: type correctness, deterministic paths in the heap, limited number of changed list segments in the local heap (each of which may be unbounded) and
limited amount of newly created heap sharing by each procedure call.
These restrictions are enforced by the generated verification condition in $\AEAR$.
This formula is automatically checked by the SAT solver.
\end{itemize}


\subsection*{Main Results}

The results presented in this chapter can be summarized as follows:
\begin{itemize}
\item We define a new logic, \AEAR, which extends the previously defined $\AER$ 
  (\defref{Reach-Logics}) with a limited idempotent function and yet is equi-satisfiable with EPR.
\item We provide a precise adaptation rule in \AEAR, 
  expressing the locality property of the change, such that, in conjunction
  with the formula expressing the post-condition of the local heap,
  it precisely updates the reachability relation over the global heap.
\item We generate a modular verification formula in \AEAR\ for each procedure, asserting that the
  procedure satisfies its pre- and post-conditions and the above restrictions.
  This verification condition is sound and complete, i.e., it is valid if and only if the procedure
  adheres to the restrictions and satisfies its requirements.
\item We implemented this tool on top of Z3.
  We were able to show that many programs can be modularly verified using our methods.
  They satisfy our restrictions and their invariants can be expressed naturally in $\AER$.
\end{itemize}
