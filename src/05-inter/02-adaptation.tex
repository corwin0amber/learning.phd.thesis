\seclabel{Inter:Adaptation}

\subsection{An FO(TC) Adaptation Rule}
\seclabel{adapt:fotc}

\begin{figure}
\centering
\begin{tikzpicture}[block/.style={fill=white}]
    \def\cw{4mm}
    \newcommand\q{node[pos=.4,above] {$_q$}}
    \newcommand\qV{node[pos=.4,above left,inner sep=0] {$_q$}}
    \newcommand\f{node[midway,above] {$_f$}}
    \newcommand\fV{node[midway,above left,inner sep=0] {$_f$}}
    \matrix(modp)[column sep=7mm, row sep={9mm,between origins}, every node/.style={block}] {
    	&
    	\node(i0)[listnode,yshift=1mm] {}; &[1mm]
        \node(a0)[listnode,yshift=2mm] {}; & 
        \node(a1)[listnode] {}; &
        \node(a2)[listnode,yshift=1.5mm] {}; &[3mm]
        \node(o0)[listnode] {}; &
        \\
    	\node(i1)[listnode,yshift=-1mm] {}; &
    	\node(i2)[listnode,yshift=-2mm] {}; &
        \node(b0)[listnode] {}; & 
        \node(b1)[listnode] {}; &
        \\
        &
    	\node(i3)[listnode,yshift=-2mm,xshift=2mm] {}; &
        \node(c0)[listnode,yshift=-1mm] {}; & 
        \node(c1)[listnode,yshift=0mm] {}; & 
        \node(c2)[listnode,yshift=-1mm] {}; &[3mm]
        \node(o2)[listnode] {}; &
        \node(o3)[listnode] {}; \\
    };
    \begin{pgfonlayer}{background layer}
    \node[modblob,fit=(a0) (b1) (c2),label={[modsetlbl]below:$\modset$}] {};
    \end{pgfonlayer}
    \draw[->,extpath] (i0) -- (a0) \q;
    \draw[->] (a0) -- (a1) \f;    \draw[->] (a1) -- (a2) \f;
    \draw[->,extpath] (i1) -- (i2) \q;
    \draw[->,extpath] (i2) -- (b0) \q;
    \draw[->,extpath] (i3) -- (b0) \qV;
    \draw[->] (b0) -- (b1) \f;    \draw[->] (b1) -- (a2) \fV;
    \draw[->] (c0) -- (c1) \f;    \draw[->] (c1) -- (c2) \f;
    \draw[->,extpath] (a2) -- (o0) \q;
    \draw[->,extpath] (c2) -- (o2) \q;
    \draw[->,extpath] (o2) -- (o3) \q;
\end{tikzpicture}
\caption{\figlabel{Edgesfq}}{Labeling edges inside and outside $\modset$.}
\end{figure}

A standard way to modularize specifications is to specify the local effect of a procedure
and then to use a general adaptation rule (or frame rule) to derive the global effect.
In our case, we know that locations outside $\modset$ are not modified.
Therefore, for example, after a call to \tfind\, a new path from node $\sigma$ to node $\tau$ is either an old path from $\sigma$ to $\tau$, or it
consists of an old path to a node $\alpha \in \modset$, a new path from $\alpha$ to a node $\beta
\in \modset$ and an old path from $\beta$ to $\tau$: 
%
\begin{equation}
  \begin{array}{@{}l@{}l@{}}
  \forall \sigma,\tau: \prtc\sigma\tau \liff{}
    & \redprtc{\sigma}{\tau} \lor{} \\
      & \exists \alpha,\beta\in \modset:
        \redprtc\sigma\alpha \land \prtc\alpha\beta \land \redprtc\beta\tau
  \end{array}
\equlabel{adapt:fotc}
\end{equation}
Notice that the condition $\alpha\in\modset$ is expressible as a quantifier-free formula using $\ufrtcSym$
when $\modset$ is specified, as it is, as a union of intervals.
Here $\redprtcSym$ denotes paths through edges, that are definitely unchanged, since at least one of their ends is outside $\modset$ (recall that $\ufsinglSym$ denotes $f$ before the change) ---
%
\begin{equation}
  \forall \alpha,\beta: \redp{\alpha}{\beta} \liff \upsingl{\alpha}{\beta} \land (\alpha\notin\modset \lor \beta\notin\modset)
\equlabel{adapt:redprtc}
\end{equation}

Thus we distinguish between edges that have both ends
in $\modset$, and therefore may have been altered in a manner described by
the ``ensures'' clause, and other edges, which were not changed at all.
We therefore define $\redpSym$ (and hence, its transitive closure,
$\redprtcSym$) to be exactly all the edges ``outside'' $\modset$, that is,
having {\bf at least} one end that does not belong to $\modset$ (\equref{adapt:redprtc}).
Now we define all the new paths $\prtcSym$ using $\redprtcSym$ and a restriction
of $\prtcSym$ to only edges inside $\modset$. See \figref{Edgesfq} for a depiction
of these labels. Every such new path is either
constructed entirely of ``old'' edges $\redpSym$, or is constructed from
some path $\prtcSym$ between two nodes $\alpha,\beta\in\modset$,
concatenated with some old prefix and some old suffix (each of which may be zero-length).
This is expressed by \equref{adapt:fotc}.

\begin{figure}
\centering
\begin{tikzpicture}[h/.style={fill=blue!10}]
    \def\cw{3mm}
    % (a)
    \begin{scope}[local bounding box=sa]
    \matrix[column sep=2mm] {
        \node[listnode,h,label=left:$\alpha$](a) {}; & &
        \node[listnode  ](t1) {\tiny 1}; & \node[listnode,h](t2) {\tiny 2}; &
        \node[listnode,h](t3) {\tiny 3}; & \node[listnode,h](t4) {\tiny 4}; \\
    };
    \draw[->] (t3) -- (t4);
    \draw[->] (t1) -- (t2); \draw[->] (t2) -- (t3);
    \draw[->] (a) edge[out=60] (t2);
    \end{scope}
    % (b)
    \begin{scope}[xshift=4cm,local bounding box=sb]
    \matrix[column sep=2mm] {
        \node[listnode,h,label=left:$\alpha$](a) {}; & &
        \node[listnode  ](t1) {\tiny 1}; & \node[listnode,h](t2) {\tiny 2}; &
        \node[listnode,h](t3) {\tiny 3}; & \node[listnode,h](t4) {\tiny 4}; \\
    };
    \draw[->] (t3) -- (t4);
    \draw[->] (t1) -- (t2); \draw[->] (t2) -- (t3);
    \draw[->] (a) edge[out=60] (t2);
    \draw[->] (a) edge[out=-60, in=-135] (t3);
    \end{scope}
    % (c)
    \begin{scope}[yshift=-2cm,local bounding box=sc]
    \matrix[column sep=2mm] {
        \node[listnode,h,label=left:$\alpha$](a) {}; & &
        \node[listnode](t1) {\tiny 1}; & \node[listnode,h](t2) {\tiny 2}; &
        \node[listnode](t3) {\tiny 3}; & \node[listnode](t4) {\tiny 4}; \\
    };
    \draw[->] (t3) -- (t4);
    \draw[->] (t1) -- (t2);
    \draw[->] (a) edge[out=60] (t2);
    \end{scope}
    % (d)
    \begin{scope}[xshift=4cm,yshift=-2cm,local bounding box=sd]
    \matrix[column sep=2mm] {
        \node[listnode,h,label=left:$\alpha$](a) {}; & &
        \node[listnode  ](t1) {\tiny 1}; & \node[listnode,h](t2) {\tiny 2}; &
        \node[listnode,h](t3) {\tiny 3}; & \node[listnode,h](t4) {\tiny 4}; \\
    };
    \draw[->] (t3) -- (t4);
    \draw[->] (t1) -- (t2);
    \draw[->] (a) edge[out=60] (t2);
    \draw[->] (a) edge[out=-60, in=-135] (t3);
    \end{scope}
    % Do all subfigure labels
    \foreach \x in {a,b}
       { \node[above=0mm of s\x] {\small(\x)}; }
    \foreach \x in {c,d}
       { \node[below=0mm of s\x |- sd.south] {\small(\x)}; }
    % Edges (a)-(c)  (b)-(d)
    \node(l1)[coordinate] at ($(sa |- sb.south)!.5!(sc.north)$) {};
    \draw[->] ($(sa.south |- sb.south)+(5mm,0)$) to[out=-60,in=60] ($(l1)+(5mm,0)$) to[out=-120,in=120]
            ($(sc.north)+(5mm,0)$);
    \node[right=5mm of l1] {\small{\tt del} $\abra{2,3}$};
    \node(l1)[coordinate] at ($(sb.south)!.5!(sd.north)$) {};
    \draw[->] ($(sb.south)+(5mm,0)$) to[out=-60,in=60] ($(l1)+(5mm,0)$) to[out=-120,in=120]
            ($(sd.north)+(5mm,0)$);
    \node[right=5mm of l1] {\small{\tt del} $\abra{2,3}$};
\end{tikzpicture}
\caption[Memory states with non-unique pointers where global reasoning about reachability is hard.]{\figlabel{NonUnique}%
Memory states with non-unique pointers where global reasoning about reachability is hard.
In the memory state~(a), there is one edge from $\alpha$ into the modified-set $\{1, 2, 3, 4\}$,
and in memory state~(b), there are two edges from $\alpha$ into the same modified-set, $\{1, 2, 3, 4\}$.
The two memory states have the same reachability relation and therefore are indistinguishable via reachability alone.
The memory states~(c)  and (d) are obtained from the memory states (a) and (b), respectively, by deleting the edge $\langle 2, 3\rangle$.
The reachability in (c) is not the same as in (d), which shows it is impossible to update
reachability in general w.r.t. edge deletion, without using the edge relation.}
\end{figure}

Taking the \newterm{adaptation rule} given by \equref{adapt:fotc} and \equref{adapt:redprtc} 
eliminates superfluous behaviours involving edges outside $\modset$ being changed.
Notice that these equations
are not specific to \tfind, but \equref{adapt:fotc} does assume that no new
path can enter and exit $\modset$ multiple times.
In this formula $\alpha$ is a placeholder for an \newterm{entry-point} into $\modset$ and $\beta$ is a placeholder for an \newterm{exit-point} from $\modset$.

The adaptation rule uses a logic which is too expressive and thus hard for automated reasoning: $\FOTC$ is not decidable
(in fact, not even recursively enumerable).
The first problem is that the $\redprtcSym$ relation is not usually first order expressible and generally requires transitive closure.
For example, \figref{NonUnique} shows that in general the adaptation rule is not necessarily
definable using only the reachability relation, when there are multiple
outgoing edges per node. We avoid this problem by only reasoning about functional fields, $f$.

The second problem with \equref{adapt:fotc} is that it contains quantifier alternation.
$\alpha$ matches an arbitrary node in $\modset$ which may be of
arbitrary size.
Therefore, it is not completely obvious how to avoid existential quantifications.

\subsection{An Adaptation Rule in a Restricted Logic}
\label{sec:adapt:afar}

We provide an equivalent adaptation rule in a restricted logic,
without extra quantifier-alternations.
Indeed, our assumptions from \secref{Inter:Problem:Working},
especially the deterministic reachability, single backbone field,
acyclicity, and 
the bound on the number of intervals in the mod-set,
greatly simplify reasoning about modified paths in the entire heap.
The simplification is obtained by employing an additional unary function symbol
$\en{\modset}$, where $\en{\modset}(\sigma)$ is meant to denote
 the entry
point of the node $\sigma$ in $\modset$, that is the {\em first} node on the (unique) path from $\sigma$ that enters $\modset$, and
$\vnull$ if no such node exists (see \figref{en-modp}).
Note that since transitive closure is only applied to functions, the entry points such as $\alpha$ in \equref{adapt:fotc} is uniquely determined by $\sigma$, the origin of the path.
A key property of $\en{\modset}$ is that on $\modset$ itself, $\en{\modset}$ acts as identity, and therefore for any $\sigma\in V$ it holds that
$\en{\modset}(\en{\modset}(\sigma)) = \en{\modset}(\sigma))$ ---
that is, the function $\en{\modset}$ is \newterm{idempotent}.
It is important to note
that $\en{\modset}$ does not change as a result of local modifications in $\modset$. Hence, we do not need to worry about $\en{\modset}$ in the pre-state as opposed to the post-state, and we do not need to parametrize $\en{\modset}$ by $f/\upsinglSym$.
Formally, $\en{\modset}$ is characterized by the following formula:

\begin{equation}
\begin{array}{ll}
\forall \sigma: &\uprtc{\sigma}{\en{\modset}(\sigma)} \land
\en{\modset}(\sigma) \in \modset \; \land \\
& \forall \alpha\in\modset: \uprtc{\sigma}{\alpha} \limplies \uprtc{\en{\modset}(\sigma)}
{\alpha}
\end{array}
\equlabel{enmodp:axiom}
\end{equation}

Using $\en{\modset}$ the new
adaptation rule $\adapt{\modset}$ is obtained by considering, for every source and target, the following
three cases:


\begin{figure}
\centering
\begin{tikzpicture}[en/.style={-stealth,dashed,shorten <=1mm,shorten >=1mm,draw=black!50,looseness=1.3}]
    \def\cw{4mm}
    \begin{scope}[start chain,node distance=\cw,a/.style={on chain,join},every join/.style={->}]
        \node(delta)[a,listnode] {};
        \node(spc1)[a,listnode] {};
        \node(alpha)[a,listnode] {};
        \node(beta)[a,listnode] {};
        \node(dummy)[a] {\ldots};
    \end{scope}
    \node(modp)[fit=(alpha) (dummy),modset,label={[modsetlbl]below:\small$\modset$}] {};
    \node(eps)[listnode,above=\cw of spc1] {};
    \node(spc2)[listnode,right=\cw of eps] {};
    \draw[->] (eps) -> (spc2); \draw[->] (spc2) -> (beta);
    \draw[en] (delta) to[out=-70,in=-110] node[below] {\small $\en{\modset}$} (alpha);
    \draw[en] (eps) to[out=40,in=80] node[above right] {\small $\en{\modset}$} (beta);
    \path[en,looseness=12] (alpha) edge[out=105,in=80] (alpha)
      (beta) edge[out=60,in=30] (beta);
    \node[right=0.3 of modp,align=left]
        {\small $\en{\modset} : V \to\modset$};
\end{tikzpicture}
\caption{\figlabel{en-modp}
The function $\en{\modset}$ maps every node $\sigma$ to
the first node in $\modset$ reachable from $\sigma$. Notice that
for any $\alpha\in\modset$, $\en{\modset}(\alpha)=\alpha$ by definition.}
\end{figure}

\begin{tabular}{rl}
{\bf Out-In:} & The source is out of $\modset$; the target is in;\\
{\bf In-Out:} &The source is in $\modset$; the target is out;\\
{\bf Out-Out:} & The source and target are both out of $\modset$.
\end{tabular}

The full adaptation rule is obtained by taking the conjunction of the
formulas for each case (\equref{out-in adaptation},
\equref{in-out adaptation},\equref{out-out adaptation}),
that are described below, and the formula defining $\en{\modset}$ 
(\equref{enmodp:axiom}).

\paragraph{Out-In Paths}

Using $\en{\modset}$ we can easily handle
paths that \emph{enter} $\modset$.
Such paths originate at some $\sigma\notin\modset$ and terminate
at some $\tau\in\modset$. Any such path therefore has to go through $\en{\modset}(\sigma)$ as depicted in \figref{adapt:depiction}.
Thus, the following simple formula can be used:

\begin{equation}
\equlabel{out-in adaptation}
\forall \sigma\notin\modset,\tau\in\modset:
            \prtc\sigma\tau \liff \prtc{\en{\modset}(\sigma)}{\tau}
\end{equation}

Observe that for some $\beta\in\modset$, the atomic formula used above, $\prtc{\en{\modset}(\sigma)}{\beta}$, corresponds to the FO(TC) sub-formula
$\exists\alpha: \redprtc{\sigma}{\alpha}\land\prtc{\alpha}{\beta}$
from \equref{adapt:fotc}.

\begin{figure}
\centering
\begin{tikzpicture}
    \def\cw{4mm}
    \node[listnode,label=above:$\sigma$](delta) {};
    \matrix(modp)[right=of delta, column sep=7mm, row sep=0mm] {
        \node(alpha)[listnode,label=above:$\alpha$] {}; & &
        \node(t1)[listnode,label=above:$t_1$] {}; \\
        & \node(beta)[listnode] {}; & \\
        & & \node(t2)[listnode,label=above:$t_2$]{}; \\
    };
    \node[modset,fit=(modp),label={[modsetlbl]below:$\modset$}] {};
    \node(epsilon)[listnode,right=of modp.-23,label=above:$\tau$] {};
    \draw[->,extpath] (delta) to[out=0,in=180] (alpha);
    \draw[->] (alpha) to[out=30,in=90] (beta);
    \draw[->] (beta) to[out=-90,in=-180] (t2);
    \draw[->,extpath] (t2) to[out=0,in=180] (epsilon);
    \node(dummy)[right=of t1] {\ldots};
    \draw[->,extpath] (t1) -- (dummy);
\end{tikzpicture}
\caption[Construction of a modified path from three segments]{\figlabel{adapt:depiction}%
This diagram depicts how an arbitrary path from $\sigma\notin\modset$
to $\tau\notin\modset$ is constructed from three segments:
$[\sigma,\alpha]_{\upsinglSym}$, $[\alpha,t_i]_f$, and
$[t_i,\tau]_{\upsinglSym}$ (here $i=2$).
Arrows in the diagram denote paths;
thick arrows entering and exiting the box denote paths that were not modified since they are outside of $\modset$.
Here, $\alpha=\en{\modset}(\sigma)$ is an entry-point and $t_1, t_2$ are exit-points.}
\end{figure}

\paragraph{In-Out Paths}

We now shift attention to paths that \emph{exit} $\modset$.
Exit points, that is, last points on some path that belong to $\modset$, are more subtle since you need both ends of the path to determine them.
The end of the path is not enough since it can be shared,
and the origin of the path is not enough
since it can exit the set multiple times, because a path may exit $\modset$ and enter it again later.
Therefore, we cannot define a function in a similar manner to $\en{\modset}$.
The fact that transitive closure is only applied to functions is useful here: every interval $[\alpha, \beta]$ has at most one exit $\beta$.
We therefore utilize the fact that $\modset$ is expressed as a bounded union of
intervals --- which bounds the potential exit points to a bounded set of terms.
We will denote the exit points of $\modset$ with $t_i$.

\begin{figure}
\begin{tabular}{@{}l@{ }l@{}}
@ requires & $E_f(x,f^1_x) \land E_f(f^1_x,f^2_x) \land E_f(f^3_x,f^2_x) \land $ \\
& $x \neq \vnull \land 
f^1_x \neq \vnull \land f^2_x \neq \vnull$ \\
@ $\modset$ & $[x,f^3_x]$\\
@ ensures &  \ldots
\end{tabular}
\begin{minipage}[t][1.8cm][b]{2cm}
{\alltt
\begin{tabbing}
vo\=id swap(Node x) \{\+\\
Node t = x.f;\\
x.f = t.f;\\
t.f = x.f.f;\\
x.f.f = t;\-\\
\}
\end{tabbing}
}
\end{minipage}
%
\def\cw{3.5mm}
\def\dw{4.5mm}
\begin{tikzpicture}[sll chain,k/.style={listnode,on chain},baseline=(modn)]
    \node(h)[k] {};
    \node(x)[k,label=above:\small$x$] {};
    \node(t)[k,join,label=below:\small$t$] {};
    \node(c)[k,join] {};
    \node(d)[k,join,label=above:\small{$f^3_x$}] {};
    \node(out)[k] {};
    \begin{scope}[local bounding box=ub]
    \path[-stealth,thick, dotted] (x) edge[out=45,in=120] (c)
        (c) edge[out=140,in=35] (t) (t) edge[out=-45,in=-135] (d);
    \end{scope}
    \node(modn)[modset,fit=(x) (t) (c) (d) (ub),inner xsep=0.5*\dw] {};
    \path[->] (h) edge (x) (d) edge (out);
    \node[modsetlbl,anchor=north east] at (modn.south east) {$\modn$};
\end{tikzpicture}
\caption{\figlabel{Swap}%
A simple function that swaps two adjacent elements following {\tt x} in a singly-linked list. 
Dotted lines denote the new state after the swap.
The notation e.g. $E_f(x,f_x^1)$ denotes the single edge from $x$
to $f_x^1$ following the $f$ field.}
\end{figure}

For example, in the procedure \tswap\ shown in \figref{Swap}, $\modset = [x, f^3_x]$ and there is one exit point $t_1=f^3_x$
($f^3_x$ is a constant set by the precondition to have the value of $f(f(f(x)))$ using
the inversion formula \equref{Inversion} introduced in \secref{Pointer:Deterministic}.

Observe a general path that originates at some $\sigma\in\modset$
and terminates at some $\tau\notin\modset$ (see \figref{adapt:non-convex}).
Evidently this path has to go though one of the $t_i\,$s,
which, as a result of our assumptions, are bounded.
Notice that the exit points, too, do not change as a result of modifying edges between nodes in $\modset$.
Assume a path from
$\sigma$ to $\tau$ and let $t_i$ be the last exit point along that path.
This is important, because it lets us know that the segment of the path between
$t_i$ and $\tau$ comprises solely of unchanged edges ---
since they are all outside of $\modset$.
We can therefore safely use $\uprtcSym$, rather than $\redprtcSym$, to characterize it.
As for the part of the path between $\sigma$ and $t_i$, it can be characterized simply by $\prtcSym$,
because $\sigma$ and $t_i$ are both in $\modset$.
Therefore the entire path can be expressed as $\prtc{\sigma}{t_i}\land \uprtc{t_i}\tau$. Consequently, we obtain the following formula:

\begin{equation}
\equlabel{in-out adaptation}
\begin{array}{l@{}l}
\forall \sigma\in\modset,{}&\tau\notin\modset: \prtc\sigma\tau \liff\\
&\bigvee_{t_i} (\prtc\sigma{t_i} \land \uprtc{t_i}\tau \;\land \;
\bigwedge_{t_j\neq t_i} t_j\notin[t_i,\tau]_{\upsinglSym})
\end{array}
\end{equation}

%We observe that in practice, most mod-sets are \newterm{convex},
%i.e., for every $\alpha,\beta\in\modset$, $[\alpha,\beta]_f\subseteq\modset$.
%This makes sure that every path has at most one exit point. In fact, if we assume %this, \equref{in-out adaptation} can be shortened to:
%\begin{equation}
%\begin{array}{l}
%\forall \sigma\in\modset,\tau\notin\modset: \\ \prtc\sigma\tau \liff
%\bigvee_{t_i} \prtc\sigma{t_i} \land \uprtc{t_i}\tau
%\end{array}
%\end{equation}
%Non-convex sets are rare in practice; for example, they occur if the same procedure
%manipulates several segments of the same list that are far apart.
%In such cases, we still need to assert that $t_i$ is the last exit point on the path from $\alpha$ to $\tau$ (see \figref{adapt:non-convex};
%see for example the code in \figref{swap_two:code}).
%For this, it is enough to require that $[t_i,\tau]_{\upsinglSym}$ contains no
%other exit points. Let $t_j$ be some other exit point,
%then checking $t_j\not\in[t_i,\tau]_{\upsinglSym}$ is a quantifier-free
%formula using the symbol $\uprtcSym$, which we are free to use with any
%pair of nodes.

Note that \equref{in-out adaptation}
corresponds to the sub-formula $\exists \beta: \prtc\alpha\beta \land \redprtc\beta\tau$
in \equref{adapt:fotc}.

\begin{figure}
\centering
\begin{tikzpicture}[nose/.style={minimum size=0.5mm,inner sep=0mm}]
    \def\cw{3mm}
    \matrix[column sep=8mm] {
        \node(delta)[listnode,label=above:$\sigma$] {}; &
        \node(en1)[nose] {}; &
        \node(t1)[listnode,label={[name=t1lbl]above:$t_1$}]  {}; &
        \node(beta)[listnode,label=above:$\tau_1$] {}; &
        \node(en2)[nose] {}; &
        \node(t2)[listnode,label={[name=t2lbl]above:$t_2$}] {}; &
        \node(epsilon)[listnode,label=above:$\tau_2$] {}; \\
    };
    \node[modset,fit=(en1) (t1) (t1lbl)](mod1) {};
    \node[modset,fit=(en2) (t2) (t2lbl)](mod2) {};
    \draw[->,extpath] (delta) -> (en1);
    \draw[->] (en1) -- (t1);
    \draw[->,extpath] (t1) -> (beta);
    \draw[->,extpath] (beta) -> (en2);
    \draw[->] (en2) -> (t2);
    \draw[->,extpath] (t2) -> (epsilon);
    \draw[modset,double] (mod1) to[out=-45,in=-135] node[above,modsetlbl] {\small$\modset$} (mod2);
\end{tikzpicture}
\caption{\figlabel{adapt:non-convex}%
A subtle situation occurs when the path from $\sigma$
passes through multiple exit-points. In such a case,
the relevant exit-point for $\prtc\sigma{\tau_1}$ is $t_1$,
whereas for $\prtc\sigma{\tau_2}$ and $\prtc{\tau_1}{\tau_2}$
it would be $t_2$.}
\end{figure}

\begin{figure}
\begin{alltt}
\begin{tabbing}
vo\=id swap_two(Node a, Node b) \{\+\\
swap(a); swap(b);\-\\
\}
\end{tabbing}
\end{alltt}
\caption{\figlabel{swap_two:code}%
An example of a procedure where the mod-set is not (essentially) convex.}
\end{figure}

%In our preliminary experience, most naturally occurring procedures fulfil this requirement.
%An artificial program with unbounded number of intervals is given in the appendix.
%This program manipulates the beginning of lists nested in an arbitrary-length outer list.


\paragraph{Out-Out Paths}

For paths between $\sigma$ and $\tau$, both outside $\modset$, there are two
possible situations:
\begin{itemize}
  \item The path goes {\bf through} $\modset$ (as in \figref{adapt:depiction}).
In this case, we can reuse the in-out case, by taking $\en{\modset}(\sigma)$
instead of $\sigma$.
  \item The path is entirely outside of $\modset$ (see \figref{adapt:outside}).
\end{itemize}

The corresponding formula in this case is:

\begin{equation}
\equlabel{out-out adaptation}
\begin{array}{l@{ }l}
\forall \sigma\notin\modset,{}&\tau\notin\modset: \prtc\sigma\tau \liff\\
&\bigvee_{t_i}(\prtc{\en{\modset}(\sigma)}{t_i} \land \uprtc{t_i}\tau \land \;
\bigwedge_{t_j\neq t_i} t_j\notin[t_i,\tau]_{\upsinglSym}) \;\lor \\
            &\en{\modset}(\sigma) = \en{\modset}(\tau) \land \uprtc{\sigma}{\tau}
\end{array}
\end{equation}

Notice that the second disjunct covers both the situation where $\sigma$ has a path that reaches some node in $\modset$ (in which case $\en{\modset}(\sigma)\neq\vnull$)
and the situation where it does not (in which case $\en{\modset}(\sigma)=\vnull$, so if $\prtc{\sigma}{\tau}$, then surely $\tau$ cannot
reach $\modset$ as well, hence $\en{\modset}(\tau)=\vnull$).

\begin{figure}
\centering
\begin{tikzpicture}
    \def\cw{4mm}
    \begin{scope}[start chain,every node/.style={on chain,listnode},every join/.style={->,extpath},node distance=10mm]
        \node(delta1) {};
        \node(epsilon1)[join] {};
        \node(alpha) {};
    \end{scope}
    \begin{scope}[yshift=-1cm,start chain,every node/.style={on chain},node distance=14mm]
        \node(delta2)[listnode] {};
        \node(epsilon2)[listnode,join={by ->,extpath}] {};
        \node(nul)[join=by extpath,yscale=.7] {};
    \end{scope}
    %\draw (epsilon2) to[extpath,ground={pos=1},red] (nul);
    % Put labels (must do that outside chain scope)
    \path (delta1) [late options={label=above:$\sigma_1$}]
        (epsilon1) [late options={label=above:$\tau_1$}]
        (alpha) [late options={label={[name=alphalbl]above:$\alpha$}}]
        (delta2) [late options={label=below:$\sigma_2$}]
        (epsilon2) [late options={label=below:$\tau_2$}];
    % Put alpha in a modset frame
    \node(modp)[fit=(alpha) (alphalbl) ($(alpha)+(right:1cm)$),label={[modsetlbl]below:\small$\modset$}] {};
    \path[modset] (modp.north east) -- (modp.north west) -- (modp.south west) -- (modp.south east);
    \draw[extpath,->] (epsilon1) -> (alpha);
    % Make an adorable, cute ``ground'' symbol
    \foreach \x/\y in {0.1/0, 0.55/0.2, 1/0.4} {
        \node(u)[coordinate] at ($(nul.north west)!\x!(nul.north east)$) {};
        \node(v)[coordinate] at ($(nul.south west)!\x!(nul.south east)$) {};
        \draw[extpath] ($(u)!\y!(v)$) -- ($(v)!\y!(u)$);
    }
\end{tikzpicture}
\caption{\figlabel{adapt:outside}%
Paths that go entirely untouched. $\en{\modset}(\sigma_1)=\alpha$,
whereas $\en{\modset}(\sigma_2)=\vnull$.}
\end{figure}

\bigskip
To conclude, given $\modset$, $\textrm{adapt}[\modset]$ is the conjunction
of the three formulas in \equref{out-in adaptation},
\equref{in-out adaptation}, \equref{out-out adaptation}, and the formula defining $\en{\modset}$ 
(\equref{enmodp:axiom}).
To show its adequacy, some more formalism is required, and it is introduced in the
following section.



\subsection{Adaptable Heap Reachability Logic}
\seclabel{Inter:Adaptation:Adaptable}

The utility of $\en{\modset}$ in the formulas for adaptation motivates the definition
of a logic fragment that would be able to accommodate it.

\begin{definition}
The new logic $\AEAR$ is obtained by augmenting $\AER$ with unary function symbols, denoted by $g,h_1 \til h_n$ where:
\begin{itemize}
\item $g$ should be interpreted as an idempotent function: $g\big(g(\alpha)\big)=g(\alpha)$.
\item The images of $h_1 \til h_n$ are all bounded by some pre-determined parameter $N$, that is: each $h_i$ takes at most $N$ distinct values.
\item All terms involving these function symbols have the form $f(z)$, where $z$ is some variable.
\end{itemize}
\end{definition}

We later show that $\AEAR$ suffices for expressing the verification conditions of the programs discussed above. In the typical use case, the function $g$ assigns the entry point in the mod-set
for every node (called $\en{\modp}$ above), and the functions $h_1 \til h_n$
are used for expressing the entry points in inner mod-sets.
The main attractive feature of this logic is given in the following theorem.

\begin{theorem}
\thelabel{AEAR-translation}
Any $\AEAR$-formula $\varphi$ can be translated to an equal-valid (first-order)
function-free $\forall^*\exists^*$-formula.
\end{theorem}

The proof of \theref{AEAR-translation}, given in \appref{Proofs:Simulation}, begins by translating 
$\varphi$ to a $\forall^*\exists^*$-formula $\varphi'$ as described in \proref{AER-simulation}, 
keeping the function symbols $g,h_1\til h_n$ as is.  These function symbols are then replaced 
by additional relation and constant symbols,
and extra universal relational formulas are used to enforce the semantic restrictions of them.
