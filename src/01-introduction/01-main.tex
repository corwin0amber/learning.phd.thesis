\seclabel{Intro:Main}

\subsection{Deterministic Transitive Closure}

A key to the reasoning techniques presented in this thesis is the concept of
\newterm{deterministic transitive closure} and its introduction into
first-order logic. We begin by defining the notation $\nextrtcInfix$, which
denotes the reflexive transitive closure of a unary function symbol 
$\nextsinglSym$. The semantics are that $\nextrtc x y$ is $\true$ iff there
is a sequence of successive applications $\nextsinglSym$ to $x$ that results in 
$y$ ($\nextsinglSym(\nextsinglSym(\cdots x\cdots))=y$).
The restriction that transitive closure can only be applied
to functions is what makes it deterministic. As we will see, this form of
transitive closure is much simpler to handle. This has been noticed before,
in other contexts, e.g. in~\cite{ImmermanRRSY:2004:CSL}.

It is then shown that $\nextrtcInfix$ can be axiomatized in pure first-order
logic, in the same way that first-order logic with equality can be axiomatized
in first-order logic (without equality) by adding the equality axioms.
One important restriction, however, is that once the transitive closure is
introduced, the original function cannot be used in logical terms anymore---
because the relationship between $\nextsinglSym$ and $\nextrtcSym$ is not
first-order-expressible.
While this seems severe, it turns out that many useful properties of linked
lists and some other linked data structures can be expressed this way. It is
analogous to reasoning about natural numbers without using $\mathit{succ}$
(the successor function) but with the relation $\leq$. In fact, it is easy
to see that using quantification one can define $\mathit{succ}$ in terms
of $\leq$:

\begin{equation}
\mathit{succ}(x)=y ~~\impliesBothWays~~ \forall \alpha: (x\leq\alpha \land \alpha\neq x) \liff y\leq\alpha
\end{equation}

This leads to a second restriction: the axiomatization of $\nextrtcInfix$ that
we construct in \secref{Pointer:Deterministic} is
complete, but only for finite structures; hence, it is essential that the logic
used for reasoning has the \newterm{finite model property}: if a formula in the
logic has a model, then it also has a finite model. This is not true in general,
of course, for first-order formulas. One fragment of first-order logic that does
have this property will receive a lot of attention throughout this thesis is
the Bernays-Sch\"onfinkel-Ramsey class---also referred to as 
\newterm{effectively propositional} (EPR). It is characterized by a relational
vocabulary, that is, only relation symbols and constants may occur in the signature
and no non-nullary function symbols, and a quantifier prefix limited to $\exists^*\forall^*$
so that all existential quantifiers precede the universal ones.
Again, we show a range of benchmarks demonstrating specifications that fall well
within this restriction. 
In fact, most of the time, just universal formulas suffice to express desired properties. 

In particular, it is very important to be able to express in a precise manner
the effect of heap mutations performed by the program. A commonly used solution
is to model the heap as a long array of pointers and to use McCarthy's axioms
defining the update of an array $a$ at index $i$ to the value $e$ as $a\{i\gets e\}$.
However, the use of transitive closure will lead us to expressions of the form
$\langle (\nextsinglSym\{i\gets e\})^*\rangle$, which cannot be handled using
the decidable logical fragment that we are interested in.

\begin{figure*}
\centering
\begin{tikzpicture}
    \def\cw{4mm}
    \def\dw{5mm}
    \matrix[column sep=\dw, row sep=\dw, label distance=3mm, font=\tiny] {
        \node(a)[listnode] {1}; &
        \node(b)[listnode,label={[name=ptr-i]above:\small$i$}] {2}; &
        \node(c)[listnode,label={[name=ptr-j]above:\small$j$}] {3}; &
        \node(d)[listnode] {4}; &
        \node(e)[listnode] {5}; \\
    };
    \path[->] (ptr-i) edge (b) (a) edge (b) 
              (ptr-j) edge (c) (c) edge (d) (d) edge (e) ;

    \begin{scope}[yshift=-2cm]
    \matrix[column sep=\dw, row sep=\dw, label distance=3mm, font=\tiny] {
        \node(a)[listnode] {1}; &
        \node(b)[listnode,label={[name=ptr-i]above:\small$i$}] {2}; &
        \node(c)[listnode,label={[name=ptr-j]above:\small$j$}] {3}; &
        \node(d)[listnode] {4}; &
        \node(e)[listnode] {5}; \\
    };
    \path[->] (ptr-i) edge (b) (a) edge (b) (b) edge (c)
              (ptr-j) edge (c) (c) edge (d) (d) edge (e) ;
	\end{scope}
	
    \node at (-4,-0.4) {(i)};
    \node at (-4,-2.4) {(ii)};
    
    \begin{scope}[label distance=3mm]
    \node(stmt)[draw,label={[name=knob1]above:},label={[name=knob2]below:},] at (-3,-1.4) {i.next := j};
    \draw[->] (knob1) -- (stmt) -> (knob2);
    \end{scope}
\end{tikzpicture}
\caption{\figlabel{i.n:=j:snap}%
The effect of an assignment statement on the heap viewed as a directed graph.}
\end{figure*}

The proposed solution is to model the heap abstractly as a directed graph, and
the transitive closure of the edges as a view of this graph. It is then our task
to maintain the view across changes to the edge set of the graph. As a very basic
example, \figref{i.n:=j:snap}(i) shows a snapshot of a heap containing two linked
lists. The $\nextsinglSym$ edge set is 
$\{\langle 1,2\rangle, \langle 3,4\rangle, \langle 4,5\rangle\}$
and the transitive closure $\nextrtcSym$ is the binary relation
$\{\langle 1,2\rangle, \langle 3,4\rangle, \langle 3,5\rangle, \langle 4,5\rangle\}$.
The depicted assignment statement {i.n := j} causes the insertion of a new edge
$\langle 2, 3\rangle$, which materializes many new paths in the view of $\nextrtcSym$,
in particular: $\langle 1, 3\rangle, \langle 1,4\rangle, \langle1,5\rangle,
\langle 2, 3\rangle, \langle 2,4\rangle, \langle2,5\rangle$ (as shown in \figref{i.n:=j:snap}(ii)); that is,
all the pairs where the first element belongs to the first list $1\to 2$ and the second
element belongs to the second list $3\to 4\to 5$. This can be formulated in logic as
a view update:

\begin{equation}
\nextrtc \alpha \beta ~:=~ \nextrtc \alpha \beta \lor \big(\nextrtc\alpha i \land \nextrtc j\beta\big)
\end{equation}

This shows that $\nextrtcSym$ can be updated using only previous values of
$\nextrtcSym$. Moreover, this particular update is quantifier-free, which plays
well with the quantifier prefix limitations of EPR explained before. \charef{Pointer}
and \charef{Inter} extensively investigate the capability to express various kinds
of view updates in a logic with restricted vocabulary and quantifiers.


\subsection{Idempotent Functions in EPR}

The use of EPR strictly rules out any function symbols other than constants.
As a corollary, we point out an interesting case where the use of a function
is benign---in the case where there is only one function symbol, and this function
is \newterm{idempotent}, that is, it is deducible from the axioms that 
$\forall \alpha: f(f(\alpha)) = f(\alpha)$.
In this case we show that there is a reduction from the satisfiability problem
of formulas in the language that includes $f$ to satisfiability of EPR formulas
without functions, but with the addition of a finite number of constants
and variables. The increase in formula size is linear in the number of symbols used.
Therefore, adding such a function symbol $f$ does not break the decidability
property of the logical fragment.
This extension can be used to write formulas in a more natural way.

\subsection{Procedural Reasoning with Adaptation}

When dealing with a procedure that manipulates a linked-list segment or several
linked-list segments, the most difficult issue is the existence of \newterm{cutpoints}~\cite{POPL:RinetzkyBRSW05},
which are pointers from other areas of the heap that reach nodes of the list
being manipulated. Such pointers are represented in our abstraction of the heap
as edges, which the procedure does not modify and in fact may not even be able to
observe, but may participate in paths that are being connected or disconnected by it.
As an example, assume that the numbered list in \figref{cutpoint:snap} is being sorted
by some sorting procedure. The node pointed to by $i$ is connected to the node
4 by a pointer field. Before the sorting, the set of nodes reachable from $i$ is,
as seen in the figure, $\{4, 1, 3\}$. However, once the list is reordered according
to the numbering, the reachable set would become $\{4, 5\}$. This is despite the
fact that the sorting routine does not change $i$ or the outgoing edge.

\begin{figure*}
\centering
\begin{tikzpicture}
    \def\cw{4mm}
    \def\dw{5mm}
    \matrix[column sep=\dw, row sep=\dw, label distance=3mm, font=\tiny] {
    	&\node(z)[listnode,label={[name=ptr-i]above:\small$i$}] {}; & & & \\
        \node(a)[listnode] {2}; &
        \node(b)[listnode] {5}; &
        \node(c)[listnode] {4}; &
        \node(d)[listnode] {1}; &
        \node(e)[listnode] {3}; \\
    };
    \path[->] (ptr-i) edge (z) (z) edge (c) (a) edge (b) 
              (b) edge (c) (c) edge (d) (d) edge (e) ;
\end{tikzpicture}
\caption{\figlabel{cutpoint:snap}An example of a cutpoint into a linked list.}
\end{figure*}

To support such reasoning, we define the notion of a \newterm{mod-set}, which is
the area of the heap being directly mutated by the procedure, and an \newterm{entry function}
used to describe the cutpoints by associating every ``foreign'' node from outside
the mod-set with the first location in the mod-set that is reachable from that node.
This allows us to formulate an \newterm{adaptation rule} that determines how
the reachable set is modified for every node outside the mod-set, essentially
providing a complete, precise characterization of the reachability relation $\nextrtcInfix$
for the entire heap.

Having observed the behavior of several heap-manipulating procedures, we found
that they share a common desirable property: the amount of shared location
introduced by a single function call is bounded, and this bound can be known
at compile time. We take advantage of this property in order to produce
modular verification conditions that do not contain quantifier alternation,
thus supporting our propositional reasoning based on the small-model property.

We use the fact that the entry function is idempotent to support decidable
reasoning using the adaptation rule within the scope of EPR as explained in the
previous paragraph. 
Thanks to this property, verification rules generated for the modular case are
still decidable for validity.

\subsection{Template-based Verification with Induction}

The downside of essentially every Floyd-Hoare verification technique is that
it requires analyzed programs to be annotated with a \emph{loop invariant} for
every loop that occurs in the program. Furthermore, the supplied invariants should
satisfy the restriction that they are \emph{inductive}, that is, any single loop 
iteration should preserve their validity by induction: if invariant $I$ holds at
iteration $k$, we must be able to conclude that it holds at iteration $k+1$ 
(if such an iteration exists, of course) without knowing anything else about the
state. This puts a heavy burden on a programmer who wishes to verify her program
using such a method, since the discovery of an appropriate inductive invariant
is usually not very intuitive.
Furthermore, a small change in the program or in its specifications may
require considerable change in the loop invariant.

We address the problem of \newterm{inductive invariant inference} by
proposing a set of logical templates to serve as building blocks for a
space of candidate invariants. For example, the following formulas are
among the ones used as templates:
\[
\begin{array}{l@{\qquad}l@{\qquad}l@{\qquad}l}
x=y & \nextsingl x y & \nextrtc x y & \forall \alpha:\nextrtc x \alpha \land \nextrtc y \alpha \limplies \alpha=\vnull
\end{array}
\]

The symbols $x$ and $y$ serve as placeholders to be replaced by program
variables. Based on our experiment, we constructed the search space as
all possible Boolean combinations of template instances. In the example
program $\progreverse$, there are 4 program variables; therefore 16 different
assignments for $x$ and $y$, giving a total of 64 instances. To take into
account Boolean combinations of these, each of them may occur positive
or negated ($\lnot$), so the basic ``literals'' are 128, from which $2^{128}$
disjunctive \newterm{clauses} may be formed. The number of combinations
is therefore the number of possible conjunctions, $2^{2^{128}}$.

Undoubtedly, this search space is much too vast for a na\"ive generate-and-test
approach. Fortunately, there are search techniques far superior.
Recent developments in model checking have given rise to the IC3 algorithm for
inferring inductive invariants given safety properties, which are usually non-inductive;
IC3 can be used to find a stronger property---that is, one that entails safety---%
that is inductive. It is much more efficient than an explicit exploration of the
state space needed in order to discover all the reachable states.
Although originally developed to verify hardware systems, such
as Boolean circuits, where the state is represented as a string of bits of a known
length, we have found that it combines well with \emph{predicate abstraction},
which allows to extend its use to software systems with unbounded state resulting
in infinitely many states.

We have effectively applied IC3 with predicate abstraction for the domain of
linked-list programs containing loops. The IC3 routine requires a theory solver,
and the EPR-based encoding of deterministic transitive closure makes a perfect
match. To use it, we made sure that all the abstraction predicates are expressible
as universal formulas in the language containing $\nextrtcInfix$, and by careful
construction of the two-vocabulary formula $\rho(X,X')$, where $X$ is a set of
``state symbols'', which are all the program variables + the special symbol $\nextrtcSym$,
and $X'$ are primed versions of all these symbols, we obtained an instance of IC3
where all the satisfiability queries encountered during its execution are of EPR
formulas. This ensures termination and relative completeness of the implementation.
That is, if an inductive strengthening of the safety property exists and is expressible
as a Boolean combination of the abstraction predicates, the algorithm is guaranteed
to find it.

Consequently, we were able to automatically infer invariants for all the loops
for which we initially had to write inductive invariants manually. It should
be noted that invariants are restricted to a ``weak'' language, and that
the results discovered by the algorithm are very different than the ones
a programmer would write naturally.

\bigskip
The above techniques proved effective for many naturally occurring programs.
Successful benchmarks include examples from the TVLA shape analysis
framework~\cite{kn:TVLASAS} using singly- and doubly-linked lists and
nested lists, as well as some textbook algorithm implementations:
several sorting routines and the union-find data structure developed
by Tarjan~\cite{JACM:Tar75}.
