
This thesis develops means for automated reasoning for the purpose of proving
the correctness of computer programs making excessive use of pointers.
These include programs manipulating linked data structures, such as linked lists,
doubly-linked lists, nested lists, and reverse trees. For automation we use
industry-standard tools whose efficiency has been proven in practice. The
proposed logical frameworks reduces verification problems into logical queries.
We show that the set of queries thus generated is a decidable one, so a definite
answer (``yes'' or ``no'') is guaranteed.

The problem of software verification is about as old as software,
perhaps even older. Floyd and Hoare~\cite{Floyd67,Hoare69} proposed logical
frameworks to construct correctness proofs for programs with respect
to formal specifications---\newterm{full correctness proofs}, which contain
a proof for the program's termination on its designated set of input,
as well as the adherence of its behavior to the one registered in
the specification; and the more popular \newterm{partial correctness proofs},
which relax the termination requirement. A continued effort to mechanize
the construction of such proofs existed ever since.
Cousot and Cousot~\cite{kn:CC77} brought the advancement of 
\newterm{abstract interpretation}, providing a multitude of
techniques used to analyze software in various domains. Simple abstract
interpretation techniques are employed day-to-day by compilers due to their
elegance, ease of implementation, and good performance. Naturally, there
is a trade-off between resource usage and accuracy of the analysis;
as a consequence such analyses, which are based on simple abstractions,
mostly provide approximate results.

Within the problem space of software verification, a particularly interesting
subset of programs is those making heavy use of pointers. In C and the C-style
programming languages that emerged as a result of its success, pointers are
basic working tools just like arithmetic operators and control structures,
combining expressivity and low-level efficiency. \newterm{Separation logic},
mostly due to Reynolds~\cite{LICS:reynolds02separation} and O'Hearn~\cite{CSL:OHearnRY01,POPL:IshtiaqO01}, 
has evolved to address this challenging aspect of programming. 
Challenges arise, by-and-large, by the occurrence of \newterm{aliasing}
in pointer programs: the situation where two pointers contain the same address,
hence a change in data stores at that address is visible at once in two
places in the program. Consider the simple number-incrementing procedure (written in C):

\begin{minipage}{0.7\textwidth}
\begin{alltt}
\begin{tabbing}
vo\=id go_up(int *x, int *y) \{ \+ \\ 
(*x)++; (*y)++; \- \\ 
\}
\end{tabbing}
\end{alltt}
\end{minipage}

The programmer's intention is to increase both counters. However, in the
corner case where the parameters {\tt x} and {\tt y} store the same address
(are aliased), the result would be increasing one counter by two. If
the programmer did not plan this scenario, it could lead to intricate 
hard-to-find bugs at runtime. An attempt to formally prove the correctness
of this procedure would immediately give rise to a candidate specification,
such as the one expressed by the equations:
\[\begin{array}{l} [x] = \underline{[x]}+1\\ {}[y] = \underline{[y]}+1
  \end{array}\]

Here we employ a somewhat standard notation where $\underline{x}$
denotes the \emph{input} value of the program variable {\tt x}, and 
plain $x$ denotes its \emph{output} value. The square brackets indicate
that the equation holds on the values stored in the address given by
the variable, not on the addresses themselves.

From here it is plain to see the defect: in the case where $x=y$ (aliasing)
the specification contradicts the program. This is because the specification
is \emph{declarative}, so the order of equations is insignificant and in
fact the two equalities state the same property: that the memory location
referenced by both $x$ and $y$ has its value increased by one. In the program's
operational semantics, however, the fact that the assignment repeats
itself is, of course, significant---the value will be increased by two.
Using a formal semantics of the language would systematically reveal this
discrepancy.

\begin{figure}[t]
\begin{align*}
j \assign{} &\vnull ~;~ \WHILE~ i \neq \vnull ~\DO \\
&(k \assign i.\nextsinglSym ~;~ i.\nextsinglSym \assign j ~;~ j \assign i ~;~ i \assign k)
\end{align*}
\caption{\figlabel{Reynolds-reverse:code}
 A classical program that performs in-place reversal
 of a list, adapted from~\cite{LICS:reynolds02separation}}
\end{figure}

Complex composition of pointers leads to more involved reasoning.
The program shown in \figref{Reynolds-reverse:code} is an early example
used by Reynolds. The program reverses the order of elements in a linked list:
its input is a linked list whose head
is pointed to by $i$ and each node contains a field $\nextsinglSym$
holding a pointer to the next node (or $\vnull$ to signify the last node).
The program outputs a list of the same structure, only that the elements
occur in reversed order. The reversal is done in-place, so that the
original input list is overwritten by the output.

\begin{figure*}
\centering
\begin{tikzpicture}
    \def\cw{4mm}
    \def\dw{5mm}
    \matrix[column sep=\dw, row sep=\dw, label distance=3mm, font=\tiny] {
        \node(a)[listnode,label={[name=ptr-i]left:\small$i$}] {1}; &
        \node(b)[listnode] {2}; &
        \node(c)[listnode] {3}; &
        \node(d)[listnode] {4}; &
        \node(e)[listnode] {5}; \\
    };
    \path[->] (ptr-i) edge (a) (a) edge (b) (b) edge (c) (c) edge (d) (d) edge (e) ;

    \begin{scope}[yshift=-1cm, xshift=3mm]
    \matrix[column sep=\dw, row sep=\dw, label distance=3mm, font=\tiny] {
        \node(a)[listnode] {1}; &
        \node(b)[listnode,label={[name=ptr-j]above:\small$j$}] {2}; &
        \node(c)[listnode,label={[name=ptr-i]above:\small$i$}] {3}; &
        \node(d)[listnode] {4}; &
        \node(e)[listnode] {5}; \\
    };
    \path[->] (ptr-j) edge (b) (b) edge (a) 
              (ptr-i) edge (c) (c) edge (d) (d) edge (e) ;
	\end{scope}
	
    \begin{scope}[yshift=-2.8cm, xshift=6mm]
    \matrix[column sep=\dw, row sep=\dw, label distance=3mm, font=\tiny] {
        \node(a)[listnode] {1}; &
        \node(b)[listnode] {2}; &
        \node(c)[listnode] {3}; &
        \node(d)[listnode] {4}; &
        \node(e)[listnode,label={[name=ptr-j]right:\small$j$}] {5}; \\
    };
    \path[->] (ptr-j) edge (e) (e) edge (d) (d) edge (c) (c) edge (b) (b) edge (a);
    \end{scope}
    
    \node at (-4,0) {(i)};
    \node at (-4,-1.4) {(ii)};
    \node at (-4,-2.8) {(iii)};
\end{tikzpicture}
\caption{\figlabel{Reynolds-reverse:snap}%
The state of the memory during the execution of the list reversal program:
(i) initial state; (ii) an intermediate state; (iii) final state.}
\end{figure*}

Reynolds identified an acute problem when reasoning with programs
that traverse such recursive data structures: the pointer $i$ serving
as the iterator is advanced at each step, and the number of steps is
not bounded. Therefore there is always a risk that a value written on
one iteration will be overwritten in subsequent iterations. In particular,
to make sure that the list remains acyclic in this example, one must
obtain that there is no $\nextsinglSym$-path from $j$ to $i$, otherwise
the addition of the edge $\langle i, j\rangle$ introduces a cycle.

We approach this problem by a careful construction of appropriate loop
invariants for iterative programs, and comprehensive summaries for
recursive programs.
Observing a typical run of $\progreverse$ (\figref{Reynolds-reverse:snap}),
an important property of it can be noticed: the pointer variables $i$ and $j$
always point into the beginning of two \textbf{disjoint} list segments.
Either segment may be empty (as in (i) and (iii)), but the segments never
share elements. It turns out that this property is crucial to prove the
correctness of the list reversal program. Formulating this property in
logic is more involved than the previous, simpler aliasing conditions. 
To address this issue, we define \newterm{reachability logics} and support
reasoning to check the validity of implications. In this approach, we would
write an invariant such as
\begin{equation}
\forall \alpha: \nextrtc i\alpha \land \nextrtc j\alpha \limplies \alpha=\vnull
\equlabel{Reverse:loopinv-nonshared}
\end{equation}

The concern raised by Reynolds was that such an approach would never scale.
To alleviate this, we suggest breaking the program down into small pieces
where the properties are simple enough, then combining the sub-proofs
to verify the whole program.
We show that for many naturally occurring instances the invariants are quite managable
and automatic reasoning is tractable. To scale up we continue to develop logical
tools for modular reasoning.  
We return to discuss the $\progreverse$ example in much detail in \secref{Pointer}.

In this thesis, we draw primarily on the development of Hoare logic and
its extensions, generally referred to as ``Hoare-style verification''.
Hoare logic is a proof system for reasoning about programs with respect to
their specifications, given as assertions---generally pre-condition and
post-condition---written in a logical language of choice.
While complete systems exist for Turing-complete imperative programming languages
(one example is presented in the preliminaries of this thesis),
the problem of proof search is a primary obstacle to implementing automated
verification and program reasoning systems. This is true even for very small
programs, since a proof is required to use formulas (assertions) that do not occur in
the specification or in the program itself (such as \equref{Reverse:loopinv-nonshared} above).
Even when these are given, verifying that they construct a valid proof
is an undecided problem, in general, since it requires proving the validity
of formulas in the assertion language.
The problem is especially difficult when programs include loops.
A loop in the program can cause a code block to be executed arbitrarily many times,
so that the number of states a program visits during its execution is 
disproportionate to the size of the program. Reasoning about sets of states
is, in general, a higher-order problem.

This thesis attempts to greatly simplify reasoning by reducing the proof
obligations that need to be checked to proving validity of sentences in
propositional calculus. Thus we show that the program's assertions are
correct if and only if some propositional formula is valid---or equivalently,
its negation is unsatisfiable. Boolean satisfiability is a decidable problem,
which is usually solved by dedicated software known as SAT solvers.
A lot of engineering effort went into this kind of software over the 
years, and in practice, such instances are solved very efficiently and effectively.

Two central observations underpin our method.
(i)~In programs that manipulate singly- and doubly-linked lists it is possible to express the `next' pointer in terms of the reachability relation between list elements. This permits direct use of recent results in descriptive complexity~\cite{Phd:Hesse03}: we can maintain reachability
with respect to heap mutation in a precise manner. Moreover, we can axiomatize reachability using quantifier-free formulas.
(ii)~In order to handle statements that traverse the heap, we allow verification conditions (VCs) with $\forall^*\exists^*$ formulas so that they can be discharged by SAT solvers (as we explain shortly). However, we allow the programmer to only write assertions in a restricted fragment of FOL
that disallows formulas with quantifier alternations but allows reflexive transitive closure. 
The main reason is that invariants occur both in the antecedent and in the consequent of the VC for loops;
thus the assertion language has to be closed under negation,
although the verification conditions are not required to have this property.

\begin{figure}
\centering
\begin{minipage}[b]{0.7in}
\xymatrix@C70pt{
  *++[F]{state}\ar[r]^{\textrm{database-update}}\ar[d]^{query} & *++[F]{state}\ar[d]^{query}\\
  *++[F]{qstate} \ar[r]^{\textrm{query-update}} & *++[F]{qstate}
}
\end{minipage}
\caption{\figlabel{View-Update}The view update problem, naturally occurring
  in databases but also applies to heap reachability.}
\end{figure}

The appeal to descriptive complexity stems from the fact that previously
it has been applied to the view-update problem in databases.  This
problem has a pleasant parallel to the heap reachability update
problem we are considering. In the view-update problem, the logical
complexity of updating a query w.r.t.~database modifications is lower
than computing the query for the updated database from scratch
(depicted in \figref{View-Update}). Indeed, the latter uses formulas with
transitive closure, while the former uses quantifier-free formulas
without transitive closure. In our setting, we compute reachability
relations instead of queries. We exploit the fact that the logical
complexity of adapting the (old) reachability relation to the updated
heap is lower than computing the new reachability relation from
scratch. The solution we employ is similar to the use of dynamic graph
algorithms for solving the view-update problem, where directed paths
between nodes are updated when edges are added/removed (e.g., see
\cite{EL:DI08}), except that our solution is geared towards
verification of heap-manipulating programs with linked data
structures.

Another aspect that complicates programmatic reasoning, especially with complex
states as is the case when pointer-based data structures are present, is
procedures. Programs are usually factored into several sub-programs in the
interest of readability and code reuse. This common idiom causes one code
block to be executed in different contexts, and it is highly desirable for
reasons of scalability not to have to verify it for each context separately.
The challenge is to be able to express the view-update that summarizes the
effect of a procedure call in an arbitrary context, where some of the elements
are not reachable by the procedure, and therefore essentially remain unmodified.
This may be seen as an instance of the \newterm{frame problem}.

The rest of this thesis introduces \newterm{reachability logics}, a formal
definition of logical fragments found useful for the systematic reasoning
over programs containing pointer structures. As a primary technique, the
semantics of such logics are embedded in first-order logic for the use
of automated solvers. While a severe limitation on the expressivity of
the defined logic, automated proof techniques prove to be so effective
compared to manual proofs, even for small, seemingly-obvious examples, 
that there is much benefit to using
them whenever possible.

The results in this thesis were published in 
\cite{cav:IBINS13}, \cite{cav:IBRST14}, and \cite{popl:IBILNS14}.
