\seclabel{Pointer:Extending}

\begin{headlines}
To handle programs with pointers, we need to address program statements involving pointers
{\blip}
Writes are analogous to edge addition or deletion
{\blip}
Reads are edge traversals
{\blip}
This provides axiomatic semantics for programs based on TC
\end{headlines}

\bigskip

In this section we show how to express the weakest liberal preconditions of atomic heap manipulating statements using
\FOAE\ formulas, for programs that manipulate acyclic singly-linked lists. 
\tabref{Wprules} shows standard $\wlpsym$ computation rules (top part) and the 
corresponding rules for field update, field read and dynamic allocation (bottom part). 

 \begin{table}
\[
\begin{array}{|rcl|}
\hline
% Destructive Updates
  \wlp{x.\nextf := \vnull}(Q) &\eqdef&
      \substitute{Q}{\alpha \B{\nextf^*} \beta}{\alpha \B{\nextf^*}\beta \land (\lnot \alpha \B{\nextf^*}x \lor \beta \B{\nextf^*}x)} \\
  \wlp{x.\nextf := y}(Q) &\eqdef& \lnot y\B{\nextf^*}x \land{}\\
      && \substitute{Q}{\alpha \B{\nextf^*} \beta}{\alpha \B{\nextf^*}\beta \lor (\alpha \B{\nextf^*}x \land y \B{\nextf^*}\beta)}\\
% Traversal
 s\B{\nextf^+}t &\eqdef&
      s\B{\nextf^*}t \land s\neq t\\
  s\B{\nextf}t &\eqdef&
      s\B{\nextf^+}t \land 
       \forall \gamma : s\B{\nextf^+}{\gamma} \limplies
                          t\B{\nextf^*}\gamma\\
  \wlp{x := y.\nextf}(Q) &\eqdef&
      \forall \alpha : x\B{\nextf}\alpha \to \substitute{Q}{x}{\alpha}\\
% Allocation
  \wlp{x := \NEW}(Q) &\eqdef&
      \forall \alpha : \left(\Land_{p\in\Pvar\cup\{\nullv\}} \neg p \B{\nextf^*} \alpha\right) \limplies
         \substitute{Q}{x}{\alpha} \\
\hline
\end{array}\]
\caption{\tablabel{Wprules}Rules for computing weakest liberal preconditions
for an extension of While-language to support
heap updates, memory allocation, and pointer dereference.}
\end{table}

\paragraph*{Destructive Update.}
The correctness of the rule for destructive field update is according to \equref{Dyn-Comp-REACHacycdet}.
The statement $x.\nextf := y$ corresponds to \textbf{ins}, and $x.\nextf := \vnull$ corresponds to \textbf{del}.
This latter one requires some adaptation because in the update rule for 
\textbf{del} both ends of the deleted edge ($a,b$) are used, whereas
the statement $x.\nextf := \vnull$ only explicates the source of the edge.
Under the assumption that $\nextf$ is deterministic and acyclic, though,
the two are equivalent.

Notice the requirement $\lnot y\B{\nextf^*}x$ in $\wlp{x.\nextf := y}(Q)$:
it makes sure that an edge ($x,y$) being added does not introduce a cycle
into the list. So in fact, the $\wlpsym$ asserts both the required post-condition $Q$
\textbf{and} the absence of cycles throughout the run of the program.


\paragraph*{Field Dereference.}
The rationale behind the formula for $\wlp{x := y.\nextf}(Q)$ is that if $y$ has a
successor, then the formula $Q$ should be satisfied when $x$ is replaced by this
successor.
The natural way to specify this is using the Hoare assignment rule
\[
\wlp{x := y.f}(Q) \eqdef \substitute{Q}{x}{f(y)}
\]
However, this rule uses the function \nextf\ and does not directly express reachability.
Instead we will construct a relation $r_f$ such that
$r_f(\alpha, \beta) \liff f(\alpha) = \beta$ and then use universal quantifications to ``access'' the value
\[
\wlp{x := y.f}(Q) \eqdef \forall \alpha: r_f(y, \alpha) \limplies \substitute{Q}{x}{\alpha}
\]

Since, for regular singly-linked lists, $\nextf$ is acyclic, we do not need a symbol for $r_\nextf$ --- 
we can express $r_\nextf$ in terms of $\nextf^*$ as follows.
First we observe that $\nextf(\alpha)\neq\alpha$.
Also, since $\nextf$ is a function, the set of nodes reachable from $\alpha$ is totally ordered by $\nextf^*$. 
Therefore, similarly to \equref{Inversion}, we can express $r_\nextf(\alpha, \beta)$ as the minimal node $\beta$ in this order where $\beta \neq \alpha$.
Expressing minimality ``costs'' one extra universal quantification.

In \tabref{Wprules}, formula $s\B{\nextf}t$ expresses $r_\nextf(s,t)$ in terms of $\nextf^*$:
$s\B{\nextf}t$ holds if and only if there is a path of length 1 between $s$ and $t$ (source and target).
Thus, $y\B{\nextf}\alpha$ is satisfied exactly when $\alpha=\nextf(y)$.
If $y$ does not have a successor, then $y\B{\nextf}\alpha$ can only be $\true$
if $\alpha=\nullv$, hence $Q$ should be satisfied when $x$ is replaced by
$\nullv$, which is in line with the concrete semantics.

\lemref{RecoverNext} in \appref{Proofs:Reductions} shows that the formula $P_\nextf$ correctly defines $\nextf$ as a relation.

\paragraph*{Dynamic allocation.}
The rule $\wlp{x := \NEW}(Q)$ 
expresses the semantic uncertainty caused by the behavior
of the memory allocator. We want to be compatible with any run-time memory management, so
we do not enforce a concrete allocation policy, but instead require that the allocated node meets
some reasonable specifications, namely, that it is different from all values stored in
program variables, and that it is unreachable from any other node allocated previously
(Note: for programs with explicit {\tt free()}, this assumption relies on
the absence of dangling pointers, which can be verified by introducing 
appropriate assertions; this is, however, beyond the scope of this thesis).

\medskip
It should be noted that the rules for field dereference and for allocation
admit an additional level of quantifier nesting. As explained in \secref{Preliminaries:Effectively},
the number of nested universal quantifiers in an EPR formula contributes
an exponential component to the complexity of solving it. This is usually
not a scalability problem because the program is broken down into its loop-free code
segments, which are typically small. If there is a long loop-free block,
it can be broken down by (manually) adding assertions in the middle of it.

\subsubsection*{Closure of $\AER$}

Notice that \tabref{VCgen} only uses weakest liberal preconditions in a positive context without negations.
Therefore, the following proposition (proof in \appref{Proofs:Program})
holds.
\begin{proposition}[VCs in \FOAE]
\prolabel{AER-VC-closure}
For every program $S$ whose precondition $P$, postcondition $Q$,
branch conditions, loop conditions, and loop invariants are 
all expressed as $\AFR$ formulas, $\vcgen(\{P\} S \{Q\}) ~\in~ \AER$.
\end{proposition}


\paragraph*{Optimization remark.}
The size of the VC can be significantly reduced
if instead of syntactic substitution, we introduce a new predicate symbol for
each substituted atomic formula, axiomatizing its meaning as a separate formula.
For example, $\substitute{Q}{\alpha\B{\nextf^*}\beta}{P(\alpha,\beta)}$
(where $P$ is some formula with free variables $\alpha$, $\beta$), can be
written more compactly as
$\substitute{Q}{\alpha\B{\nextf^*}\beta}{r_1(\alpha,\beta)} \land
  \forall \alpha,\beta: r_1(\alpha,\beta)\impliesBothWays P(\alpha,\beta)$, where $r_1$ is a fresh predicate symbol.
When $Q$ contains many applications of $\B{\nextf^*}$ and $P$ is large, this
may save a lot of formula space; roughly, it reduces the order of the VC size
from quadratic to linear.

Another way to reduce the VC's size is to define an entirely new set of
symbols for each program point.
Our original implementation employed this optimization, which is also nice for finding bugs ---
when the program violates the invariants the SAT solver produces a counterexample with the concrete states at every
program point. A similar approach by~\cite{POPL:FlanaganS01} is also
applicable in this case.
