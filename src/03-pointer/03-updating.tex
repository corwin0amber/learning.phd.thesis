
\begin{headlines}
The problem of \newterm{view update} is to maintain some view over a relation,
while the underlying relation changes
{\blip}
In context of TC, it means how paths change when edges are added or removed
{\blip}
Recent results in descriptive complexity show that for DTC, the update is
expressible in $\FOL$ without quantifiers
\end{headlines}

\bigskip

An interesting development comes from the field of data systems and databases.
It is not uncommon for a database application to need the use of transitive
closure as part of its functionality. A famous example is the trip planner
application: a relation (table) contains available transportation links between
major cities or sites:

\begin{center}
$E =$~
\begin{tabular}{|l|l|}
\hline
{\bf from} & {\bf to} \\
\hline
Boston & New York \\
Boston & Amherst \\
Amherst & Pittsburgh \\
New York & Philadelphia \\
Philadelphia & Pittsburgh \\
Philadelphia & Baltimore \\
\hline
\end{tabular}
\end{center}

$E$ is a binary relation; for example, $E(\textrm{Boston}, \textrm{Amherst})$
(the column heads are inessential). It can be conceptualized as a directed
graph, as shown in~\figref{Cities-Graph}. One may then be interested in the
following query: is there a {\emph path}, using edges of $E$, from Boston to
Baltimore? Or, using logical notation, is the value of 
$E^*(\textrm{Boston}, \textrm{Baltimore})$ $\true$? Instead of having to scan
the graph every time such a query needs to be answered, a database can store
an auxiliary table $P$ containing all the pairs $\langle x, y\rangle$ such that
$E^*(x,y)$, and then for every incoming query one look-up suffices.

\begin{figure}
\begin{center}
\begin{tikzpicture}[>=latex]
  \node[rectangle,draw](Boston) { Boston };
  \node[rectangle,draw,right=of Boston](New York) { New York };
  \node[rectangle,draw,above right=of Boston](Amherst) { Amherst };
  \node[rectangle,draw,right=of New York](Philadelphia) { Philadelphia };
  \node[rectangle,draw,right=of Philadelphia](Pittsburgh) { Pittsburgh };
  \node[rectangle,draw,below right=of Philadelphia](Baltimore) { Baltimore };
  \draw[->] (Boston) edge (Amherst);
  \draw[->] (Boston) edge (New York);
  \draw[->] (New York) edge (Philadelphia);
  \draw[->] (Philadelphia) edge (Pittsburgh);
  \draw[->] (Philadelphia) edge (Baltimore);
  \draw[->] (Amherst) edge (Pittsburgh);
\end{tikzpicture}
\end{center}
\caption{\figlabel{Cities-Graph} Binary relation $P$ as a directed graph.}
\end{figure}

Besides the space trade-off needed to store the table, which may be manageable,
there exists the conceptual and computational cost of keeping the auxiliary table
$P$ {\emph up to date} with respect to an ever-changing table $E$. Changes to
$E$ come in the form of two operations: {\bf ins} --- insert a new pair,
{\bf del} -- delete an existing pair.

It has been long known (\cite{Immerman:1998:DC}) that the updates required to $P=E^*$ in
order to compensate for these operations on $E$, where $E$ is {\bf acyclic} 
(in the sense that there are no cycles in the graph of~\figref{Cities-Graph})
can be expressed by first-order formulas:

\begin{equation}
\begin{array}{r@{~~~}l@{~}l}
\textbf{ins}(a, b, E): &P'(x,y) \equiv &P(x,y) \lor \big(P(x,a) \land P(b,y)\big) \\[1em]
\textbf{del}(a, b, E): &P'(x,y) \equiv &P(x,y) \land
	    \Big[\lnot\big(P(x,a) \land P(b, y)\big) \lor{} \\[0.7em]
	& & \exists u,v: \Big(P(x,u) \land E(u,v) \land P(v,y) \\[0.7em]
	& & ~~{}\land P(u,a) \land \lnot P(v,a) \land (a\neq u\lor b\neq v)\Big)\Big]
\end{array}
\equlabel{Dyn-Comp-REACHacyc}
\end{equation}

The first equivalence in \equref{Dyn-Comp-REACHacyc} is very convenient as it
describes a quantifier-free correlation between $P$ and $P'$.
The second one is a bit trickier:
\begin{itemize}
  \item It uses an existential quantifier, which limits the context in which it
    may be used, when the interest is $\AER$ formulas.
  \item It uses both $P$ and $E$ intermixed.
\end{itemize}

Fortunately, according to recent results in dynamic complexity (\cite{Phd:Hesse03}),
when the relation $E$ is \newterm{deterministic}---that is, at every given time,
for every element $x$ there is at most one $y$ such that $E(x,y)$---
The definition for \textbf{del} can be greatly simplified (\textbf{ins} is 
unchanged and is brought here again for completeness):

\begin{equation}
\begin{array}{r@{~~~}l@{~}l}
\textbf{ins}(a, b, E): &P'(x,y) \equiv &P(x,y) \lor \big(P(x,a) \land P(b,y)\big) \\[1em]
\textbf{del}(a, b, E): &P'(x,y) \equiv &P(x,y) \land \lnot\big(P(x,a)\land P(b,y)\big)
\end{array}
\equlabel{Dyn-Comp-REACHacycdet}
\end{equation}

