\seclabel{Pointer:Deterministic}

\begin{headlines}
It is known that $\FOTC$ cannot be embedded fully in first-order logic
{\blip}
Some properties of transitive closure may be written as axioms, but the
result would never be complete
{\blip}
However, when the transitive closure is of a {\emph function} $f$ (in which
case it is referred to as {\emph deterministic} transitive closure, DTC), there
is some hope
{\blip}
By carefully expressing properties of $f^*$, while removing the explicit
symbol $f$ from the signature, a precise axiomatization is obtained
{\blip}
Still --- it is only complete w.r.t. finite structures, hence a finite-model
property is required to ensure correctness
\end{headlines}

\bigskip

A natural approach to expressing reachability with the \newterm{transitive closure operator}:

\begin{definition}[TC]
Let $\varphi(x,x')$ be a first-order formula with two free variables $x$ and $x'$.
We write $(\textrm{TC}_{x,x'}\,\varphi)$ to denote the reflexive, transitive closure
of the binary relation expressed by $\varphi$. Let $\FOTC$ be the logic comprising of 
all first-order formulas with arbitrary occurrences of $\textrm{TC}$.
\end{definition}

It is easy to see that $\B{\nextf^*}$ is but a special case of $\textrm{TC}$:
\[s \B{\nextf^*} t ~\equiv~ \big(\textrm{TC}_{xy}\,\nextf(x)=y\big)(s, t)\]

$\FOTC$ is strictly more expressive than first-order logic.
As a particular, fundamental example, it is well-known that, according to
the compactness theorem of first-order logic, the set $\mathbb{N}$
of natural numbers is not first-order expressible.
This means that for every first-order theory whose theorems are
valid over $\mathbb{N}$, there must also be non-standard models.
Indeed, from the L\"owenheim-Skolem theorem, it follows that it would
have models of any infinite cardinality. In contrast, the following
$\FOTC$ theory~\cite{Avron:2003}:
%
\begin{equation}
\begin{array}{l}
\forall x: S(x)\neq 0\\
\forall x,y: S(x)=S(y) \limplies x = y\\
\forall x: (\textrm{TC}_{xy}\,S(x)=y)(0, x)\\
\forall x: x+0=x\\
\forall x: x+S(y)=S(x+y)
\end{array}
\end{equation}

\noindent
has only models that are isomorphic to $\mathbb{N}$, where $S$ is interpreted
as the successor function and $+$ is interpreted as addition. Notice that
$(\textrm{TC}_{xy}\,S(x)=y)$ evaluates to $\leq$, the less-than-or-equals relation.
The axiomatization would {\bf not} be complete if we replaced it by
the standard equivalent $x\leq y ~\Leftrightarrow~ \exists z: x+z=y$, which is first-order.

It has been shown, however, that adding \textrm{TC}, even with various restrictions of
the first-order language, leads immediately to an undecidable logic (that is,
the satisfiability/validity check for formulas in this language is
undecidable); this can be shown by reducing the satisfiability problem
of universal $\FOTC$ formulas to tiling problems~\cite{CSL:ImmermanRRSY04}.

\medskip

Attempts have been made to use first-order reasoning to mechanically prove
properties of formulas with transitive closure. \cite{LMCS:Lev-Ami09} suggests
that we add the axiom
\[T_1[f] ~~\equiv~~ \forall u,v: f_{\mathrm{tc}}(u,v) \liff
  (u=v) \lor \exists w: f(u,w)\land f_{\mathrm{tc}}(w,v)\]

Where $f_{\textrm{tc}}$ is a new binary relation symbol used to denote
the transitive closure of an existing binary relation whose symbol is $f$.
While sound, it does not suffice to prove many valid $\FOTC$-theorems, and
an induction principle is required, which goes beyond first-order logic.
This is not surprising, since $\FOTC$ could never be fully axiomatized
via a first-order theory.

\bigskip
We therefore try to find a fragment of $\FOTC$ that would be expressive
enough to describe interesting properties of pointer data structures,
yet not too powerful as to become undecidable.
As a first step to exploring the decidability of formulas containing the transitive closure
operator, we define names for several limited classes of formulas.

\begin{definition}
\deflabel{Reach-Logics}
Let $t_1, t_2, \ldots t_n$ be logical variables or constant symbols.
We define four types of \textbf{atomic propositions}:
\begin{enumerate}
\item $\true$ / $\false$
\item $t_1 = t_2$ denoting equality
\item $R(t_1, t_2, \ldots, t_r)$ denoting the application of relation symbol $R$ of arity $r$
\item $t_1 \B{f^*} t_2$ denoting the existence of $k \geq 0$ such that $f^k(t_1) = t_2$, where
$f^0(t_1) \eqdef t_1$,
and
$f^{k+1}(t_1) \eqdef f (f^k(t_1))$
\end{enumerate}

We say that $t_1 \B{f^*} t_2$ is a \textbf{reachability constraint} between $t_1$ and $t_2$ via the function $f$.
\begin{itemize}
 \item \textbf{Quantifier-free formulas with Reachability} ($\QF$) are Boolean combinations of such formulas without quantifiers.
 \item \textbf{Alternation-free formulas with Reachability} ($\AF$) are Boolean combinations of such formulas with additional quantifiers
of the form $\forall^*\!\!:\!\! \varphi$ or  $\exists^*\!\!:\!\! \varphi$ where $\varphi$ is a \QF\ formula.
 \item \textbf{Forall-Exists Formulas with Reachability} ($\FOAE$) are Boolean combinations of such formulas
with additional quantifiers of the form $\forall^*\exists^*\!\!:\!\! \varphi$ where $\varphi$ is a \QF\ formula.
\end{itemize}
In particular, $\QF \subset \AF \subset \FOAE$. % \subset \FOReach$.
\end{definition}


\subsubsection*{Inverting Reachability Constraints}
A crucial step in moving from arbitrary $\FOTC$ formulas to $\AER$ or $\AFR$ formulas is eliminating explicit uses of functions % and in particular
such as the ``next'' function, $\nextf$.
While this may be difficult for a general graph,
we show that it can be done for programs that manipulate singly- and doubly-linked lists.
In this section, we informally demonstrate this elimination for acyclic lists.
We observe that if $\nextf$ is acyclic, we can construct $\nextf^+$  from $\nextf^*$
by

\begin{equation}
 \alpha \B{\nextf^+} \beta  ~~\impliesBothWays~~ \alpha \B{\nextf^*} \beta  \land \alpha \neq \beta
\end{equation}

Also, since $\nextf$ is a function, the set of nodes reachable from a node $\alpha$ is totally ordered
by $\nextf^*$.
Therefore, $\nextf(\alpha)$ is the minimal node in this order that is not $\alpha$.
The minimality is expressed using extra universal quantification in

\begin{equation}
 \nextf(\alpha) = \beta  ~~\impliesBothWays~~ \alpha \B{\nextf^+} \beta  \land \forall \gamma: \alpha \B{\nextf^+} \gamma \limplies \beta \B{\nextf^*} \gamma
 \equlabel{Inversion}
\end{equation}

This ``\emph{inversion}'' shows that $\nextf$ can be expressed using $\AFR$ formulas.
However, caution must be practiced when using the elimination above, because it may introduce 
unwanted quantifier alternations (see~\secref{Discussion:Expressivity:Inversion}).
Nevertheless our experiments demonstrate that in a number of commonly occurring examples,
the alternation can be removed or otherwise avoided, yielding an equivalent $\AER$/$\AFR$ formula.



\subsubsection*{Decidability of $\AER$}

Reachability constraints written as $\alpha\B{\nextf^*}\beta$ are not
directly expressible in $FOL$.  However, $\FOAE$ formulas can be
reduced to first-order $\forall^*\exists^*$ formulas without function
symbols (which are decidable; see \secref{Preliminaries:Effectively}) in the
following fashion: Introduce a new binary relation symbol $\rtnext$
with the intended meaning that $\rtnext(\alpha, \beta)
\liff \alpha \B{\nextf^*} \beta$.  Even though $\rtnext$ is
an uninterpreted relation, we will consistently maintain the fact that
it models reachability.  Every formula $\varphi$ is translated into

\begin{equation}
\varphi'\eqdef\substitute{\varphi}{t_1 \B{\nextf^*} t_2}{\rtnext(t_1, t_2)}
\equlabel{AER-to-EPR}
\end{equation}

For example, the acyclicity relation shown in~\equref{acyclic} is translated into:
\begin{equation}
\widehat{ac} \eqdef \forall \alpha, \beta: \rtnext(\alpha,\beta) \land \rtnext(\beta, \alpha) \limplies \alpha = \beta
\equlabel{acyclicr}
\end{equation}


We add the consistency rule $\Tlinord$ shown in \tabref{Rtnext}, which requires that
$\rtnext$ is a total order. This leads to the following propositions:

\begin{table}
\[
\begin{array}{|rcl|}\hline
\Tlinord &\eqdef& \forall \alpha,\beta: \rtnext(\alpha,\beta) \land \rtnext(\beta,
\alpha)\leftrightarrow \alpha = \beta \quad \land\\
&&\forall \alpha, \beta, \gamma: \rtnext(\alpha, \beta) \land \rtnext(\beta, \gamma) \limplies
\rtnext(\alpha, \gamma)\quad \land\\
&&\forall \alpha, \beta, \gamma: \rtnext(\alpha, \beta) \land \rtnext(\alpha, \gamma) \limplies  (\rtnext(\beta, \gamma) \lor \rtnext(\gamma, \beta))\\
\hline
\end{array}
\]
\caption{\tablabel{Rtnext}$\Tlinord$ says all points reachable from a given
  point are linearly ordered.}
\end{table}
%

\begin{proposition}[Simulation of $\AER$]
\prolabel{AER-simulation}
Consider an $\AER$ formula $\varphi$ over vocabulary $\voc=\B{\constants, \{\nextf\}, \relations}$.
Let $\varphi' \eqdef \varphi[\rtnext(t_1, t_2)/ t_1 \B{\nextf^*} t_2]$. Then
$\varphi'$ is a \FO\ formula over vocabulary $\voc'=\B{\constants, \emptyset, \relations \cup \{\rtnext\}}$ and
$\varphi$ is simulated by $ \Gamma_{\mbox{\small linOrd}} \limplies \varphi'$ where
$\Gamma_{\mbox{\small linOrd}}$ is the formula in \tabref{Rtnext}.
\end{proposition}

By ``simulate'' we mean that the constructed first-order formula 
is valid iff the given $\AER$ formula $\varphi$ is valid. 
\appref{Proofs:Reductions} contains a proof of this proposition.
The proof constructs real models from ``simulated'' $FO$ models 
using the reachability inversion~(\equref{Inversion}).

\paragraph{Finite Model Property}

One thing to point out regarding the proof of \proref{AER-simulation}, is that it
relies on the finiteness of the domain to reconstruct $\nextf$ from $\rtnext$.
To illustrate, consider the domain $Q$ of rational numbers with the order $\leq$.
This interpretation satisfies all the properties due to $\Gamma_{\mbox{\small linOrd}}$,
but for every two numbers $x \leq y, x \neq y$, there exists a number $z$ such that
$x \leq z \leq y, z\neq x, z\neq y$. In this case no element has a successor, and we cannot use
\equref{Inversion} to reconstruct a function $\nextf$ such that $\nextf^* = {}\leq$.
This is the main reason for requiring the $\forall^*\exists^*$ quantifier prefix
for our formulas; without it, not only does the logic become undecidable, but it
is also not complete: not every model of the ``simulated'' formula has a corresponding
model of the original formula in $\AER$.


\subsubsection*{Expressivity of $\AER$}

Although $\AE$ is a relatively weak logic, it can express interesting
properties of lists.
Typical predicates that express disjointness of two lists and sharing of tails
are expressible in \AE. For example, for two singly-linked lists with headers
$h, k$, $disjoint(h, k) \liff \forall \alpha: \alpha\neq\nullv\limplies \neg(h \B{\nextf^*} \alpha \land k \B{\nextf^*} \alpha)$.

Another capability still within the power of $\AE$ is to
relax the earlier assumption that the program manipulates the whole memory.
We describe a summary of \progreverse\ on arbitrary acyclic linked lists in a heap that may contain other linked data structures.
Realistic programs obey ownership requirements, e.g., the head $h$ of the list \textit{owns} the input list which means that it is impossible to
reach one of the list nodes without passing through $h$. That is,
\begin{equation}
\forall \alpha, \beta: \alpha\neq\nullv \limplies (h \B{\nextf^*} \alpha  \land \beta \B{\nextf^*} \alpha) \limplies h \B{\nextf^*} \beta
\equlabel{Owner}
\end{equation}
This requirement is conjoined to the precondition, $ac$, of \progreverse.
Its postcondition is the conjunction of $ac$, the fact that $h_0$ and $d$ reach the same nodes, 
(i.e., $\forall\alpha: h_0 \B{\nextf_0^*}\alpha \liff d \B{\nextf^*}\alpha$) and
\begin{equation}
\forall \alpha, \beta: \alpha \B{\nextf^*} \beta \liff
\LIFFour
{h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta}{\beta \B{\nextf_0^*} \alpha\land\beta\neq\nullv}
{\neg h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta}{\alpha \B{\nextf_0^*} \beta}
{h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta}{\false}
{\neg h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta}
{\alpha \B{\nextf_0^*} h_0 \land \beta = h_0}
\equlabel{SreachReverse}
\end{equation}
Here, the bracketed formula should be read as a four-way case, i.e., as disjunction of the formulas
$h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta \land\beta \B{\nextf_0^*} \alpha \land\beta\neq\nullv$;
$\neg h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta \land \alpha \B{\nextf_0^*} \beta$;
$h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta \land \false$;
and,
$\neg h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta \land
\alpha \B{\nextf_0^*} h_0 \land \beta = h_0$.
Intuitively, this summary distinguishes between the following four cases:
(i)~both the source ($\alpha$) and the target ($\beta$)
are in the reversed list
(ii)~both source and target are outside of the reversed list
(iii)~the source is in the reversed list and the target is not,
and
(iv)~the source is outside and the target is in the reversed list.
Cases (i)--(iii) are self-explanatory. For (iv)  reachability can occur when
there exists a path from $\alpha$ to $h_0 = \beta$.
\equref{SreachReverse} is a universal formula with $\B{\nextf_0^*}$, so it is in $\AFR$.
In terms of~\cite{POPL:RinetzkyBRSW05}, this means that we assume that the procedure is cutpoint-free,
that is, there are no pointers directly into the middle of the list.

The more general case where arbitrary cutpoints occur requires more quantifiers.
A non-\AF\ formula also arises when we want to specify the behavior of this function
in this case (such an attempt is discussed in \secref{Discussion:Expressivity:Formulas}).
We defer the issue of cutpoints and handle it extensively in \secref{Inter}.