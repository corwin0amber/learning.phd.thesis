
\begin{headlines}
The concept of pointers in data structures lends itself to
first-order axiomatization
{\blip}
A pointer field is thought of as a function mapping one object
to another
{\blip}
Problems arise when the two objects are of the same kind
{\blip}
The function can then be applied multiple types
{\blip}
Interesting properties require reasoning about paths between
objects, thus naturally requiring the use of transitive closure
\end{headlines}

\bigskip

Composite data structures are a desirable feature of a programming language. They
allow the programmer to reuse code by combining multiple entities and to define a hierarchy
of objects. For example, an object representing a book may contain a reference to an object
representing an author.

\begin{alltt}
\begin{tabbing}
cl\=ass Author \{ \+ \\
String firstName;\\
String lastName; \- \\
\}\\
\\
cl\=ass Book \{ \+ \\
String title;  \\
Author author; \- \\
\}
\end{tabbing}
\end{alltt}

In this example, every {\tt Book} instance is linked to at most one {\tt Author} instance.
The heap space may then be described as two disjoint sets of objects, $\mathit{Book}$ and
$\mathit{Author}$, and a mathematical function $\mathit{author} : \mathit{Book}\to\mathit{Author}$
representing the pointer field. In this case, if there are no more types defined, then a
program having $k$ variables of type {\tt Author} and $m$ variables of type {\tt Book} can
manipulate at most $k + 2m$ distinct objects at any given time.

The situation becomes more complicated with the introduction of collections, which require
the use of {\emph recursive types}. For example, a typical definition of a list looks like
the following:

\begin{alltt}
\begin{tabbing}
cl\=ass Node \{ \+ \\
String data;\\
Node next; \- \\
\}
\end{tabbing}
\end{alltt}

\figref{Reverse:code} presents a Java program for {\it in situ} reversal of a linked list.
It is a type-safe version of \figref{Reynolds-reverse:code}.
Every node of the list has a {\tt next} field that points to its successor node in the list.
Thus, we can model {\tt next} as a function $\nextsinglSym : \textit{Node} \to \textit{Node}$
that maps a node in the list to its successor.
Because we are going to use this function a lot in this chapter and the following,
we employ the abbreviation $\nextf$ for $\nextsinglSym$ in order to
keep formulas short and readable.
Now, assume, for simplicity of the example, that
the program $\progreverse$ manipulates the entire heap, that is, the heap consists of just the nodes in the linked list,
where the head of the input list is given as a formal parameter $h$.
To describe the heap that is \newterm{reachable} from $h$, we use
the formula
%
\begin{equation}
\forall \alpha: h \B{\nextf^*} \alpha
\end{equation}

Where the special notation $\B{\nextf^*}$ means zero or more applications of $\nextf$. In
this case, since the
range of this function is the same as its domain, an {\bf unbounded} number of elements can
be accessed by repeated application --- even for a program with only one variable.

We also assume, for this chapter, that the heap  is acyclic, i.e., the formula $ac$ below
is a precondition of $\progreverse$.
%% Acyclicity uses the \emph{reachability relation}
%% $\nextf^*$ where, as noted above,
%% $\alpha \B{\nextf^*} \beta$ says that node $\beta$ is reachable from
%% node $\alpha$.
%
\begin{equation}
ac ~\eqdef~ \forall \alpha, \beta: \alpha \B{\nextf^*} \beta \land \beta \B{\nextf^*} \alpha \limplies \alpha = \beta
\equlabel{acyclic}
\end{equation}

\begin{figure}
\begin{alltt}
\begin{tabbing}
No\=de reverse(Node h) \{  \+ \\
0: Node c = h;\\
1: Node d = null;\\
2: wh\=ile  3: (c != null)\+ \{ \\
4: Node t = c.next;\\
5: c.next = null;\\
6: c.next = d;\\
7: d = c;\\
8: c = t;\- \\
\}\\
9: return d; \- \\
\}
\end{tabbing}
\end{alltt}
\caption{\figlabel{Reverse:code}%
A simple Java program that reverses a list in-place.}
\end{figure}

\tabref{ReverseNOF} shows the invariants $I_0$, $I_3$ and $I_9$ that describe
a precondition, a loop invariant, and a postcondition of \progreverse.
They are expressed in \AF\ which permits use of function symbols (e.g. $\nextf$) in formulas only to express reachability
 (cf. $\nextf^*$); moreover, quantifier alternation is not permitted.
The notation {\tiny$\LIF{b}{f}{g}$} is shorthand for the conditional
$(b \land f) \lor (\neg b \land g)$.


\begin{table}
\[
\begin{array}{|c|}
\hline
I_0 \eqdef ac\land \forall \alpha: h \B{\nextf^*} \alpha\\
I_3 \eqdef ac \land \forall \alpha, \beta\neq \nullv:
\LIF{d \B{\nextf^*} \alpha}{\alpha \B{\nextf^*}  \beta \impliesBothWays \beta \B{\nextf_0^*} \alpha}
{c \B{\nextf^*} \alpha \land (\alpha \B{\nextf^*} \beta \impliesBothWays  \alpha \B{\nextf_0^*} \beta)}\\
I_9 = ac \land \forall \alpha: d \B{\nextf^*} \alpha \land (\forall \alpha, \beta: \alpha \B{\nextf^*} \beta \impliesBothWays  \beta \B{\nextf_0^*} \alpha)\\
\hline
\end{array}
\]
\caption{\tablabel{ReverseNOF}%
\AF\ invariants for $reverse$ (shown in \figref{Reverse:code}).
Note that $\nextf, \nextf_0$ are function symbols while
$\alpha \B{\nextf^*} \beta$, $\alpha \B{\nextf_0^*} \beta$ are atomic propositions on the reachability via directed paths from $\alpha$ to $\beta$
consisting of $\nextf$, $\nextf_0$ edges.}
\end{table}



Note that $I_3$ and $I_9$ refer to $\nextf_0$, the value of $\nextf$
at procedure entry.
The postcondition $I_9$ says that $reverse$ preserves
acyclicity of the list and updates $\nextf$
so that, upon procedure termination, the links of the original list have been reversed.
It also says that all the nodes are reachable from $d$ in the reversed list.
$I_3$ says that at loop entry $c$ is non-null and moreover, the original list is partially reversed. 
That is, the part of the list reachable from $d$ has its links reversed wrt.~the original list, 
whereas any node not reachable from $d$ is reachable from $c$ and keeps its original link.

The formulas represented so far are not first-order, due to the use of $\B{\nextf^*}$. This
puts these formulas in the class $\FOTC$ of {\emph first-order logic with transitive closure}.
This is a very rich logic, which is undecidable~\cite{ImmermanRRSY:2004:CSL}.

Observe that $I_3$ and $I_9$ only  refer to $\nextf^*$ and never to $\nextf$ alone.
A more natural way to express $I_9$ would be
\begin{equation}
I'_9 \eqdef ac \land \forall \alpha: d \B{\nextf^*} \alpha \land(\forall \alpha, \beta: \nextf(\alpha) = \beta \impliesBothWays \nextf_0(\beta)= \alpha)
\equlabel{NatSummaryReverse}
\end{equation}

But this mix of $\nextf^*$ and $\nextf$ is risky in this context. We shall immediately see why.
