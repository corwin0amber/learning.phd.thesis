
\begin{headlines}
Implemented $\wlp{}$ for While-language with pointers
{\blip}
Proved validity of VC using Z3 as solver for EPR
{\blip}
Our tool was able to verify a collection of naturally-occurring programs
\end{headlines}


\subsubsection*{Details}
We have implemented a VC generator, according to
Tables \tabref{Wprules} and \tabref{VCgen}, in Python,
and PLY (Python Lex-Yacc) is employed at the front-end 
to parse While-language programs annotated with $\AF$ assertions.
The tool verifies that invariants are in the class {\AF} and have
reachability constraints along a single field (of the form $f^*$).
The assertions may refer to the store and heap at the entry to the procedure via $x_0$, $f_0$, etc.
SMT-LIB v2~\cite{smtlib} standard notation is used to format the VC and
to invoke Z3. The validity of the VC can be checked by providing
its negation to Z3. 
If Z3 exhibits a satisfying assignment then that serves as counterexample 
for the correctness of the assertions.
If no satisfying assignment exists, then the generated
VC is valid, and therefore the program satisfies the assertions.

The output model/counterexample (S-Expression), if one is generated,
is then also parsed, so that we have the truth table of $\nextf^*$.
This structure represents the state of the program either at entry or at the
beginning of a loop iteration: running the program from this
point will violate one or more invariants.
To provide feedback to the user, $\nextf$ is recovered by computing \equref{Inversion}, 
and then the {\tt pygraphviz} tool is used to visualize
and present to the user a directed graph, whose vertices are
nodes in the heap, and whose edges are the $\nextf$ pointer fields.

We also implemented two procedures for generating VCs: the first one
implements the standard rules shown in \tabref{VCgen} and a second one uses a separate set of (predicate and constant) symbols per program point
as a way to reduce the size of the generated VC formula, as explained in the optimization
remark above.
We only report data on the former since it exhibited shorter running times,
through the latter may scale better.


\subsubsection*{Verification Examples}
We have written \AF\ loop invariants and procedure pre- and postconditions for 13 example procedures shown
in \tabref{Experiments}.
These are standard benchmarks and what they do can be inferred either from 
their names or from \tabref{Exp-Descriptions}.
We are encouraged by the fact that it was not difficult to express assertions in \AF\ for these procedures.
The annotated examples and the VC generation tool are publicly available from~\url{bitbucket.org/tausigplan/epr-verification}.

For an example of the annotations used in the benchmarks, see \tabref{ReverseNOF},
listing the precondition, loop invariant, and postcondition of \progreverse.
More examples are given in \appref{Code:Examples}.

\begin{comment}
As another example, the program {\tt filter} given in \figref{Filter:code} was
verified using the following loop invariant:
%
\begin{eqnarray*}
&&(i \neq \nullv \implies h\B{\nextf^*}i) \land \\
&& if(j = null, i = h, P_{\nextf}[j/c,i/d] \land h\B{\nextf^*}j) \land \\
&&     \forall \alpha,\beta: \alpha\B{\nextf^*_0}\beta \impliesBothWays \alpha\B{\nextf^*}\beta \land \\
&&     \forall \gamma : \gamma \neq null \implies
               if(C(\gamma), h\B{\nextf^*}\gamma \impliesBothWays h\B{\nextf^*_0}\gamma \land i\B{\nextf^*}\gamma, \\
&& \hspace{4.5cm}                    h\B{\nextf^*}\gamma \impliesBothWays h\B{\nextf^*_0}\gamma)
\end{eqnarray*}
\end{comment}
As expected, Z3 is able to verify all the correct programs.
\tabref{Experiments} shows statistics for size and complexity of the invariants and the running times for Z3.
The tests were conducted on a 1.7GHz Intel Core i5 machine with 4GB
of RAM, running OS X 10.7.5. The version of Z3 used was 4.2, compiled
for 64-bit Intel architecture (using {\tt gcc} 4.2, LLVM). The solving
time reported is wall clock time of the execution of Z3.

To give some account of the programs' sizes, we observe the program summary
specification given as pre- and postcondition,
count the number of atomic formulas in each of them, and note the depth of
quantifier nesting; all our samples had only universal quantifiers.
We did the same for each program's loop invariant
and for the generated $\vcgen$.
Naturally, the size of the VC grows rapidly ---approximately at a 
quadratic rate. 
This can be observed in the result of the measurements for ``SLL: merge'',
where (i)~the size of the invariant and
(ii)~the number of if-branches and heap manipulating statements,
was larger than those in other examples.
Still, the time required by Z3 to prove that the VC is valid is short.

For comparison, the size of the formula generated by the alternative
implementation, using a separate set of symbols for each program location,
was about 10 times shorter --- 239 atomic formulas. However, Z3 took
a significantly longer time, at 1357ms. We therefore preferred to use
the first implementation.

Thanks to the fact that FOL-based tools, and in particular SAT
solvers, permit multiple relation symbols we were able to express ordering properties in sorted lists, and so
verify order-aware programs such as ``insert'' and ``merge''.
This situation can be contrasted with tools like Mona~(\cite{ks},\cite{KlaEtAl:Mona}) which are based on 
monadic second-order logic, where multiple relation symbols are disallowed.


\subsubsection*{Composing Procedure Summaries}
\begin{figure}
\begin{minipage}{2.7in}
\begin{alltt}
\begin{tabbing}
No\=de filter(Node h) \{ \+\\
Node i = h, j = null; \\
wh\=ile (i != null) \{ \+\\
if (i!=h && C(i)) j.n := i.n; \\
else j := i; \\
i := i.n; \-\\
\}\\
return h;\-\\
\}
\end{tabbing}
\end{alltt}
\end{minipage}
\caption{\figlabel{Filter:code}A simplified Java program that removes elements
from a list according to some predicate; for simplicity, we
assume that the head is never removed.}
\end{figure}

Additionally, we performed experiments in composing summaries of {\progfilter} (\figref{Filter:code})
and {\progreverse} (\figref{Reverse:code}). In this case, we wrote the formulas
manually and ran Z3 on them, to get a proof of the validity
of the equivalences. We found that $\AFR$-postconditions of procedure summaries can be sequentially composed 
for this purpose.


\medskip
\textit{Illustrating $reverse(reverse\; h) = h$.}
% To show that running $reverse$ twice yields the original list,
Let $\nextf^*_1$ denote the reachability after running the inner
$reverse$, and let $\nextf^*_2$ denote the reachability after running the outer
$reverse$. We can express the equivalence of $reverse(reverse\;h)$ and $h$ using the following \AF\ implication:
%
%% \begin{eqnarray*}
%%   &&\Tlinord \land \\
%%   &&(\forall \alpha,\beta:
%%       \alpha\B{\nextf^*_1}\beta \impliesBothWays \beta\B{\nextf^*_0}\alpha) \land \\
%%   &&(\forall \alpha,\beta:
%%       \alpha\B{\nextf^*_2}\beta \impliesBothWays \beta\B{\nextf^*_1}\alpha) \\
%%   &&\implies \\
%%   &&\forall \alpha,\beta:
%%       \alpha\B{\nextf^*_2}\beta \impliesBothWays \alpha\B{\nextf^*_0}\beta
%% \end{eqnarray*}
\begin{equation}
 \begin{array}{l}
%  \Tlinord \land
  (\forall \alpha,\beta:
      \alpha\B{\nextf^*_1}\beta \impliesBothWays \beta\B{\nextf^*_0}\alpha) \land
  (\forall \alpha,\beta:
      \alpha\B{\nextf^*_2}\beta \impliesBothWays \beta\B{\nextf^*_1}\alpha) \\
\qquad\qquad  \implies \forall \alpha,\beta:
      \alpha\B{\nextf^*_2}\beta \impliesBothWays \alpha\B{\nextf^*_0}\beta
\end{array}
\end{equation}
The second conjunct of the implication's antecedent describes the effect of the inner $reverse$ on the initial state while
the third conjunct describes the effect of the outer $reverse$ on the state resulting from the first.
The consequent of the implication states that the initial and final states are equivalent.
\\[1ex]
\textit{Illustrating $filter(C, reverse(h)) = reverse(filter(C, h))$.}\
The program \progfilter\ takes a unary predicate $C$ on nodes, and a list with head $h$, and returns a list with all nodes satisfying $C$ removed.
The postcondition of \progfilter\ is:
%\begin{center}
$\forall \alpha,\beta:
    \alpha\B{\nextf^*}\beta \impliesBothWays
       \lnot C(\alpha)\land \lnot C(\beta) \land \alpha\B{\nextf^*_0}\beta$.
%\end{center}
It says that $\beta$ is reachable from $\alpha$ in the filtered list provided neither $\alpha$ nor $\beta$ satisfies $C$ and $\beta$ was reachable from $\alpha$ initially.
We show that the equivalence of $filter(C, reverse(h))$ and $reverse(filter(C, h))$ can be expressed using an $\AFR$-theorem:

\begin{eqnarray*}
  &&(\forall \alpha,\beta:
      \alpha\B{\nextf^*_1}\beta \impliesBothWays \beta\B{\nextf^*_0}\alpha) \land \\
  &&(\forall \alpha,\beta:
      \alpha\B{\nextf^*_2}\beta \impliesBothWays
      \lnot C(\alpha)\land \lnot C(\beta) \land \alpha\B{\nextf^*_1}\beta) \land\\
  &&(\forall \alpha,\beta:
      \alpha\B{{\nextf^*_1}'}\beta \impliesBothWays
         \lnot C(\alpha)\land \lnot C(\beta) \land \alpha\B{\nextf^*_0}\beta) \land\\
  &&(\forall \alpha,\beta:
      \alpha\B{{\nextf^*_2}'}\beta \impliesBothWays \beta\B{{\nextf^*_1}'}\alpha) \\
  &&\implies  \\
  &&\forall \alpha,\beta:
      \alpha\B{\nextf^*_2}\beta \impliesBothWays \alpha\B{{\nextf^*_2}'}\beta
\end{eqnarray*}
Here $\nextf_1^*$ denotes the reachability after running \progreverse~on the input list (first conjunct of the implication's antecedent)
and $\nextf_2^*$ denotes the reachability after running \progfilter~on this reversed list (second conjunct). 
Similarly ${\nextf_{1}^*}'$ denotes the reachability after running \progfilter~on the input list (third conjunct) 
and ${\nextf_{2}^*}'$ denotes the reachability after running \progreverse~on this filtered list (fourth conjunct). 
The consequent of the implication states that the reachability 
after performing the first execution path (first \progreverse, then \progfilter)
is equivalent to that after performing the second execution path
(first \progfilter, then \progreverse).

%% \smallnote{Need more explanation!!   ?}



%We will point out here that the size of the VC can be significantly reduced
%if instead of syntactic substitution, we introduce new vocabulary for
%substituted atomic formulas, axiomatizing their meaning as a separate formula.
%For example, $\substitute{Q}{\alpha\B{\nextf^*}\beta}{P(\alpha,\beta)}$
%(where $P$ is some formula with free variables $\alpha$, $\beta$), can be
%written more compactly as
%$\substitute{Q}{\alpha\B{\nextf^*}\beta}{r_1(\alpha,\beta)} \land
%  \forall \alpha,\beta: r_a(\alpha,\beta)\impliesBothWays P(\alpha,\beta)$.
%When $Q$ contains many application of $\B{\nextf^*}$ and $P$ is large, this
%may save a lot of formula space; roughly, it reduces the order of the VC size
%from quadratic to linear.

\begin{table}
\begin{tabular}{l@{\;---\;}p{3.5in}}
  SLL: insert & Adds a node into a sorted list, preserving order. \\
  SLL: find & Locates the first item in the list with a given value. \\
  SLL: last & Returns the last node of the list. \\
  SLL: merge & Merges two sorted lists into one, preserving order. \\
  SLL: swap & Exchanges the first and second element of a list. \\
  DLL: fix  & Directs the back-pointer of each node towards the previous
              node, as required by data structure invariants. \\
  DLL: splice & Splits a list into two well-formed doubly-linked lists.
\end{tabular}
\caption{\tablabel{Exp-Descriptions}Description of some linked list manipulating programs verified by our tool.}
\end{table}


\begin{table}
\begin{tabular}{|l|rr|rr|rr|r|}
 \hline
   \multirow{3}{*}
   {Benchmark} & \multicolumn{6}{c}{Formula size}
                & Solving \\
   & \multicolumn{2}{c}{P,Q} 
   & \multicolumn{2}{c}{I}  
   & \multicolumn{2}{c}{VC} & \multicolumn{1}{c|}{time} \\             
   & {\small \#} & {\small $\forall$}
   & {\small \#} & {\small $\forall$}
   & {\small \#} & {\small $\forall$}
             & \multicolumn{1}{c|}{\small (Z3)} \\
   \hline
   SLL: reverse     &  2 &  2 & 11 &  2 &  133 &  3 &   57ms \\
   SLL: filter      &  5 &  1 & 14 &  1 &  280 &  4 &   39ms \\
   SLL: create      &  1 &  0 &  1 &  0 &   36 &  3 &   13ms \\
   SLL: delete      &  5 &  0 & 12 &  1 &  152 &  3 &   23ms \\
   SLL: deleteAll   &  3 &  2 &  7 &  2 &  106 &  3 &   32ms \\
   SLL: insert      &  8 &  1 &  6 &  1 &  178 &  3 &   17ms \\
   SLL: find        &  7 &  1 &  7 &  1 &   64 &  3 &   15ms \\
   SLL: last        &  3 &  0 &  5 &  0 &   74 &  3 &   15ms \\
   SLL: merge       & 14 &  2 & 31 &  2 & 2255 &  3 &  226ms \\
   SLL: rotate      &  6 &  1 &  - &  - &   73 &  3 &   22ms \\
   SLL: swap        & 14 &  2 &  - &  - &  965 &  5 &   26ms \\
 \hline
   DLL: fix         &  5 &  2 & 11 &  2 &  121 &  3 &   32ms \\
   DLL: splice      & 10 &  2 &  - &  - &  167 &  4 &   27ms \\   
 \hline 
\end{tabular}
\caption{\tablabel{Experiments}Implementation Benchmarks;
P,Q --- program's specification given as pre- and post-condition,
I --- loop invariant, VC --- verification condition, 
\# --- number of atomic formulas, $\forall$ --- quantifier nesting depth}
\end{table}



\subsubsection*{Buggy Examples}
\seclabel{Pointer:Empirical:Buggy}

\begin{table}
\begin{tabular}{|l|p{4.7cm}|rr|rr|rr|r|r|}
 \hline
   \multirow{3}{*}
   {Benchmark} & \multirow{3}{*}{Nature of Defect}
               & \multicolumn{6}{c}{Formula size}
                & \;Solving\; & \multicolumn{1}{c|}{C.e.} \\
   && \multicolumn{2}{c}{P,Q} 
    & \multicolumn{2}{c}{I}  
    & \multicolumn{2}{c}{VC} & \multicolumn{1}{c|}{time} 
    & \multicolumn{1}{c|}{size} \\             
   && \;{\small \#}\; & \;{\small $\forall$}\;
    & \;{\small \#}\; & \;{\small $\forall$}\;
    & \;{\small \#}\; & \;{\small $\forall$}\;
             & \multicolumn{1}{c|}{\small (Z3)}
             & \multicolumn{1}{c|}{\small ($|\Locs|$)} \\
 \hline
  SLL: find       & $\nullv$ pointer dereference. 
                  &  7 &  1 &  7 &  1 &   64 &  3 &   18ms & 2\; \\
  SLL: deleteAll  & Loop invariant in annotation is too weak to
                   prove the desired property.  
                  &  3 &  2 &  5 &  2 &   68 &  3 &   58ms & 5\; \\
  SLL: rotate     & Transient cycle introduced during execution. 
                  &  6 &  1 &  - &  - &  109 &  3 &   25ms & 3\; \\
  SLL: insert     & Unhandled corner case when an element with the
                    same value already exists in the list ---
                    ordering violated. 
                  &  8 &  1 &  6 &  1 &  178 &  3 &   33ms & 4\; \\
 \hline
\end{tabular}
\vspace{0.5em}
\caption{\tablabel{Experiments-Bugs}Information about benchmarks that demonstrate
  detection of several kinds of bugs in pointer programs.
  In addition to the previous measurements, the last column lists the size
  of the generated counterexample in terms of the number of vertices,
  or linked-list nodes.}
\end{table}


We also applied the tool to erroneous programs and programs with incorrect assertions.
The results, including run-time statistics and formula sizes,
are reported in \tabref{Experiments-Bugs}. In addition, we measured the size of the
model generated, by observing the size of the generated domain---which reflects the
number of nodes in the heap.
As expected, Z3 was able to produce a concrete counterexample of a small size.
Since these are slight variations of the correct programs, size and running
time statistics are similar.

An example of generated output when a program fails to verify can be seen,
for the $insert$ program, in
\figref{Counterexample-example}.
The tool reports, as part of its output, that a counterexample occurs when $j=\nullv$ and $h.val = i.val = e.val$.

\begin{figure}
\begin{tabular}{ll}
\begin{minipage}{8.5cm}
\begin{alltt}\small
\begin{tabbing}
No\=de insert(Node h, Node e) \{\+\\
Node i = h, j = null;\\
wh\=ile (i != null && e.val >= i.val) \{\+\\
j = i; i = i.n;\-\\  
\}\\
if (j != null) \{ j.n = e; e.n = i; \}\\
else \{ e.n = h; h = e; \} \\
return h;\-\\
\}
\end{tabbing}
\end{alltt}
\end{minipage}
&
\begin{tikzpicture}[listnode/.style={shape=rectangle,draw,minimum size=4mm},
                    data/.style={shape=rectangle,draw,minimum size=1mm,fill=Grey!40},
                    >=latex,baseline=-1cm]
  \node[listnode](hval) {\small $v$};
  \node[listnode,right=0mm of hval](h) {};
  \node[listnode,right=of h](tailval) {\small $v$};
  \node[listnode,right=0mm of tailval](tail) {};
  \node[listnode,below=of hval](eval) {\small $v$};
  \node[listnode,right=0mm of eval](e) {};
  \draw[->] (h.center) -- node[above] {$\;\nextf$} (tailval);
  \node[above=3mm of hval](iptr) {$i$};
  \node[left=0mm of iptr](hptr) {$h$};
  \node[above=3.6mm of eval](dummy) {};
  \node[left=1mm of dummy](eptr) {$e$};
  \draw[->] (hptr) -- (hval);
  \draw[->] (iptr) -- (hval);
  \draw[->] (eptr) -- (eval);
%  \node[below=5mm of tail](jnull) {$j=\nullv$};
\end{tikzpicture}
\end{tabular}
\caption{\figlabel{Counterexample-example}
Sample counterexample generated for a buggy version of $insert$
for a sorted list.
Here, the loop invariant required that 
$\forall \alpha: (h\B{\nextf^*}\alpha \land 
                 \neg i\B{\nextf^*}\alpha) \limplies 
                 \alpha <_{val} e$
(where $<_{val}$ is an ordering on nodes according to their values),
but the loop condition is $\true$, therefore loop will execute one more time, violating this.
}
\end{figure}
