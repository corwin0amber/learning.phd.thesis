\paragraph*{Decidable Logic.}
The results in this chapter show that reachability properties of programs manipulating linked lists can be verified
using a simple decidable logic \FOAE.
Many recent decidable logics for reasoning about linked lists have been proposed~\cite{PLDI:MS01,JLP:Yorsh07,POPL:MPQ11,ATVA:BouajjaniDES12}.
%In particular, the Mona system was used in \cite{PLDI:MS01} to show correctness of linked lists with invariants expressed using weak monadic 2nd order logic.
In comparison to these works we drastically restrict the way quantifiers are allowed but permit arbitrary use of relations.
Thus, strictly speaking our logic is incomparable to the above logics.
We show that relations are used even in programs like $reverse$ to write procedure summaries such as the one in~(\ref{Eq:SreachReverse}) and for expressing numeric orders in sorting programs.

\paragraph*{Employing Theorem Provers.}
The seminal paper on pointer verification~\cite{Nelson} provides useful axioms for verifying reachability in linked data structures using theorem provers
and conjectures that these axioms are complete for describing reachability.
Lev-Ami et al.~\cite{LMCS:Lev-Ami09} show that no such axiomatization is possible.
This thesis sidesteps the above impossibility results by restricting first-order quantifications and by using the fact that
Bernays-Sch\"{o}nfinkel formulas have the finite model property.

Lahiri and Qadeer~\cite{popl:LahiriQ08} provide rules for weakest of preconditions for programs with circular linked lists.
The formulas are similar to Hesse's~\cite{Phd:Hesse03} but require that the programmer explicitly break the cycle---by annotating
the code in a way that designates one edge per cycle as the ``closing link''.
Our framework can be used both with and without the help of the programmer.
In practice it may be beneficial to require that the programmer break the cycle in certain cases in order to allow invariants 
that distinguish between segments in the cycle.

\paragraph*{Descriptive Complexity.}
Descriptive complexity was recently incorporated into the TVLA shape analysis framework~\cite{TOPLAS:RepsSL10}.
In this chapter we pioneer the use of descriptive complexity for \textit{guaranteeing} that if the programmer writes \AF\ assertions and if the program
manipulates singly- and doubly-linked lists, then the VCs are guaranteed to be expressible as \FOAE\ formulas.
