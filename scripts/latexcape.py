"""
A handy script to translate formulas occurring in the test benchmarks
to proper LaTeX with logical connectives and quantifiers.
"""

import sys
import re



class OperatorSubstitution(object):
    
    OP_RE = re.compile(r"&|\||->|<->|forall|exists|!=|~|null|ite")
    OP_DICT = {'&': r'\land', '|': r'\lor', '->': r'\limplies',
               '<->': r'\liff', 'forall': r'\forall', 'exists': r'\exists',
               '!=': r'\neq', '~': r'\lnot', 'null': r'\vnull', 'ite': r'\ite'}
    
    def __call__(self, text):
        return self.OP_RE.sub((lambda x: self.OP_DICT[x.group()]+' '), text)
    
    

class MacroSubstitution(object):
    
    MACRO_RE = re.compile(r"(n\*|n\+_|ntot_|_n\*)\((.*?),(.*?)\)")
    MACRO_DICT = {'n*': r'\nrtc', 'n+_': r'\ntc', 'ntot_': r'\nsingl', '_n*': r'\unrtc'}
    
    def __call__(self, text):
        def sub(mo):
            return r'%s{%s}{%s}' % (self.MACRO_DICT[mo.group(1)],
                                    mo.group(2).strip(), mo.group(3).strip())
        return self.MACRO_RE.sub(sub, text)
    
    

class Alignment(object):
    
    LET_RE = re.compile(r'^\w+\s*:=\s*')
    INDENT_RE = re.compile(r'^\s+')
    
    def __call__(self, text):
        def scan():
            for line in text.splitlines():
                mo = self.LET_RE.match(line)
                if mo:
                    line = self.LET_RE.sub(mo.group().replace(":=", "~:=~") + "&", line) + r" \\"
                elif line.strip():
                    mo = self.INDENT_RE.match(line)
                    if mo:
                        line = self.INDENT_RE.sub(mo.group() + "&", line) + r" \\"
                yield line
        return '\n'.join(scan())
        
    
    
if __name__ == '__main__':
    import argparse
    a = argparse.ArgumentParser()
    a.add_argument('filename', nargs='?', default="-")
    a = a.parse_args()
    
    if a.filename == '-':
        text = sys.stdin.read()
    else:
        text = open(a.filename).read()
    #text = open("/Users/corwin/var/workspace/Sindarin.EPR/examples/imp/sll-delete.imp").read()
    print Alignment()(MacroSubstitution()(OperatorSubstitution()(text)))
    