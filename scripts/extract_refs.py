
import re
import os.path



LABEL_REF_RE = re.compile(r'\\(\w+)(label|ref)\{(.*?)\}')
# sec|tab|fig|equ|lem|the|pro|def|ex|alg|item|line|ln|

REF_AGG = {"lines": 'line', "lineseq": 'line', "cha": 'sec', "app": 'sec'}


if __name__ == '__main__':
    def iter_sources():
        for dirpath, dirs, files in os.walk("../src"):
            for file in files:
                yield os.path.join(dirpath, file)

    by_file = {}
                
    for srcfile in iter_sources():
        if srcfile.endswith(".tex"):
            text = open(srcfile).read()
            labels_and_refs = LABEL_REF_RE.findall(text)
            labels_and_refs = [(REF_AGG.get(x, x), y, z) for x,y,z in labels_and_refs]
            by_file[srcfile] = labels_and_refs
    
    labels_by_key = {}
    for srcfile, labels_and_refs in by_file.iteritems():
        for (kind, lr, name) in labels_and_refs:
            if lr == 'label':
                key = (kind, name)
                labels_by_key[key] = srcfile
    
    def show_bare_labels_and_refs():
        for srcfile, labels_and_refs in sorted(by_file.items()):
            print srcfile, ":", [(y,z) for x,y,z in labels_and_refs if not x]
    
    def find_unresolved():
        for srcfile, labels_and_refs in sorted(by_file.items()):
            unresolved = []
            for (kind, lr, name) in labels_and_refs:
                if lr == 'ref':
                    key = (kind, name)
                    if key not in labels_by_key:
                        unresolved += [key]
            if unresolved:
                print srcfile, ":", unresolved 
    
    #show_bare_labels_and_refs()
    find_unresolved()
    